import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {NewHolePage} from './new-hole';
import {HorizontalSpinnerModule} from "@4r/horizontal-spinner/horizontal-spinner.module";
import {ComponentsModule} from "../../components/components.module";

@NgModule({
  declarations: [
    NewHolePage,
  ],
  imports: [
    IonicPageModule.forChild(NewHolePage),
    ComponentsModule,
    HorizontalSpinnerModule
  ],
})
export class NewHolePageModule {
}
