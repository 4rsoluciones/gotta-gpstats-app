import {BaseEntity, Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Play} from "./play";
import {Round} from "./round";

@Entity('hole')
export class Hole extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  number: number;

  @Column()
  par: number;

  @Column()
  design: string;

  @Column({nullable: true})
  penalityShotsCount: number;

  @Column({nullable: true})
  description: string;

  @ManyToOne(() => Round, round => round.holes, {
    cascadeInsert: true,
    cascadeUpdate: true,
    onDelete: "CASCADE"
  })
  round: Round;

  @OneToMany(() => Play, play => play.hole, {
    cascadeInsert: true,
    cascadeUpdate: true
  })
  strikes: Play[];

  /**
   * -1: se debe eliminar
   * 0: no hay modificaciones o no esta completo
   * 1: hay modificaciones y se debe enviar al backend
   * 2: es un hoyo por defecto
   */
  @Column()
  modified: number;

}
