import {Injectable} from '@angular/core';
import {ApiProvider} from "../api/api";

@Injectable()
export class ComparativesProvider {
  stats: any;
  gettingStats: boolean;
  statsPromise: Promise<any>;

  constructor(private api: ApiProvider) {
    this.init();
  }

  init() {
    this.gettingStats = false;
  }

  clear() {
    delete this.stats;
    delete this.statsPromise;
    this.init();
  }

  get(force?: boolean): Promise<any> {
    if (!this.gettingStats) {
      this.gettingStats = true;
      this.statsPromise = new Promise((resolve, reject) => {
        if (force) {
          delete this.stats;
        }
        if (this.stats) {
          this.gettingStats = false;
          resolve(this.stats);
        }
        this.api.getStats().then(response => {
          this.stats = response;
          this.gettingStats = false;
          resolve(this.stats);
        }).catch(error => {
          this.gettingStats = false;
          reject(error);
        })
      })
    }
    return this.statsPromise;
  }

}
