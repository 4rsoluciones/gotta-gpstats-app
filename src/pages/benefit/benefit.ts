import {Component} from '@angular/core';
import {IonicPage, NavParams, ToastController} from 'ionic-angular';
import {Benefit} from "../../models/benefit";
import {BenefitsProvider, ErrorProvider} from "../../providers/providers";

@IonicPage()
@Component({
  selector: 'page-benefit',
  templateUrl: 'benefit.html',
})
export class BenefitPage {
  benefit: Benefit;

  constructor(private navParams: NavParams,
              private error: ErrorProvider,
              private toast: ToastController,
              private benefitsProvider: BenefitsProvider) {
    this.benefit = this.navParams.get('benefit');
  }

  canjearClicked() {
    this.benefitsProvider.canjear(this.benefit).then((benefit: Benefit) => {
      this.benefit = benefit;
      this.toast.create({
        message: '¡Felicitaciones! Ha canjeado este beneficio y ya puede utilizarlo',
        position: 'bottom',
        showCloseButton: true,
        closeButtonText: 'Ok'
      }).present();
    }).catch(error => {
      switch (error.error.error.status) {
        case '5': {
          this.error.handle({
            error: error,
            log: 'error',
            text: 'Usted ya ha canjeado este beneficio. Cada beneficio sólo se puede utilizar una vez.'
          });
          break;
        }
        case '4': {
          this.error.handle({
            error: error,
            log: 'error',
            text: 'Lo sentimos. Este beneficio ya no esta disponible.'
          });
          break;
        }
        case '3': {
          this.error.handle({
            error: error,
            log: 'error',
            text: 'Lo sentimos. Este beneficio ya no esta disponible.'
          });
          break;
        }
        case '2': {
          this.error.handle({
            error: error,
            log: 'error',
            text: 'Lo sentimos. Este beneficio ya no esta disponible.'
          });
          break;
        }
        default: {
          this.error.handle(error);
        }
      }
    })
  }

}
