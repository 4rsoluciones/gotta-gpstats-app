import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {ComparativesPage} from './comparatives';
import {NgCircleProgressModule} from "ng-circle-progress";

@NgModule({
  declarations: [
    ComparativesPage,
  ],
  imports: [
    IonicPageModule.forChild(ComparativesPage),
    NgCircleProgressModule.forRoot({
      outerStrokeWidth: 8,
      space: -6,
      innerStrokeWidth: 4,
      innerStrokeColor: '#F4F4F4',
      titleColor: '#3333333',
      subtitleColor: '#999999',
      animation: false
    })
  ],
})
export class ComparativesPageModule {
}
