import {Injectable} from '@angular/core';
import {ApiProvider} from "../api/api";
import {Service} from "../../models/service";

@Injectable()
export class ServicesProvider {
  services: any;

  constructor(private api: ApiProvider) {

  }

  clear() {
    delete this.services;
  }

  get(force?: boolean): Promise<any> {
    return new Promise((resolve, reject) => {
      if (force) {
        delete this.services;
      }
      if (this.services) {
        resolve(Object.assign({}, this.services));
      } else {
        this.api.getServices().then(services => {
          this.services = services;
          resolve(Object.assign({}, this.services));
        }).catch(error => {
          reject(error);
        })
      }
    })
  }

  getServiceUrl(service: Service, platform: string): string {
    return this.api.getCentralizadorConfig().url + 'services/' + service.id + '/' + platform.split(' ').join('').toLowerCase();
  }

}
