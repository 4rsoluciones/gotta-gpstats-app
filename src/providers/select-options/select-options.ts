import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class SelectOptionsProvider {

  constructor(private http: HttpClient) {

  }

  getCountries(): Promise<any> {
    return new Promise(resolve => {
      this.http.get('assets/data/countries.json')
        .toPromise()
        .then(response => resolve(response))
    })
  }

  getStates(countryId: number): Promise<any> {
    return new Promise(resolve => {
      this.http.get('assets/data/states.json')
        .toPromise()
        .then(response => {
          let retValue = [];
          for (let i = 0; i < response['length']; i++) {
            if (response[i].country_id == countryId) {
              retValue.push(response[i]);
            }
          }
          resolve(retValue);
        })
    })
  }

  getAccountCountries(): Promise<any> {
    return new Promise(resolve => {
      this.http.get('assets/data/accountCountries.json')
        .toPromise()
        .then(response => resolve(response))
    })
  }

  getMovements(): Promise<any> {
    return new Promise(resolve => {
      this.http.get('assets/data/movements.json')
        .toPromise()
        .then(response => resolve(response))
    })
  }

  getFieldTypes(): Promise<any> {
    return new Promise(resolve => {
      this.http.get('assets/data/fieldTypes.json')
        .toPromise()
        .then(response => resolve(response))
    })
  }

  getParOptions(): Promise<any> {
    return new Promise(resolve => {
      this.http.get('assets/data/parOptions.json')
        .toPromise()
        .then(response => resolve(response))
    })
  }

  getGameConditions(): Promise<any> {
    return new Promise(resolve => {
      this.http.get('assets/data/gameConditions.json')
        .toPromise()
        .then(response => resolve(response))
    })
  }

  getGameTypes(): Promise<any> {
    return new Promise(resolve => {
      this.http.get('assets/data/gameTypes.json')
        .toPromise()
        .then(response => resolve(response))
    })
  }

  getTours(): Promise<any> {
    return new Promise(resolve => {
      this.http.get('assets/data/tours.json')
        .toPromise()
        .then(response => resolve(response))
    })
  }

  getInitialStateQuestionnaire(): Promise<any> {
    return new Promise(resolve => {
      this.http.get('assets/data/initialStates.json')
        .toPromise()
        .then(response => resolve(response))
    })
  }

  getFinalStateOptions(): Promise<any> {
    return new Promise(resolve => {
      this.http.get('assets/data/finalStates.json')
        .toPromise()
        .then(response => resolve(response))
    })
  }

  getHolePar(): Promise<any> {
    return new Promise(resolve => {
      this.http.get('assets/data/holePar.json')
        .toPromise()
        .then(response => resolve(response))
    })
  }

  getHoleDesigns(): Promise<any> {
    return new Promise(resolve => {
      this.http.get('assets/data/holeDesigns.json')
        .toPromise()
        .then(response => resolve(response))
    })
  }

  getBeforeQuestions(): Promise<any> {
    return new Promise(resolve => {
      this.http.get('assets/data/playBeforeQuestions.json')
        .toPromise()
        .then(response => resolve(response))
    })
  }

  getDistances(): Promise<any> {
    return new Promise(resolve => {
      this.http.get('assets/data/distances.json')
        .toPromise()
        .then(response => resolve(response))
    })
  }

  getPuttDistances(): Promise<any> {
    return new Promise(resolve => {
      this.http.get('assets/data/puttDistances.json')
        .toPromise()
        .then(response => resolve(response))
    })
  }

  getClubs(): Promise<any> {
    return new Promise(resolve => {
      this.http.get('assets/data/clubs.json')
        .toPromise()
        .then(response => resolve(response))
    })
  }

  getDirections(): Promise<any> {
    return new Promise(resolve => {
      this.http.get('assets/data/directions.json')
        .toPromise()
        .then(response => resolve(response))
    })
  }

  getInclinationBall(): Promise<any> {
    return new Promise(resolve => {
      this.http.get('assets/data/inclinationBall.json')
        .toPromise()
        .then(response => resolve(response))
    })
  }

  getInclinationLat(): Promise<any> {
    return new Promise(resolve => {
      this.http.get('assets/data/inclinationLat.json')
        .toPromise()
        .then(response => resolve(response))
    })
  }

  getResults(): Promise<any> {
    return new Promise(resolve => {
      this.http.get('assets/data/results.json')
        .toPromise()
        .then(response => resolve(response))
    })
  }

  getPuttResults(): Promise<any> {
    return new Promise(resolve => {
      this.http.get('assets/data/puttResults.json')
        .toPromise()
        .then(response => resolve(response))
    })
  }

  getFailReasons(): Promise<any> {
    return new Promise(resolve => {
      this.http.get('assets/data/failReasons.json')
        .toPromise()
        .then(response => resolve(response))
    })
  }

  getLieHole(): Promise<any> {
    return new Promise(resolve => {
      this.http.get('assets/data/lieHole.json')
        .toPromise()
        .then(response => resolve(response))
    })
  }

  getLieFoot(): Promise<any> {
    return new Promise(resolve => {
      this.http.get('assets/data/lieFoot.json')
        .toPromise()
        .then(response => resolve(response))
    })
  }

  getPlayerTypes(): Promise<any> {
    return new Promise(resolve => {
      this.http.get('assets/data/playerType.json')
        .toPromise()
        .then(response => resolve(response))
    })
  }

}
