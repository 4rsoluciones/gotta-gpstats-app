import {Component, ViewChild} from '@angular/core';
import {
  ActionSheetController, AlertController, Content, Events, IonicPage, LoadingController, ModalController, NavController,
  Slides, ToastController
} from 'ionic-angular';
import {ErrorProvider, RoundProvider, SelectOptionsProvider} from "../../providers/providers";

@IonicPage()
@Component({
  selector: 'page-round',
  templateUrl: 'round.html',
})
export class RoundPage {
  @ViewChild(Content) content: Content;
  @ViewChild('summarySlides') summarySlides: Slides;
  @ViewChild('holesSlides') holesSlides: Slides;

  roundInfo: any;
  initialStateQuestions: any[];
  finalStateQuestions: any[];
  initialState: any;
  summaryCards: any[];
  showRoundInfo: boolean;
  finalState: any;
  holes: any[];
  holeDesigns;
  conditions: any[] = [];
  gameTypes: any[] = [];
  tours: any[] = [];
  canConfirm: boolean;

  constructor(private error: ErrorProvider,
              private roundProvider: RoundProvider,
              private selectOptions: SelectOptionsProvider,
              private navCtrl: NavController,
              private events: Events,
              private toastCtrl: ToastController,
              private alertCtrl: AlertController,
              private loadingCtrl: LoadingController,
              private actionSheetCtrl: ActionSheetController,
              private modalCtrl: ModalController) {
    this.showRoundInfo = false;
    this.roundInfo = this.roundProvider.getRoundInfo();
    this.initialState = this.roundProvider.getInitialState();
    this.finalState = this.roundProvider.getFinalState();
    this.selectOptions.getInitialStateQuestionnaire().then(response => {
      this.initialStateQuestions = response;
    });
    this.selectOptions.getFinalStateOptions().then(response => {
      this.finalStateQuestions = response;
    });
    this.selectOptions.getGameConditions().then(response => {
      this.conditions = response;
    });
    this.selectOptions.getGameTypes().then(response => {
      this.gameTypes = response;
    });
    this.selectOptions.getTours().then(response => {
      this.tours = response;
    });
    this.selectOptions.getHoleDesigns().then(response => {
      this.holeDesigns = response;
    });
    this.summaryCards = new Array(this.roundInfo.played_holes / 9);
    for (let i = 0; i < this.summaryCards.length; i++) {
      this.summaryCards[i] = {holes: [{}, {}, {}, {}, {}, {}, {}, {}, {}], ida: {}};
    }
    this.holes = new Array(this.roundInfo.played_holes);
    for (let i = 0; i < this.holes.length; i++) {
      this.holes[i] = {};
    }
    this.events.subscribe('round:updated', () => {
      this.updateInfo();
    });
  }

  ionViewDidEnter() {
    this.updateInfo();
  }

  getGameConditionsByValue(value: any) {
    for (let i = 0; i < this.conditions.length; i++) {
      if (this.conditions[i].value == value) {
        return this.conditions[i];
      }
    }
    return false;
  }

  getGameTypesByValue(value: any) {
    for (let i = 0; i < this.gameTypes.length; i++) {
      if (this.gameTypes[i].value == value) {
        return this.gameTypes[i];
      }
    }
    return false;
  }

  getFinalStateByValue(value: any) {
    for (let i = 0; i < this.finalStateQuestions.length; i++) {
      if (this.finalStateQuestions[i].value == value) {
        return this.finalStateQuestions[i];
      }
    }
    return false;
  }

  getHoleDesignsByValue(value: any) {
    for (let i = 0; i < this.holeDesigns.length; i++) {
      if (this.holeDesigns[i].value == value) {
        return this.holeDesigns[i];
      }
    }
    return false;
  }

  getTourById(value: any) {
    for (let i = 0; i < this.tours.length; i++) {
      if (this.tours[i].id == value) {
        return this.tours[i];
      }
    }
    return false;
  }

  updateInfo() {
    this.roundInfo = this.roundProvider.getRoundInfo();
    this.initialState = this.roundProvider.getInitialState();
    this.finalState = this.roundProvider.getFinalState();
    let holes = this.roundProvider.getHoles();
    for (let i = 0; i < this.holes.length; i++) {
      if (!!holes[i]) {
        this.holes[i].par = holes[i].par;
        this.holes[i].design = holes[i].design;
        this.holes[i].plays = holes[i].strikes;
        this.holes[i].penalties = holes[i].penalityShotsCount;
      } else {
        this.holes[i].par = undefined;
        this.holes[i].design = undefined;
        this.holes[i].plays = undefined;
        this.holes[i].penalties = undefined;
      }
      this.summaryCards[Math.floor(i / 9)].holes[i % 9] = this.holes[i];
    }
    for (let i = 0; i < this.summaryCards.length; i++) {
      let parIDA = 0;
      let totalIDA = 0;
      for (let j = 0; j < this.summaryCards[i].holes.length; j++) {
        parIDA += this.summaryCards[i].holes[j].par ? this.summaryCards[i].holes[j].par : 0;
        totalIDA += (this.summaryCards[i].holes[j].plays ? this.summaryCards[i].holes[j].plays.length : 0) + (this.summaryCards[i].holes[j].penalties ? this.summaryCards[i].holes[j].penalties : 0);
      }
      this.summaryCards[i].ida = {par: parIDA, total: totalIDA};
    }
    if (this.roundProvider.getNextStep() == 'readyToConfirm') {
      this.canConfirm = true;
      this.content.resize();
    }
  }

  viewRoundInfoClicked() {
    this.showRoundInfo = !this.showRoundInfo;
  }

  roundInfoMoreClicked() {
    let actionSheet = this.actionSheetCtrl.create({
      cssClass: 'round-action-sheet',
      buttons: [
        {
          text: 'Editar datos de la vuelta',
          icon: 'md-create',
          handler: () => {
            this.editRoundInfo();
          }
        }
      ]
    });
    actionSheet.present();
  }

  editRoundInfo() {
    if (this.canEditRoundInfo()) {
      this.navCtrl.push('NewRoundPage', {edit: true});
    } else {
      this.toastCtrl.create({
        message: 'No se pueden editar los datos de la vuelta una vez que se empezaron a cargar las jugadas',
        duration: 3000
      }).present();
    }
  }

  canEditRoundInfo(): boolean {
    for (let i = 0; i < this.holes.length; i++) {
      if (this.holes[i].plays && this.holes[i].plays.length) {
        return false;
      }
    }
    return true;
  }

  loadInitialStateClicked() {
    let initialStateModal = this.modalCtrl.create('LoadInitialStatePage');
    initialStateModal.onDidDismiss(data => {
      this.updateInfo();
    });
    initialStateModal.present();
  }

  initialStateMoreClicked() {
    let actionSheet = this.actionSheetCtrl.create({
      cssClass: 'round-action-sheet',
      buttons: [
        {
          text: 'Editar estado inicial',
          icon: 'md-create',
          handler: () => {
            this.loadInitialStateClicked()
          }
        }
      ]
    });
    actionSheet.present();
  }

  holeMoreClicked(index) {
    let actionSheet = this.actionSheetCtrl.create({
      cssClass: 'round-action-sheet',
      buttons: [(this.holes[index].plays && this.holes[index].plays.length) ?
        {
          text: 'Eliminar jugada en el hoyo',
          icon: 'ios-trash',
          handler: () => {
            this.deleteHolePlayClicked(index + 1);
          }
        } :
        {
          text: 'Editar datos del hoyo',
          icon: 'md-create',
          handler: () => {
            this.loadHoleDataClicked(index + 1, !!this.holes[index].design);
          }
        }
      ]
    });
    actionSheet.present();
  }

  deleteHolePlayClicked = (holeNumber) => {
    let alert = this.alertCtrl.create({
      title: 'Confirmar',
      message: '¿Está seguro que desea eliminar la jugada en el hoyo ' + holeNumber + '?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel'
        },
        {
          text: 'Eliminar',
          handler: () => {
            this.deleteHolePlay(holeNumber);
          }
        }
      ]
    });
    alert.present();
  };

  deleteHolePlay(holeNumber) {
    this.roundProvider.deleteHolePlay(holeNumber).then(() => {
      this.updateInfo();
    }).catch(error => {
      this.error.handle({
        error: error,
        log: 'error',
        text: 'Hubo un error al eliminar la jugada en el hoyo. Intente nuevamente más tarde.'
      });
    })
  }

  loadHoleDataClicked = (holeNumber, edit?: boolean) => {
    if (this.roundProvider.getNextStep() == 'state-initial') {
      this.toastCtrl.create({
        message: 'Antes de comenzar a cargar datos de los hoyos debe cargar el estado inicial.',
        duration: 3000
      }).present();
      return;
    }
    let newHoleModal = this.modalCtrl.create('NewHolePage', {
      hole: holeNumber,
      edit: !!edit,
      showPlayPage: this.loadHolePlayClicked
    });
    newHoleModal.onWillDismiss(() => {
      this.updateInfo();
    });
    return newHoleModal.present();
  };

  holeSlideChanged() {
    let currentIndex = this.holesSlides.getActiveIndex();
    let previousIndex = this.holesSlides.getPreviousIndex();
    if ((previousIndex < currentIndex) && (currentIndex == 9)) {
      this.summarySlides.slideNext();
    }
    if ((previousIndex > currentIndex) && (currentIndex == 8)) {
      this.summarySlides.slidePrev();
    }
  }

  loadHolePlayClicked = (holeNumber) => {
    if (this.roundProvider.getNextStep() == 'state-initial') {
      this.toastCtrl.create({
        message: 'Antes de comenzar a cargar datos de los hoyos debe cargar el estado inicial.',
        duration: 3000
      }).present();
      return;
    }
    let playModal = this.modalCtrl.create('PlayPage', {
      hole: holeNumber,
      showPlayPage: this.loadHolePlayClicked,
      showNewHolePage: this.loadHoleDataClicked
    });
    playModal.onWillDismiss(data => {
      this.updateInfo();
      if (data) {
        this.playModalDismissed(data);
      }
    });
    return playModal.present();
  };

  playModalDismissed = (data) => {
    switch (data.next) {
      case 'state-final': {
        this.loadFinalStateClicked();
        break;
      }
      case 'goroot': {
        this.navCtrl.popToRoot();
        break;
      }
      case 'newround': {
        this.navCtrl.popToRoot().then(() => {
          this.navCtrl.push('NewRoundPage');
        });
        break;
      }
    }
  };

  loadFinalStateClicked() {
    let nextStep = this.roundProvider.getNextStep();
    if ((nextStep == 'state-final') || (nextStep == 'readyToConfirm') || (nextStep == 'completed')) {
      let finalStateModal = this.modalCtrl.create('LoadFinalStatePage');
      finalStateModal.onDidDismiss(data => {
        this.updateInfo();
      });
      finalStateModal.present();
    } else {
      this.toastCtrl.create({
        message: 'El estado final sólo se puede cargar cuando ya se cargaron el estado inicial y todos los hoyos.',
        duration: 3000
      }).present();
    }
  }

  finalStateMoreClicked() {
    let actionSheet = this.actionSheetCtrl.create({
      cssClass: 'round-action-sheet',
      buttons: [
        {
          text: 'Editar estado final',
          icon: 'md-create',
          handler: () => {
            this.loadFinalStateClicked()
          }
        }
      ]
    });
    actionSheet.present();
  }

  roundMoreClicked() {
    let actionSheet = this.actionSheetCtrl.create({
      cssClass: 'round-action-sheet',
      buttons: [
        {
          text: 'Descartar',
          icon: 'ios-trash',
          handler: () => {
            let alert = this.alertCtrl.create({
              title: 'Confirmar',
              message: '¿Está seguro que desea eliminar esta vuelta?',
              buttons: [
                {
                  text: 'Cancelar',
                  role: 'cancel'
                },
                {
                  text: 'Eliminar',
                  handler: () => {
                    this.deleteRound();
                  }
                }
              ]
            });
            alert.present();
          }
        }
      ]
    });
    actionSheet.present();
  }

  deleteRound() {
    let loading = this.loadingCtrl.create();
    let timeout = setTimeout(() => {
      loading.present();
    }, 500);
    this.roundProvider.deleteRound().then(() => {
      clearTimeout(timeout);
      loading.dismissAll();
      this.navCtrl.pop();
    }).catch(error => {
      clearTimeout(timeout);
      loading.dismissAll();
      this.error.handle({error: error, log: 'error', text: 'Hubo un error al eliminar la vuelta. Intente nuevamente.'});
      this.navCtrl.pop();
    })
  }

  confirmRoundClicked() {
    let alert = this.alertCtrl.create({
      title: 'Confirmar vuelta',
      message: 'Una vez que confirme la vuelta ya no se podrán editar los datos. Verifique todos los datos ingresados.',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel'
        },
        {
          text: 'Confirmar',
          handler: () => {
            this.confirmRound();
          }
        }
      ]
    });
    alert.present();
  }

  confirmRound() {
    this.roundProvider.confirmRound().then(() => {
      this.navCtrl.popToRoot();
    }).catch(error => {
      this.error.handle({
        error: error,
        log: 'error',
        text: 'Hubo un error al confirmar la vuelta. Intente nuevamente.'
      });
    })
  }

}
