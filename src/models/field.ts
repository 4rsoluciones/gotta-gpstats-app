import {BaseEntity, Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {Round} from "./round";

@Entity('field')
export class Field extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  country: string;

  @Column()
  hole_count: number;

  @Column()
  par: string;

  @Column()
  route: string;

  @Column()
  sport_club_name: string;

  @Column()
  start_point: string;

  @Column()
  topographic_movements: string;

  @Column()
  type: string;

  @OneToOne(() => Round, round => round.field, {
    onDelete: "CASCADE"
  })
  @JoinColumn()
  round: Round;

  /**
   * Si esta propiedad es 0, la cancha se debe enviar al servicio para crear cancha por defecto.
   * Si es -1 no se debe enviar porque esta relacionada a una vuelta
   * Si ya se mando el servicio este valor es el id en el backend.
   */
  @Column({nullable: true})
  synchronized: number;

}
