import {Component, ViewChild} from '@angular/core';
import {IonicPage, Slides, ViewController} from 'ionic-angular';
import {RoundProvider, SelectOptionsProvider} from "../../providers/providers";
import {InitialState} from "../../models/initialState";

@IonicPage()
@Component({
  selector: 'page-load-initial-state',
  templateUrl: 'load-initial-state.html',
})
export class LoadInitialStatePage {
  @ViewChild(Slides) slides: Slides;
  questions: any[];
  answers: any[];

  constructor(private roundProvider: RoundProvider,
              private selectOptions: SelectOptionsProvider,
              private viewCtrl: ViewController) {
    this.selectOptions.getInitialStateQuestionnaire().then(response => {
      this.questions = response;
    });
    this.answers = [];
  }

  ionViewDidLoad() {
    this.slides.lockSwipes(true);
  }

  noClicked(i) {
    this.answers[i] = false;
    this.questionAnswered(i);
  }

  yesClicked(i) {
    this.answers[i] = true;
    this.questionAnswered(i);
  }

  assembleAnswers(): InitialState {
    let answers = new InitialState();
    for (let i = 0; i < this.questions.length; i++) {
      answers[this.questions[i].value] = this.answers[i] ? true : false;
    }
    return answers;
  }

  questionAnswered(i) {
    if (i == this.questions.length - 1) {
      this.roundProvider.saveInitialState(this.assembleAnswers());
      let nextStep = this.roundProvider.getNextStep();
      if (nextStep == 'readyToConfirm') {
        this.viewCtrl.dismiss({next: 'readyToConfirm'});
      } else {
        this.viewCtrl.dismiss();
      }
    } else {
      this.slides.lockSwipes(false);
      this.slides.slideNext();
      this.slides.lockSwipes(true);
    }
  }

}
