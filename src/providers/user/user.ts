import {Injectable} from '@angular/core';
import {ApiProvider} from "../api/api";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {DatabaseProvider} from "../database/database";

@Injectable()
export class UserProvider {
  loginStatus: Observable<boolean>;
  loginObserver: any;
  accountInfo: any;

  constructor(private api: ApiProvider,
              private db: DatabaseProvider,
              private http: HttpClient) {
    this.loginStatus = new Observable(observer => {
      this.loginObserver = observer;
    });
  }

  getLoginStatus(): Observable<boolean> {
    return this.loginStatus;
  }

  checkLoginStatus() {
    if (window.localStorage['OAUTH2']) {
      this.loginObserver.next(true);
    } else {
      this.loginObserver.next(false);
    }
  }

  hasGpstatsService(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.api.getGpstatsSubscriptionStatus().then((status: boolean) => {
        window.localStorage['gpstatsService'] = status;
        resolve(status);
      }).catch(error => {
        if ((error.error == 'timeout') && (window.localStorage['gpstatsService'])) {
          resolve(JSON.parse(window.localStorage['gpstatsService']));
        }
        reject(error);
      })
    })
  }

  login() {
    return new Promise((resolve, reject) => {
      let apiConfig = this.api.getCentralizadorConfig();
      this.http.post(apiConfig.url + 'api/oauth/token', {})
        .timeout(apiConfig.timeout)
        .toPromise()
        .then(() => {
          this.db.reset().then(() => {
            resolve();
            this.checkLoginStatus();
          }).catch(error => {
            reject(error);
          })
        })
        .catch(error => {
          reject(error);
          this.checkLoginStatus();
        })
    })
  }

  logout() {
    delete this.accountInfo;
  }

  getAccountInfo(force?: boolean): Promise<any> {
    return new Promise((resolve, reject) => {
      if (force) {
        delete this.accountInfo;
      }
      if (this.accountInfo) {
        resolve(Object.assign({}, this.accountInfo));
      } else {
        this.api.getAccount().then(response => {
          this.accountInfo = response;
          this.getCountryByState(this.accountInfo.state_id).then(countryId => {
            this.accountInfo.country = countryId;
            resolve(Object.assign({}, this.accountInfo));
          });
        }).catch(error => {
          reject(error);
        })
      }
    });
  }

  getCountryByState(stateId: number): Promise<number> {
    return new Promise(resolve => {
      this.http.get('assets/data/states.json')
        .toPromise()
        .then(response => {
          let retValue = -1;
          for (let i = 0; i < response['length']; i++) {
            if (response[i].id == stateId) {
              retValue = response[i].country_id;
            }
          }
          resolve(retValue);
        })
    })
  }

  editAccountInfo(params): Promise<any> {
    return new Promise((resolve, reject) => {
      this.api.editAccount(params).then(response => {
        this.accountInfo = response;
        this.getCountryByState(this.accountInfo.state_id).then(countryId => {
          this.accountInfo.country = countryId;
          resolve(Object.assign({}, this.accountInfo));
        });
      }).catch(error => {
        reject(error);
      })
    })
  }

}
