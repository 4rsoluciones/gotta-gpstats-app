import {Component} from '@angular/core';
import {IonicPage, ModalController} from 'ionic-angular';
import {ErrorProvider, UserProvider} from "../../providers/providers";
import {SelectOptionsProvider} from "../../providers/providers";

@IonicPage()
@Component({
  selector: 'page-account',
  templateUrl: 'account.html',
})
export class AccountPage {
  loading: boolean;
  accountInfo: any;
  countries: any[];
  states: any[];
  genders: any[];

  constructor(private modalCtrl: ModalController,
              private user: UserProvider,
              private selectOptions: SelectOptionsProvider,
              private error: ErrorProvider) {
    this.genders = [
      {
        id: 'male',
        name: 'Masculino'
      },
      {
        id: 'female',
        name: 'Femenino'
      }
    ]
  }

  ionViewCanEnter() {
    let promises = [];
    promises.push(this.selectOptions.getAccountCountries().then(response => {
      this.countries = response;
    }));
    return Promise.all(promises);
  }

  ionViewWillEnter() {
    this.update();
  }

  getCountryById(id) {
    for (let i = 0; i < this.countries.length; i++) {
      if (this.countries[i].id == id) {
        return this.countries[i];
      }
    }
    return undefined;
  }

  getStateById(id) {
    for (let i = 0; i < this.states.length; i++) {
      if (this.states[i].id == id) {
        return this.states[i];
      }
    }
    return undefined;
  }

  getGendersById(id) {
    for (let i = 0; i < this.countries.length; i++) {
      if (this.genders[i].id == id) {
        return this.genders[i];
      }
    }
    return undefined;
  }

  update() {
    this.loading = true;
    this.user.getAccountInfo().then(accountInfo => {
      this.accountInfo = accountInfo;
      this.selectOptions.getStates(accountInfo.country).then(states => {
        this.states = states;
        this.loading = false;
      });
    }).catch(error => {
      this.loading = false;
      this.error.handle({error: error, log: 'error', text: 'Ha ocurrido un error. Intente nuevamente más tarde.'});
    });
  }

  editAccountClicked() {
    let editProfileModal = this.modalCtrl.create('EditAccountPage');
    editProfileModal.onWillDismiss(() => {
      this.update();
    });
    editProfileModal.present();
  }

}
