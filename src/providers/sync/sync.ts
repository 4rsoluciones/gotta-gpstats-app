import {Injectable} from '@angular/core';
import {Network} from "@ionic-native/network";
import {ApiProvider} from "../api/api";
import {DatabaseProvider} from "../database/database";
import {Round} from "../../models/round";
import {InitialState} from "../../models/initialState";
import {FinalState} from "../../models/finalState";
import {Field} from "../../models/field";
import {Hole} from "../../models/hole";
import {ErrorProvider} from "../error/error";
import {Events} from "ionic-angular";
import {HoleToDelete} from "../../models/holeToDelete";

@Injectable()
export class SyncProvider {
  syncing: boolean;
  syncPromise: Promise<any>;
  gettingRounds: boolean;
  roundsPromise: Promise<any>;
  completedRounds: number;

  constructor(private network: Network,
              private events: Events,
              private api: ApiProvider,
              private error: ErrorProvider,
              private db: DatabaseProvider) {
    this.registerListeners();
    this.startSyncInterval();
  }

  startSyncInterval() {
    setInterval(() => {
      this.beginSync();
    }, 5 * 60 * 1000);
  }

  clear() {
    delete this.syncing;
    delete this.syncPromise;
    delete this.gettingRounds;
    delete this.roundsPromise;
    delete this.completedRounds;
  }

  registerListeners() {
    this.network.onConnect().subscribe(() => {
      this.beginSync();
    })
  }

  beginSync(): Promise<any> {
    this.error.handle({error: 'beginSync', log: 'log'});
    if (!this.syncing) {
      this.syncing = true;
      this.completedRounds = 0;
      this.syncPromise = this.sync().then(() => {
        this.syncing = false;
        if (this.completedRounds > 0) {
          this.error.presentToast(this.completedRounds == 1 ? 'Se ha sincronizado con éxito una vuelta completa' : 'Se han sincronizado con éxito ' + this.completedRounds + ' vueltas completas');
          this.events.publish('complete-round-synced');
        }
        delete this.completedRounds;
      }).catch(error => {
        this.syncing = false;
        delete this.completedRounds;
        this.error.handle({error: error, log: 'error'});
      })
    }
    return this.syncPromise;
  }

  private async sync() {
    await this.syncDefaultFields();
    await this.syncNewRounds();
    await this.syncDeletedRounds();
    await this.syncDefaultHoles();
    await this.syncEditedRoundsInfo();
    await this.syncDeletedHolePlays();
    await this.syncPreCompetitiveQuestionnaires();
    await this.syncHoles();
    await this.syncPostCompetitiveQuestionnaires();
    await this.syncNewDefaultHoles();
    await this.db.clean();
  }

  private getRounds(): Promise<any> {
    if (!this.gettingRounds) {
      this.gettingRounds = true;
      this.roundsPromise = new Promise((resolve, reject) => {
        this.db.reset().then(() => {
          this.api.getRounds({incomplete: true, limit: 50}).then(response => {
            let rounds: Array<Round> = [];
            let promises = [];
            for (let i = 0; i < response.length; i++) {
              promises.push(this.api.getRound({id: response[i].synchronized}).then(response => {
                rounds.push(response);
              }));
            }
            Promise.all(promises).then(() => {
              let promises = [];
              for (let i = 0; i < rounds.length; i++) {
                rounds[i].modified = 2;
                if (rounds[i].holes) {
                  for (let j = 0; j < rounds[i].holes.length; j++) {
                    rounds[i].holes[j].modified = 0;
                  }
                }
                promises.push(this.db.saveRound(rounds[i]));
              }
              Promise.all(promises).then(() => {
                this.syncDefaultHoles().then(() => {
                  resolve();
                  this.gettingRounds = false;
                }).catch(error => {
                  resolve();
                  this.error.handle({error: error, log: 'log'});
                  this.gettingRounds = false;
                })
              }).catch(error => {
                reject(error);
                this.gettingRounds = false;
              })
            }).catch(error => {
              reject(error);
              this.gettingRounds = false;
            })
          }).catch(error => {
            reject(error);
            this.gettingRounds = false;
          });
        }).catch(error => {
          reject(error);
          this.gettingRounds = false;
        });
      })
    }
    return this.roundsPromise;
  }

  updateIncompleteRounds(): Promise<any> {
    return new Promise((resolve, reject) => {
      if (this.network.type != 'none') {
        this.beginSync().then(() => {
          this.getRounds().then(() => {
            resolve();
          }).catch(error => {
            reject(error);
          })
        }).catch(error => {
          this.error.handle({error: error, log: 'error'});
          this.getRounds().then(() => {
            resolve();
          }).catch(error => {
            reject(error);
          })
        })
      } else {
        reject();
      }
    })
  }

  syncDefaultFields(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.db.getDefaultFields().then((fields: Field[]) => {
        let promises = [];
        for (let i = 0; i < fields.length; i++) {
          promises.push(this.syncDefaultField(fields[i]));
        }
        Promise.all(promises).then(() => {
          resolve();
        }).catch(error => {
          reject(error);
        })
      }).catch(error => {
        reject(error);
      })
    })
  }

  syncDefaultField(field: Field): Promise<Field> {
    return new Promise((resolve, reject) => {
      let id = field.id;
      delete field.id;
      delete field.synchronized;
      this.api.newCourse(field).then((field: Field) => {
        field.id = id;
        this.db.removeField(field).then(() => {
          resolve(field);
        }).catch(error => {
          reject(error);
        })
      }).catch(error => {
        // TODO segun el error, eliminar de la base de datos
        reject(error);
      })
    })
  }

  syncNewRounds(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.db.getNewRounds().then((rounds: Round[]) => {
        let promises = [];
        for (let i = 0; i < rounds.length; i++) {
          promises.push(this.syncNewRound(rounds[i]));
        }
        Promise.all(promises).then(() => {
          resolve();
        }).catch(error => {
          reject(error);
        })
      }).catch(error => {
        reject(error);
      })
    })
  }

  syncNewRound(round: Round): Promise<Round> {
    return new Promise((resolve, reject) => {
      let id = round.id;
      this.api.newRound(round).then((round: Round) => {
        round.id = id;
        round.modified = 2;
        this.db.setRoundSynchronized(round).then(() => {
          resolve(round);
        }).catch(error => {
          reject(error);
        })
      }).catch(error => {
        // TODO segun el error, ver que hacer
        reject(error);
      })
    })
  }

  syncDefaultHoles(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.db.getRoundsWithDefaultHolesToUpdate().then((rounds: Round[]) => {
        let promises = [];
        for (let i = 0; i < rounds.length; i++) {
          promises.push(this.syncDefaultHole(rounds[i]));
        }
        Promise.all(promises).then(() => {
          resolve();
        }).catch(error => {
          reject(error);
        })
      }).catch(error => {
        reject(error);
      })
    })
  }

  syncDefaultHole(round: Round): Promise<Round> {
    return new Promise((resolve, reject) => {
      this.api.getDefaultHoles(round.field).then((holes: Hole[]) => {
        let promises = [];
        for (let i = 0; i < holes.length; i++) {
          let exists = false;
          if (round.holes) {
            for (let j = 0; j < round.holes.length; j++) {
              if (round.holes[j].number == holes[i].number) {
                exists = true;
                break;
              }
            }
          }
          if (!exists) {
            holes[i].round = round;
            promises.push(this.db.saveHole(holes[i], false));
          }
        }
        Promise.all(promises).then(() => {
          round.modified = 0;
          this.db.setRoundSynchronized(round).then(() => {
            this.events.publish('sync:round-updated', {id: round.id});
            resolve();
          }).catch(error => {
            reject(error);
          })
        }).catch(error => {
          reject(error);
        })
      }).catch(error => {
        // TODO segun el error, ver que hacer
        reject(error);
      })
    })
  }

  syncDeletedRounds(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.db.getRoundsToDelete().then((rounds: Round[]) => {
        let promises = [];
        for (let i = 0; i < rounds.length; i++) {
          promises.push(this.syncDeleteRound(rounds[i]));
        }
        Promise.all(promises).then(() => {
          resolve();
        }).catch(error => {
          reject(error);
        })
      }).catch(error => {
        reject(error);
      })
    })
  }

  syncDeleteRound(round: Round): Promise<Round> {
    return new Promise((resolve, reject) => {
      if (!round.synchronized) {
        this.db.removeRound(round).then(() => {
          resolve(round);
        }).catch(error => {
          reject(error);
        })
      } else {
        this.api.deleteRound({id: round.synchronized}).then(() => {
          this.db.removeRound(round).then(() => {
            resolve(round);
          }).catch(error => {
            reject(error);
          })
        }).catch(error => {
          // TODO segun el error, ver que hacer
          reject(error);
        })
      }
    })
  }

  syncEditedRoundsInfo(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.db.getRoundsInfoToUpdate().then((rounds: Round[]) => {
        let promises = [];
        for (let i = 0; i < rounds.length; i++) {
          promises.push(this.syncEditedRoundInfo(rounds[i]));
        }
        Promise.all(promises).then(() => {
          resolve();
        }).catch(error => {
          reject(error);
        })
      }).catch(error => {
        reject(error);
      })
    })
  }

  syncEditedRoundInfo(round: Round): Promise<Round> {
    return new Promise((resolve, reject) => {
      let id = round.id;
      this.api.editRoundInfo(round).then((round: Round) => {
        round.id = id;
        this.db.setRoundSynchronized(round).then(() => {
          resolve(round);
        }).catch(error => {
          reject(error);
        })
      }).catch(error => {
        // TODO segun el error, ver que hacer
        reject(error);
      })
    })
  }

  syncPreCompetitiveQuestionnaires(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.db.getPreCompetitiveQuestionnairesToUpdate().then((initialStates: InitialState[]) => {
        let promises = [];
        for (let i = 0; i < initialStates.length; i++) {
          promises.push(this.syncPreCompetitiveQuestionnaire(initialStates[i]));
        }
        Promise.all(promises).then(() => {
          resolve();
        }).catch(error => {
          reject(error);
        })
      }).catch(error => {
        reject(error);
      })
    })
  }

  syncPreCompetitiveQuestionnaire(state: InitialState): Promise<InitialState> {
    return new Promise((resolve, reject) => {
      let id = state.id;
      this.api.setInitialState(state).then((initialState: InitialState) => {
        initialState.id = id;
        let promises = [];
        promises.push(this.db.setPreCompetitiveQuestionnaireSynchronized(initialState));
        if (initialState.round.is_completed) {
          promises.push(this.db.setRoundCompleted(initialState.round));
        }
        Promise.all(promises).then(() => {
          resolve(initialState);
        }).catch(error => {
          reject(error);
        })
      }).catch(error => {
        // TODO segun el error, ver que hacer
        reject(error);
      })
    })
  }

  syncPostCompetitiveQuestionnaires(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.db.getPostCompetitiveQuestionnairesToUpdate().then((finalStates: FinalState[]) => {
        let promises = [];
        for (let i = 0; i < finalStates.length; i++) {
          promises.push(this.syncPostCompetitiveQuestionnaire(finalStates[i]));
        }
        Promise.all(promises).then(() => {
          resolve();
        }).catch(error => {
          reject(error);
        })
      }).catch(error => {
        reject(error);
      })
    })
  }

  syncPostCompetitiveQuestionnaire(state: FinalState): Promise<FinalState> {
    return new Promise((resolve, reject) => {
      let id = state.id;
      let roundId = state.round.id;
      this.api.setFinalState(state).then((finalState: FinalState) => {
        finalState.id = id;
        let promises = [];
        promises.push(this.db.setPostCompetitiveQuestionnaireSynchronized(finalState));
        if (finalState.round.is_completed) {
          finalState.round.id = roundId;
          this.completedRounds++;
          promises.push(this.db.setRoundCompleted(finalState.round));
        }
        Promise.all(promises).then(() => {
          resolve(finalState);
        }).catch(error => {
          reject(error);
        })
      }).catch(error => {
        // TODO segun el error, ver que hacer
        reject(error);
      })
    })
  }

  syncNewDefaultHoles(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.db.getDefaultHolesToUpdate().then((holes: Hole[]) => {
        let promises = [];
        for (let i = 0; i < holes.length; i++) {
          promises.push(this.syncNewDefaultHole(holes[i]));
        }
        Promise.all(promises).then(() => {
          resolve();
        }).catch(error => {
          reject(error);
        })
      }).catch(error => {
        reject(error);
      })
    })
  }

  syncNewDefaultHole(hole: Hole): Promise<Hole> {
    return new Promise((resolve, reject) => {
      let id = hole.id;
      delete hole.id;
      delete hole.penalityShotsCount;
      delete hole.description;
      delete hole.modified;
      this.api.setDefaultHole(hole).then((hole: Hole) => {
        hole.id = id;
        this.db.removeHole(hole).then(() => {
          resolve(hole);
        }).catch(error => {
          reject(error);
        })
      }).catch(error => {
        if (error.error == 'exists') {
          this.db.removeHole(hole).then(() => {
            hole.id = id;
            resolve(hole);
          }).catch(error => {
            reject(error);
          })
        } else {
          reject(error);
        }
      })
    })
  }

  syncDeletedHolePlays(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.db.getHolePlaysToDelete().then((holesToDelete: HoleToDelete[]) => {
        let promises = [];
        for (let i = 0; i < holesToDelete.length; i++) {
          promises.push(this.syncDeletedHolePlay(holesToDelete[i]));
        }
        Promise.all(promises).then(() => {
          resolve();
        }).catch(error => {
          reject(error);
        })
      }).catch(error => {
        reject(error);
      })
    })
  }

  syncDeletedHolePlay(holeToDelete: HoleToDelete): Promise<HoleToDelete> {
    return new Promise((resolve, reject) => {
      let id = holeToDelete.id;
      this.api.deleteHolePlay(holeToDelete).then((holeToDelete: HoleToDelete) => {
        holeToDelete.id = id;
        this.db.removeHoleToDelete(holeToDelete).then(() => {
          resolve(holeToDelete);
        }).catch(error => {
          reject(error);
        })
      }).catch(error => {
        if (error.error == 'not-found') {
          holeToDelete.id = id;
          this.db.removeHoleToDelete(holeToDelete).then(() => {
            resolve(holeToDelete);
          }).catch(error => {
            reject(error);
          })
        } else {
          reject(error);
        }
      })
    })
  }

  syncHoles(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.db.getHolesToUpdate().then((holes: Hole[]) => {
        let promises = [];
        for (let i = 0; i < holes.length; i++) {
          promises.push(this.syncHole(holes[i]));
        }
        Promise.all(promises).then(() => {
          resolve();
        }).catch(error => {
          reject(error);
        })
      }).catch(error => {
        reject(error);
      })
    })
  }

  syncHole(hole: Hole): Promise<Hole> {
    return new Promise((resolve, reject) => {
      let id = hole.id;
      this.api.setHolePlay(hole).then((hole: Hole) => {
        hole.id = id;
        let promises = [];
        promises.push(this.db.setHoleSynchronized(hole));
        if (hole.round && hole.round.is_completed) {
          promises.push(this.db.setRoundCompleted(hole.round));
        }
        Promise.all(promises).then(() => {
          resolve(hole);
        }).catch(error => {
          reject(error);
        })
      }).catch(error => {
        if (error.error == 'exists') {
          hole.id = id;
          this.db.setHoleSynchronized(hole).then(() => {
            resolve(hole);
          }).catch(error => {
            reject(error);
          })
        } else {
          reject(error);
        }
      })
    })
  }

}
