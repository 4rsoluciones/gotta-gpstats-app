export const environment = {
  production: true,
  apiConfig: {
    centralizador: {
      dev: {
        clientId: 2,
        clientSecret: 'cTddvi1PBEqWUzMl8nGlGU2pHlgJv3ooYZhFIQhe',
        url: 'http://center.php56.projectsunderdev.com/',
        tokenUrl: ['api/oauth/token'],
        timeout: 15000
      },
      prod: {
        clientId: 2,
        clientSecret: 'f8oJ0ewaSsBMlDzX5WycGtxeaVb2mWPET4ARLmUF',
        url: 'https://center.andresgotta.com.ar/',
        tokenUrl: ['api/oauth/token'],
        timeout: 15000
      }
    },
    gpstats: {
      dev: {
        url: 'https://php56.projectsunderdev.com/gpstats-staging/web/',
        timeout: 30000
      },
      prod: {
        url: 'https://andresgotta.com.ar/',
        timeout: 30000
      }
    }
  }
};
