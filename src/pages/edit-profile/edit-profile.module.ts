import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {EditProfilePage} from './edit-profile';
import {HorizontalSpinnerModule} from "@4r/horizontal-spinner/horizontal-spinner.module";

@NgModule({
  declarations: [
    EditProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(EditProfilePage),
    HorizontalSpinnerModule
  ],
})
export class EditProfilePageModule {
}
