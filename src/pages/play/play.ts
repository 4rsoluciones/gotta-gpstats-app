import {Component, ViewChild} from '@angular/core';
import {
  ActionSheetController, Content, IonicPage, ModalController, NavController, NavParams,
  PopoverController, ToastController, ViewController
} from 'ionic-angular';
import {OptionsPage} from "../options/options";
import {ErrorProvider, RoundProvider, SelectOptionsProvider} from "../../providers/providers";
import {Play} from "../../models/play";
import {EmotionalQuestionnaire} from "../../models/emotionalQuestionnaire";
import {FoodQuestionnaire} from "../../models/foodQuestionnaire";
import {PhysicalQuestionnaire} from "../../models/physycalQuestionnaire";
import {Hole} from "../../models/hole";

@IonicPage()
@Component({
  selector: 'page-play',
  templateUrl: 'play.html',
})
export class PlayPage {
  @ViewChild(Content) content: Content;

  holeNumber: number;
  penalties: number;
  hole: Hole;

  questions: any[];
  clubs: any[];
  directions: any[];
  inclinationBall: any[];
  inclinationLat: any[];
  distances: any[];
  puttDistances: any[];
  results: any[];
  puttResults: any[];
  failReasons: any;
  lieHole: any[];
  lieFoot: any[];

  plays: Play[];
  currentPlay: any;
  displays: any;
  showPenalties: boolean;
  penaltiesDescription: string;

  constructor(private navCtrl: NavController,
              private viewCtrl: ViewController,
              private modalCtrl: ModalController,
              private popoverCtrl: PopoverController,
              private roundProvider: RoundProvider,
              private error: ErrorProvider,
              private selectOptions: SelectOptionsProvider,
              private actionSheetCtrl: ActionSheetController,
              private toastCtrl: ToastController,
              private navParams: NavParams) {
    this.holeNumber = this.navParams.get('hole');
    this.hole = this.roundProvider.getHole(this.holeNumber);
    this.penalties = 0;
  }

  ionViewCanEnter(): Promise<any> {
    let promises = [];
    promises.push(this.selectOptions.getBeforeQuestions().then(response => {
      this.questions = response;
    }));
    promises.push(this.selectOptions.getClubs().then(response => {
      this.clubs = response;
    }));
    promises.push(this.selectOptions.getDirections().then(response => {
      this.directions = response;
    }));
    promises.push(this.selectOptions.getInclinationBall().then(response => {
      this.inclinationBall = response;
    }));
    promises.push(this.selectOptions.getInclinationLat().then(response => {
      this.inclinationLat = response;
    }));
    promises.push(this.selectOptions.getDistances().then(response => {
      this.distances = response;
    }));
    promises.push(this.selectOptions.getPuttDistances().then(response => {
      this.puttDistances = response;
    }));
    promises.push(this.selectOptions.getResults().then(response => {
      this.results = response;
    }));
    promises.push(this.selectOptions.getPuttResults().then(response => {
      this.puttResults = response;
    }));
    promises.push(this.selectOptions.getFailReasons().then(response => {
      this.failReasons = response;
    }));
    promises.push(this.selectOptions.getLieHole().then(response => {
      this.lieHole = response;
    }));
    promises.push(this.selectOptions.getLieFoot().then(response => {
      this.lieFoot = response;
    }));
    return Promise.all(promises).then(() => {
      this.plays = [];
      this.showPenalties = false;
      this.resetCurrentPlay();
    })
  }

  resetCurrentPlay() {
    this.currentPlay = {};
    if (this.plays.length == 0) {
      this.currentPlay.type = 'strike_type.first';
    } else {
      if (this.plays[this.plays.length - 1].type == 'strike_type.putt') {
        this.currentPlay.type = 'strike_type.putt';
      } else if (this.plays[this.plays.length - 1].result != 'inside.green') {
        this.currentPlay.type = 'strike_type.other';
      } else {
        this.currentPlay.type = 'strike_type.putt';
      }
    }
    this.resetDisplays();
  }

  resetDisplays() {
    this.displays = {};
    switch (this.currentPlay.type) {
      case 'strike_type.first': {
        this.displays.clubs = this.clubs;
        this.displays.directions = this.directions;
        this.displays.distances = this.getDistances();
        this.displays.results = this.getResults();
        if (this.currentPlay.result != null) {
          this.resultClicked();
        }
        break;
      }
      case 'strike_type.other': {
        this.displays.directions = this.directions;
        this.displays.lieHole = this.lieHole;
        this.displays.lieFoot = this.lieFoot;
        this.displays.distances = this.getDistances();
        this.displays.results = this.getResults();
        if (this.currentPlay.result != null) {
          this.resultClicked();
        }
        break;
      }
      case 'strike_type.putt': {
        this.displays.inclinationBall = this.inclinationBall;
        this.displays.inclinationLat = this.inclinationLat;
        this.displays.distances = this.getDistances();
        this.displays.results = this.getResults();
        if (this.currentPlay.result != null) {
          this.resultClicked();
        }
        break;
      }
    }
  }

  getDistances() {
    switch (this.currentPlay.type) {
      case 'strike_type.first': {
        switch (this.hole.par) {
          case 3: {
            return [
              {
                "value": "less.than.100.yds",
                "title": "- de 100"
              },
              {
                "value": "btw.100.to.120.yds",
                "title": "100 a 120"
              },
              {
                "value": "btw.121.to.140.yds",
                "title": "121 a 140"
              },
              {
                "value": "btw.141.to.160.yds",
                "title": "141 a 160"
              },
              {
                "value": "btw.161.to.180.yds",
                "title": "161 a 180"
              },
              {
                "value": "btw.181.to.200.yds",
                "title": "181 a 200"
              },
              {
                "value": "btw.201.to.220.yds",
                "title": "201 a 220"
              },
              {
                "value": "more.than.220.yds",
                "title": "+ de 220"
              }
            ];
          }
          case 4: {
            return [
              {
                "value": "less.than.100.yds",
                "title": "- de 100"
              },
              {
                "value": "btw.100.to.120.yds",
                "title": "100 a 120"
              },
              {
                "value": "btw.121.to.140.yds",
                "title": "121 a 140"
              },
              {
                "value": "btw.141.to.160.yds",
                "title": "141 a 160"
              },
              {
                "value": "btw.161.to.180.yds",
                "title": "161 a 180"
              },
              {
                "value": "btw.181.to.200.yds",
                "title": "181 a 200"
              },
              {
                "value": "btw.201.to.220.yds",
                "title": "201 a 220"
              },
              {
                "value": "btw.221.to.240.yds",
                "title": "221 a 240"
              },
              {
                "value": "btw.241.to.260.yds",
                "title": "241 a 260"
              },
              {
                "value": "btw.261.to.280.yds",
                "title": "261 a 280"
              },
              {
                "value": "btw.281.to.300.yds",
                "title": "281 a 300"
              },
              {
                "value": "btw.301.to.320.yds",
                "title": "301 a 320"
              },
              {
                "value": "more.than.320.yds",
                "title": "+ de 320"
              }
            ];
          }
          case 5: {
            return [
              {
                "value": "less.than.100.yds",
                "title": "- de 100"
              },
              {
                "value": "btw.100.to.120.yds",
                "title": "100 a 120"
              },
              {
                "value": "btw.121.to.140.yds",
                "title": "121 a 140"
              },
              {
                "value": "btw.141.to.160.yds",
                "title": "141 a 160"
              },
              {
                "value": "btw.161.to.180.yds",
                "title": "161 a 180"
              },
              {
                "value": "btw.181.to.200.yds",
                "title": "181 a 200"
              },
              {
                "value": "btw.201.to.220.yds",
                "title": "201 a 220"
              },
              {
                "value": "btw.221.to.240.yds",
                "title": "221 a 240"
              },
              {
                "value": "btw.241.to.260.yds",
                "title": "241 a 260"
              },
              {
                "value": "btw.261.to.280.yds",
                "title": "261 a 280"
              },
              {
                "value": "btw.281.to.300.yds",
                "title": "281 a 300"
              },
              {
                "value": "btw.301.to.320.yds",
                "title": "301 a 320"
              },
              {
                "value": "more.than.320.yds",
                "title": "+ de 320"
              }
            ];
          }
        }
        break;
      }
      case 'strike_type.other': {
        switch (this.hole.par) {
          case 3: {
            return [
              {
                "value": "less.than.20.yds",
                "title": "- de 20"
              },
              {
                "value": "btw.21.to.40.yds",
                "title": "21 a 40"
              },
              {
                "value": "btw.41.to.60.yds",
                "title": "41 a 60"
              },
              {
                "value": "btw.61.to.80.yds",
                "title": "61 a 80"
              },
              {
                "value": "btw.81.to.100.yds",
                "title": "81 a 100"
              },
              {
                "value": "btw.101.to.120.yds",
                "title": "101 a 120"
              },
              {
                "value": "more.than.120.yds",
                "title": "+ de 120"
              }
            ];
          }
          case 4: {
            return this.distances;
          }
          case 5: {
            return [
              {
                "value": "less.than.20.yds",
                "title": "- de 20"
              },
              {
                "value": "btw.21.to.40.yds",
                "title": "21 a 40"
              },
              {
                "value": "btw.41.to.60.yds",
                "title": "41 a 60"
              },
              {
                "value": "btw.61.to.180.yds",
                "title": "61 a 80"
              },
              {
                "value": "btw.81.to.100.yds",
                "title": "81 a 100"
              },
              {
                "value": "btw.101.to.120.yds",
                "title": "101 a 120"
              },
              {
                "value": "btw.121.to.140.yds",
                "title": "121 a 140"
              },
              {
                "value": "btw.141.to.160.yds",
                "title": "141 a 160"
              },
              {
                "value": "btw.161.to.180.yds",
                "title": "161 a 180"
              },
              {
                "value": "btw.181.to.200.yds",
                "title": "181 a 200"
              },
              {
                "value": "more.than.200.yds",
                "title": "+ de 200"
              }
            ];
          }
        }
        break;
      }
      case 'strike_type.putt': {
        return this.puttDistances;
      }
    }
  }

  getResults() {
    switch (this.currentPlay.type) {
      case 'strike_type.first': {
        switch (this.hole.par) {
          case 3: {
            return [
              {
                "value": "inside.green",
                "img": "assets/imgs/jugada/resultado_dentro-green.png",
                "title": "Dentro del green"
              },
              {
                "value": "outside.green",
                "img": "assets/imgs/jugada/resultado_dentro-fairway.png",
                "title": "Fuera del green"
              },
              {
                "value": "outside.green.into.the.bunker",
                "img": "assets/imgs/jugada/resultado_fuera-fairway-bunker.png",
                "title": "Fuera del green en el bunker"
              },
              {
                "value": "hole.out",
                "img": "assets/imgs/jugada/resultado_embocada.png",
                "title": "Embocada"
              },
              {
                "value": "ball.in.hazard",
                "img": "assets/imgs/jugada/resultado_hazard.png",
                "title": "Pelota en hazard de agua"
              },
              {
                "value": "out.of.limits",
                "img": "assets/imgs/jugada/resultado_fuera-limite.png",
                "title": "Fuera del límite"
              }
            ];
          }
          case 4: {
            return this.results;
          }
          case 5: {
            return this.results;
          }
        }
        break;
      }
      case 'strike_type.other': {
        //si es par 5 y es el segundo golpe le muestro mas posibilidades de resultados
        if ((this.hole.par == 5) && (this.plays.length == 1)) {
          return [
            {
              "value": "inside.fairway",
              "img": "assets/imgs/jugada/resultado_dentro-fairway.png",
              "title": "Dentro del fairway"
            },
            {
              "value": "inside.green",
              "img": "assets/imgs/jugada/resultado_dentro-green.png",
              "title": "Dentro del green"
            },
            {
              "value": "outside.green",
              "img": "assets/imgs/jugada/resultado_dentro-fairway.png",
              "title": "Fuera del green"
            },
            {
              "value": "cross.bunker.in.fairway",
              "img": "assets/imgs/jugada/resultado_cross-bunker-fairway.png",
              "title": "Cross bunker en el fairway"
            },
            {
              "value": "outside.green.into.the.bunker",
              "img": "assets/imgs/jugada/resultado_fuera-fairway-bunker.png",
              "title": "Fuera del green en el bunker"
            },
            {
              "value": "hole.out",
              "img": "assets/imgs/jugada/resultado_embocada.png",
              "title": "Embocada"
            },
            {
              "value": "outside.fairway",
              "img": "assets/imgs/jugada/resultado_fuera-fairway.png",
              "title": "Fuera del fairway"
            },
            {
              "value": "outside.fairway.into.the.bunker",
              "img": "assets/imgs/jugada/resultado_fuera-fairway-bunker.png",
              "title": "Fuera del fairway en el bunker"
            },
            {
              "value": "ball.in.hazard",
              "img": "assets/imgs/jugada/resultado_hazard.png",
              "title": "Pelota en hazard de agua"
            },
            {
              "value": "out.of.limits",
              "img": "assets/imgs/jugada/resultado_fuera-limite.png",
              "title": "Fuera del límite"
            }
          ];
        } else {
          return [
            {
              "value": "inside.green",
              "img": "assets/imgs/jugada/resultado_dentro-green.png",
              "title": "Dentro del green"
            },
            {
              "value": "outside.green",
              "img": "assets/imgs/jugada/resultado_dentro-fairway.png",
              "title": "Fuera del green"
            },
            {
              "value": "outside.green.into.the.bunker",
              "img": "assets/imgs/jugada/resultado_fuera-fairway-bunker.png",
              "title": "Fuera del green en el bunker"
            },
            {
              "value": "hole.out",
              "img": "assets/imgs/jugada/resultado_embocada.png",
              "title": "Embocada"
            },
            {
              "value": "ball.in.hazard",
              "img": "assets/imgs/jugada/resultado_hazard.png",
              "title": "Pelota en hazard de agua"
            },
            {
              "value": "out.of.limits",
              "img": "assets/imgs/jugada/resultado_fuera-limite.png",
              "title": "Fuera del límite"
            }
          ];
        }
      }
      case 'strike_type.putt': {
        return this.puttResults;
      }
    }
  }

  ask(question) {
    let questionPopover = this.popoverCtrl.create(OptionsPage, {options: question.options});
    questionPopover.onDidDismiss(answer => {
      if (answer) {
        this.currentPlay[question.value] = answer;
      }
    });
    questionPopover.present();
  }

  inputClicked(event: MouseEvent) {
    setTimeout(() => {
      event.toElement.closest('horizontal-spinner').scrollIntoView(true);
    }, 0);
  }

  resultClicked(event?) {
    if (event) {
      this.inputClicked(event);
    }
    let okResults = [];
    switch (this.currentPlay.type) {
      case 'strike_type.first': {
        switch (this.hole.par) {
          case 3: {
            okResults = ['inside.green', 'hole.out'];
            break;
          }
          case 4: {
            okResults = ['cross.bunker.in.fairway', 'inside.fairway', 'inside.green', 'hole.out'];
            break;
          }
          case 5: {
            okResults = ['cross.bunker.in.fairway', 'inside.fairway', 'inside.green', 'hole.out'];
            break;
          }
        }
        break;
      }
      case 'strike_type.other': {
        okResults = ['inside.green', 'hole.out'];
        break;
      }
      case 'strike_type.putt': {
        okResults = ['hole.out'];
        break;
      }
    }
    if ((this.currentPlay.result != null) && (okResults.indexOf(this.displays.results[this.currentPlay.result].value) == -1)) {
      this.displays.failReasons = this.getFailReasons(this.currentPlay.type);
    } else {
      this.displays.failReasons = null;
      delete this.currentPlay.failReason;
    }
    this.calcPenalties();
    /*if (this.isPenalty(this.displays.results[this.currentPlay.result].value)) {
      this.penalties++;
    }*/
  }

  getFailReasons(playType) {
    switch (playType) {
      case 'strike_type.first': {
        if (['outside.green', 'outside.green.into.the.bunker'].indexOf(this.displays.results[this.currentPlay.result].value) > -1) {
          return this.failReasons['four'];
        }
        if (['ball.in.hazard', 'out.of.limits', 'outside.fairway', 'outside.fairway.into.the.bunker'].indexOf(this.displays.results[this.currentPlay.result].value) > -1) {
          return this.failReasons['two'];
        }
        break;
      }
      case 'strike_type.other': {
        if (['outside.green', 'cross.bunker.in.fairway', 'outside.green.into.the.bunker', 'outside.fairway', 'outside.fairway.into.the.bunker', 'ball.in.hazard', 'out.of.limits'].indexOf(this.displays.results[this.currentPlay.result].value) > -1) {
          return this.failReasons['four'];
        }
        break;
      }
      case 'strike_type.putt': {
        return this.failReasons['putt'];
      }
    }
  }

  deleteClicked() {
    this.swipePrev().then(() => {
      let currPlay = this.plays.pop();
      this.currentPlay.type = currPlay.type;
      this.resetDisplays();
      this.currentPlay = this.assembleCurrentPlay(currPlay);
      this.calcPenalties();
      this.resetDisplays();
    })
  }

  continueButtonClicked() {
    if (this.playValid()) {
      this.plays.push(this.assemblePlay(this.currentPlay));
      if (this.plays[this.plays.length - 1].result == 'hole.out') {
        this.swipePenaltiesIn();
      } else {
        this.swipeNext().then(() => {
          this.resetCurrentPlay();
          this.calcPenalties();
        })
      }
    }
  }

  assemblePlay(play): Play {
    let ret: Play = new Play();
    ret.order = this.plays.length + 1;
    ret.type = play.type;
    if (play.emotionalQuestionnaire) {
      let values = [];
      for (let i = 0; i < play.emotionalQuestionnaire.length; i++) {
        values.push(play.emotionalQuestionnaire[i].value);
      }
      let eq = new EmotionalQuestionnaire();
      eq.feelsUptight = (values.indexOf('feelsUptight') > -1);
      eq.feelsDistrustful = (values.indexOf('feelsDistrustful') > -1);
      eq.feelsAnnoyed = (values.indexOf('feelsAnnoyed') > -1);
      eq.feelsFrustrated = (values.indexOf('feelsFrustrated') > -1);
      eq.feelsTrustful = (values.indexOf('feelsTrustful') > -1);
      eq.feelsEnjoying = (values.indexOf('feelsEnjoying') > -1);
      eq.feelsFocused = (values.indexOf('feelsFocused') > -1);
      ret.emotionalQuestionnaire = eq;
    }
    if (play.foodQuestionnaire) {
      let values = [];
      for (let i = 0; i < play.foodQuestionnaire.length; i++) {
        values.push(play.foodQuestionnaire[i].value);
      }
      let fq = new FoodQuestionnaire();
      fq.drankWater = (values.indexOf('drankWater') > -1);
      fq.drankSportsDrink = (values.indexOf('drankSportsDrink') > -1);
      fq.swallowedCerealOrNuts = (values.indexOf('swallowedCerealOrNuts') > -1);
      fq.swallowedSandwich = (values.indexOf('swallowedSandwich') > -1);
      fq.swallowedFruit = (values.indexOf('swallowedFruit') > -1);
      ret.foodQuestionnaire = fq;
    }
    if (play.physicalQuestionnaire) {
      let values = [];
      for (let i = 0; i < play.physicalQuestionnaire.length; i++) {
        values.push(play.physicalQuestionnaire[i].value);
      }
      let pq = new PhysicalQuestionnaire();
      pq.feelsEnergetic = (values.indexOf('feelsEnergetic') > -1);
      pq.feelsGettingTired = (values.indexOf('feelsGettingTired') > -1);
      pq.feelsPainful = (values.indexOf('feelsPainful') > -1);
      pq.feelsTired = (values.indexOf('feelsTired') > -1);
      pq.feelsExhausted = (values.indexOf('feelsExhausted') > -1);
      ret.physicalQuestionnaire = pq;
    }
    if (play.club != null) {
      ret.club = this.displays.clubs[play.club].value;
    }
    if (play.direction != null) {
      ret.ballFlight = this.displays.directions[play.direction].value;
    }
    if (play.lieHole != null) {
      ret.strikeLies = [];
      ret.strikeLies.push(this.displays.lieHole[play.lieHole].value);
      if (play.lieHole != null && play.lieFoot != null) {
        ret.strikeLies.push(this.displays.lieFoot[play.lieFoot].value);
      }
    }
    if (play.inclinationBall != null || play.inclinationLat != null) {
      ret.landSlopes = [];
    }
    if (play.inclinationBall != null) {
      ret.landSlopes.push(this.displays.inclinationBall[play.inclinationBall].value);
    }
    if (play.inclinationLat != null) {
      ret.landSlopes.push(this.displays.inclinationLat[play.inclinationLat].value);
    }
    if (play.distance != null) {
      ret.distance = this.displays.distances[play.distance].value;
    }
    if (play.result != null) {
      ret.result = this.displays.results[play.result].value;
    }
    if (play.failReason != null) {
      ret.reason = this.displays.failReasons[play.failReason].value;
    }
    return ret;
  }

  getIndexByValue(array, value) {
    for (let i = 0; i < array.length; i++) {
      if (array[i].value == value) {
        return i;
      }
    }
    return -1;
  }

  assembleCurrentPlay(play: Play) {
    let ret: any = {};
    ret.type = play.type;
    if (play.emotionalQuestionnaire) {
      ret.emotionalQuestionnaire = [];
      let questions = [];
      for (let i = 0; i < this.questions.length; i++) {
        if (this.questions[i].value == 'emotionalQuestionnaire') {
          questions = this.questions[i].options;
        }
      }
      for (let i = 0; i < questions.length; i++) {
        if (play.emotionalQuestionnaire[questions[i].value]) {
          ret.emotionalQuestionnaire.push(questions[i]);
        }
      }
    }
    if (play.foodQuestionnaire) {
      ret.foodQuestionnaire = [];
      let questions = [];
      for (let i = 0; i < this.questions.length; i++) {
        if (this.questions[i].value == 'foodQuestionnaire') {
          questions = this.questions[i].options;
        }
      }
      for (let i = 0; i < questions.length; i++) {
        if (play.foodQuestionnaire[questions[i].value]) {
          ret.foodQuestionnaire.push(questions[i]);
        }
      }
    }
    if (play.physicalQuestionnaire) {
      ret.physicalQuestionnaire = [];
      let questions = [];
      for (let i = 0; i < this.questions.length; i++) {
        if (this.questions[i].value == 'physicalQuestionnaire') {
          questions = this.questions[i].options;
        }
      }
      for (let i = 0; i < questions.length; i++) {
        if (play.physicalQuestionnaire[questions[i].value]) {
          ret.physicalQuestionnaire.push(questions[i]);
        }
      }
    }
    if (play.club) {
      ret.club = this.getIndexByValue(this.displays.clubs, play.club);
    }
    if (play.ballFlight) {
      ret.direction = this.getIndexByValue(this.displays.directions, play.ballFlight);
    }
    if (play.strikeLies) {
      for (let i = 0; i < play.strikeLies.length; i++) {
        if (this.getIndexByValue(this.displays.lieHole, play.strikeLies[i]) > -1) {
          ret.lieHole = this.getIndexByValue(this.displays.lieHole, play.strikeLies[i]);
        }
        if (this.getIndexByValue(this.displays.lieFoot, play.strikeLies[i]) > -1) {
          ret.lieFoot = this.getIndexByValue(this.displays.lieFoot, play.strikeLies[i]);
        }
      }
    }
    if (play.landSlopes) {
      for (let i = 0; i < play.landSlopes.length; i++) {
        if (this.getIndexByValue(this.displays.inclinationBall, play.landSlopes[i]) > -1) {
          ret.inclinationBall = this.getIndexByValue(this.displays.inclinationBall, play.landSlopes[i]);
        }
        if (this.getIndexByValue(this.displays.inclinationLat, play.landSlopes[i]) > -1) {
          ret.inclinationLat = this.getIndexByValue(this.displays.inclinationLat, play.landSlopes[i]);
        }
      }
    }
    if (play.distance) {
      ret.distance = this.getIndexByValue(this.displays.distances, play.distance);
    }
    if (play.result) {
      ret.result = this.getIndexByValue(this.displays.results, play.result);
      this.currentPlay.result = ret.result;
      this.resultClicked();
    }
    if (play.reason) {
      ret.failReason = this.getIndexByValue(this.displays.failReasons, play.reason);
    }
    return ret;
  }

  isPenalty(v): boolean {
    let penResults = [
      'ball.in.hazard',
      'out.of.limits'
    ];
    return penResults.indexOf(v) > -1;
  }

  calcPenalties() {
    let penalties = 0;
    for (let i = 0; i < this.plays.length; i++) {
      if (this.isPenalty(this.plays[i].result)) {
        penalties++;
      }
    }
    this.penalties = penalties;
  }

  swipePenaltiesIn() {
    let playElem = document.getElementById('play');
    playElem.style.transitionDuration = '.15s';
    playElem.style.transform = 'translateX(-120%)';
    let penElem = document.getElementById('pen');
    penElem.style.transform = 'translateX(120%)';
    this.showPenalties = true;
    setTimeout(() => {
      penElem.style.transitionDuration = '.15s';
      penElem.style.transform = 'translateX(0)';
      this.content.resize();
    }, 150);
  }

  swipePenaltiesOut() {
    let penElem = document.getElementById('pen');
    penElem.style.transitionDuration = '.15s';
    penElem.style.transform = 'translateX(120%)';
    let playElem = document.getElementById('play');
    this.showPenalties = false;
    setTimeout(() => {
      playElem.style.transitionDuration = '.15s';
      playElem.style.transform = 'translateX(0)';
      this.content.resize();
    }, 150);
  }

  swipePrev(): Promise<any> {
    return new Promise((resolve) => {
      let playElem = document.getElementById('play');
      playElem.style.transitionDuration = '.15s';
      playElem.style.transform = 'translateX(120%)';
      setTimeout(() => {
        resolve();
        playElem.style.transitionDuration = '0s';
        playElem.style.transform = 'translateX(-120%)';
        this.content.scrollToTop(0);
        setTimeout(() => {
          playElem.style.transitionDuration = '.15s';
          playElem.style.transform = 'translateX(0)';
        }, 50);
      }, 150);
    })
  }

  swipeNext(): Promise<any> {
    return new Promise((resolve) => {
      let playElem = document.getElementById('play');
      playElem.style.transitionDuration = '.15s';
      playElem.style.transform = 'translateX(-120%)';
      setTimeout(() => {
        resolve();
        playElem.style.transitionDuration = '0s';
        playElem.style.transform = 'translateX(120%)';
        this.content.scrollToTop(0);
        let spinners = document.getElementsByClassName('hs-container');
        for (let i = 0; i < spinners.length; i++) {
          spinners[i].scrollTo(0, 0);
        }
        setTimeout(() => {
          playElem.style.transitionDuration = '.15s';
          playElem.style.transform = 'translateX(0)';
        }, 50);
      }, 150);
    })
  }

  playValid(): boolean {
    switch (this.currentPlay.type) {
      case 'strike_type.first': {
        if (!this.currentPlay.club && this.currentPlay.club !== 0) {
          this.presentToast('Debe seleccionar el palo');
          return false;
        }
        if (!this.currentPlay.direction && this.currentPlay.direction !== 0) {
          this.presentToast('Debe seleccionar el vuelo de la pelota');
          return false;
        }
        if (!this.currentPlay.distance && this.currentPlay.distance !== 0) {
          this.presentToast('Debe seleccionar la distancia');
          return false;
        }
        if (!this.currentPlay.result && this.currentPlay.result !== 0) {
          this.presentToast('Debe seleccionar el resultado');
          return false;
        }
        if (this.displays.failReasons && !this.currentPlay.failReason && this.currentPlay.failReason !== 0) {
          this.presentToast('Debe seleccionar el motivo del fallo');
          return false;
        }
        return true;
      }
      case 'strike_type.other': {
        if (!this.currentPlay.direction && this.currentPlay.direction !== 0) {
          this.presentToast('Debe seleccionar el vuelo de la pelota');
          return false;
        }
        if (!this.currentPlay.lieFoot && this.currentPlay.lieFoot !== 0) {
          this.presentToast('Debe seleccionar el lie de tiro respecto de los pies');
          return false;
        }
        if (!this.currentPlay.lieHole && this.currentPlay.lieHole !== 0) {
          this.presentToast('Debe seleccionar el lie de tiro respecto del hoyo');
          return false;
        }
        if (!this.currentPlay.distance && this.currentPlay.distance !== 0) {
          this.presentToast('Debe seleccionar la distancia del golpe ejecutado');
          return false;
        }
        if (!this.currentPlay.result && this.currentPlay.result !== 0) {
          this.presentToast('Debe seleccionar el resultado');
          return false;
        }
        if (this.displays.failReasons && !this.currentPlay.failReason && this.currentPlay.failReason !== 0) {
          this.presentToast('Debe seleccionar el motivo del fallo');
          return false;
        }
        return true;
      }
      case 'strike_type.putt': {
        if (!this.currentPlay.inclinationBall && this.currentPlay.inclinationBall !== 0) {
          this.presentToast('Debe seleccionar la inclinación del terreno');
          return false;
        }
        if (!this.currentPlay.inclinationLat && this.currentPlay.inclinationLat !== 0) {
          this.presentToast('Debe seleccionar la inclinación del terreno');
          return false;
        }
        if (!this.currentPlay.distance && this.currentPlay.distance !== 0) {
          this.presentToast('Debe seleccionar la distancia');
          return false;
        }
        if (!this.currentPlay.result && this.currentPlay.result !== 0) {
          this.presentToast('Debe seleccionar el resultado');
          return false;
        }
        if (this.displays.failReasons && !this.currentPlay.failReason && this.currentPlay.failReason !== 0) {
          this.presentToast('Debe seleccionar el motivo del fallo');
          return false;
        }
        return true;
      }
    }
  }

  penaltiesValid(): boolean {
    console.log('penaltiesValid');
    //TODO
    return true;
  }

  saveClicked() {
    if (this.penaltiesValid()) {
      let hole = new Hole();
      hole.strikes = this.plays;
      hole.penalityShotsCount = this.penalties;
      hole.description = this.penaltiesDescription;
      hole.design = this.hole.design;
      this.hole.id ? (hole.id = this.hole.id) : '';
      hole.number = this.hole.number;
      hole.par = this.hole.par;
      this.roundProvider.setHoleData(hole);
      this.presentHoleFinishedAlert();
    }
  }

  closeClicked() {
    let actionSheet = this.actionSheetCtrl.create({
      cssClass: 'round-action-sheet',
      buttons: [
        {
          text: 'Volver a empezar',
          icon: 'md-refresh',
          handler: () => {
            this.restart()
          }
        }, {
          text: 'Descartar',
          icon: 'md-close',
          handler: () => {
            this.navCtrl.pop();
          }
        }
      ]
    });
    actionSheet.present();
  }

  restart() {
    this.penalties = 0;
    this.penaltiesDescription = '';
    this.plays = [];
    this.resetCurrentPlay();
    this.swipePenaltiesOut();
  }

  presentHoleFinishedAlert() {
    let nextStep = this.roundProvider.getNextStep();
    switch (nextStep.split('-')[0]) {
      case 'hole': {
        let alertModal = this.modalCtrl.create('AlertPage', {
          image: 'assets/imgs/iconos/jugada-cargada.png',
          title: '¡Jugada cargada!',
          subtitle: 'Ahora podés seguir cargando los siguientes hoyos.',
          okButton: 'CONTINUAR CARGANDO',
          cancelButton: 'Continuar más tarde.'
        });
        alertModal.onDidDismiss(data => {
          if (data) {
            let holeNumber = parseInt(nextStep.split('-')[1]) + 1;
            if (this.roundProvider.getHole(holeNumber).design) {
              let showPlayPage = this.navParams.get('showPlayPage');
              showPlayPage(holeNumber).then(() => {
                this.viewCtrl.dismiss();
              }).catch(error => {
                this.error.handle({
                  error: error,
                  log: 'error',
                  text: 'Ha ocurrido un error. Intente nuevamente más tarde.'
                });
                this.viewCtrl.dismiss();
              })
            } else {
              let showNewHolePage = this.navParams.get('showNewHolePage');
              showNewHolePage(holeNumber).then(() => {
                this.viewCtrl.dismiss();
              }).catch(error => {
                this.error.handle({
                  error: error,
                  log: 'error',
                  text: 'Ha ocurrido un error. Intente nuevamente más tarde.'
                });
                this.viewCtrl.dismiss();
              })
            }
          } else {
            this.viewCtrl.dismiss();
          }
        });
        alertModal.present();
        break;
      }
      case 'state': {
        let state = nextStep.split('-')[1];
        let alertModal = this.modalCtrl.create('AlertPage', {
          image: 'assets/imgs/iconos/jugada-cargada.png',
          title: '¡Ronda cargada!',
          subtitle: (state == 'final') ? 'Sólo falta un paso para completar la vuelta.\nCompletá ahora tu estado final.' : 'Sólo falta un paso para completar la vuelta.\nCompletá ahora tu estado inicial.',
          okButton: 'CONTINUAR AHORA',
          cancelButton: 'Continuar más tarde.'
        });
        alertModal.onDidDismiss(data => {
          if (data) {
            this.viewCtrl.dismiss({next: nextStep});
          } else {
            this.navCtrl.pop();
          }
        });
        alertModal.present();
        break;
      }
      case 'readyToConfirm': {
        this.navCtrl.pop();
      }
    }
  }

  presentToast(msg: string) {
    this.toastCtrl.create({
      message: msg,
      duration: 3000
    }).present();
  }

  newArray(length: number) {
    return new Array(length);
  }

}
