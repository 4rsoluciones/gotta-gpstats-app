import {BaseEntity, Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity('hole-to-delete')
export class HoleToDelete extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  number: number;

  @Column()
  round: number;

}
