import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {BenefitsPage} from './benefits';
import {PipesModule} from "../../pipes/pipes.module";

@NgModule({
  declarations: [
    BenefitsPage,
  ],
  imports: [
    IonicPageModule.forChild(BenefitsPage),
    PipesModule
  ],
})
export class BenefitsPageModule {
}
