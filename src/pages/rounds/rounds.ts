import {Component, ViewChild} from '@angular/core';
import {
  ActionSheetButton,
  ActionSheetController,
  AlertController,
  Content,
  Events,
  InfiniteScroll,
  IonicPage,
  LoadingController,
  NavController,
  Refresher
} from 'ionic-angular';
import {ErrorProvider, RoundProvider, RoundsProvider} from "../../providers/providers";

@IonicPage()
@Component({
  selector: 'page-rounds',
  templateUrl: 'rounds.html',
})
export class RoundsPage {
  @ViewChild(Refresher) refresher: Refresher;
  @ViewChild(Content) content: Content;
  @ViewChild(InfiniteScroll) infiniteScroll: InfiniteScroll;
  loading: boolean;
  rounds: any[];
  limit: number = 20;

  constructor(private navCtrl: NavController,
              private events: Events,
              private error: ErrorProvider,
              private roundsProvider: RoundsProvider,
              private roundProvider: RoundProvider,
              private loadingCtrl: LoadingController,
              private alertCtrl: AlertController,
              private actionSheetCtrl: ActionSheetController) {
    this.rounds = [];
    this.events.subscribe('complete-round-synced', () => {
      this.update(true);
    })
  }

  ionViewDidEnter() {
    this.loading = true;
    this.update().then(() => {
      this.content.resize();
      this.loading = false;
    }).catch(() => {
      this.content.resize();
      this.loading = false;
    })
  }

  update(force?: boolean): Promise<any> {
    return new Promise((resolve, reject) => {
      let promises = [];
      this.rounds = [];
      promises.push(this.roundsProvider.getIncompleteRounds({}).then(response => {
        this.rounds = response.concat(this.rounds);
      }));
      promises.push(this.roundsProvider.getRounds({limit: this.limit, force: force}).then(response => {
        this.rounds = this.rounds.concat(response);
      }));
      Promise.all(promises).then(() => {
        this.content.resize();
        resolve();
      }).catch(error => {
        this.error.handle(error);
        reject(error);
      })
    })
  }

  getMoreRounds(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.roundsProvider.getMoreRounds().then(response => {
        this.rounds = this.rounds.concat(response);
        this.infiniteScroll.enable(response.length > 0);
        resolve();
      }).catch(error => {
        this.error.handle(error);
        reject(error);
      })
    })
  }

  doRefresh(refresher) {
    this.update(true).then(() => {
      refresher.complete();
    }).catch(() => {
      refresher.complete();
    })
  }

  doInfinite() {
    return this.getMoreRounds();
  }

  roundOptionsClicked(round) {
    let buttons: ActionSheetButton[] = [];
    buttons.push({
      text: (!round.is_completed_locally ? 'Continuar carga' : 'Ver vuelta'),
      icon: (!round.is_completed_locally ? 'md-create' : 'ios-information-circle'),
      handler: () => {
        this.editClicked(round)
      }
    });
    buttons.push({
      text: 'Descartar',
      icon: 'ios-trash',
      handler: () => {
        let alert = this.alertCtrl.create({
          title: 'Confirmar',
          message: '¿Está seguro que desea eliminar esta vuelta?',
          buttons: [
            {
              text: 'Cancelar',
              role: 'cancel'
            },
            {
              text: 'Eliminar',
              handler: () => {
                this.discardClicked(round)
              }
            }
          ]
        });
        alert.present();
      }
    });
    let actionSheet = this.actionSheetCtrl.create({
      cssClass: 'round-action-sheet',
      buttons: buttons
    });
    actionSheet.present();
  }

  newRoundClicked() {
    this.navCtrl.push('NewRoundPage').catch(error => {
      this.error.handle({error: error, text: 'Ha ocurrido un error. Intente nuevamente más tarde.', log: 'error'});
    })
  }

  completeProfileClicked() {
    this.navCtrl.push('MyDataPage').catch(error => {
      this.error.handle({error: error, text: 'Ha ocurrido un error. Intente nuevamente más tarde.', log: 'error'});
    })
  }

  roundClicked(round) {
    this.editClicked(round);
  }

  editClicked(round) {
    let loading = this.loadingCtrl.create();
    let timeout = setTimeout(() => {
      loading.present();
    }, 500);
    this.roundProvider.setActiveRound(round).then(() => {
      this.navCtrl.push('RoundPage').then(() => {
        clearTimeout(timeout);
        loading.dismissAll();
      }).catch(error => {
        clearTimeout(timeout);
        loading.dismissAll();
        this.error.handle({error: error, text: 'Ha ocurrido un error. Intente nuevamente más tarde.', log: 'error'});
      })
    }).catch(error => {
      loading.dismissAll();
      this.error.handle(error);
    })
  }

  discardClicked(round) {
    let loading = this.loadingCtrl.create();
    let timeout = setTimeout(() => {
      loading.present();
    }, 500);
    this.roundsProvider.deleteRound(round).then(() => {
      clearTimeout(timeout);
      loading.dismissAll();
      this.ionViewDidEnter();
    }).catch(error => {
      clearTimeout(timeout);
      loading.dismissAll();
      this.error.handle({error: error, log: 'error', text: 'Hubo un error al eliminar la vuelta. Intente nuevamente.'});
      this.ionViewDidEnter();
    })
  }

}
