import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {NewRoundPage} from './new-round';
import {HorizontalSpinnerModule} from "@4r/horizontal-spinner/horizontal-spinner.module";
import {ComponentsModule} from "../../components/components.module";

@NgModule({
  declarations: [
    NewRoundPage,
  ],
  imports: [
    IonicPageModule.forChild(NewRoundPage),
    HorizontalSpinnerModule,
    ComponentsModule
  ],
})
export class NewRoundPageModule {
}
