import {Component} from '@angular/core';

@Component({
  selector: 'near-me-card',
  templateUrl: 'near-me-card.html',
  inputs: ['place']
})
export class NearMeCardComponent {
  place: any;

  constructor() {

  }

}
