import {Component} from '@angular/core';
import {IonicPage} from 'ionic-angular';
import {ComparativesProvider, ErrorProvider} from "../../providers/providers";

@IonicPage()
@Component({
  selector: 'page-comparatives',
  templateUrl: 'comparatives.html',
})
export class ComparativesPage {
  loading: boolean;
  comparatives: any;
  width: number;

  constructor(private comparativesProvider: ComparativesProvider,
              private error: ErrorProvider) {

  }

  ionViewWillEnter() {
    this.loading = true;
    this.update().then(() => {
      this.loading = false;
    }).catch(() => {
      this.loading = false;
    });
  }

  ionViewDidEnter() {
    this.getWidth();
  }

  getWidth() {
    let style = window.getComputedStyle(document.getElementsByTagName('ion-card-content')[0]);
    let width = parseFloat(style.getPropertyValue('width').split('px')[0]);
    let pl = parseFloat(style.getPropertyValue('padding-left').split('px')[0]);
    let pr = parseFloat(style.getPropertyValue('padding-right').split('px')[0]);
    this.width = width - pl - pr;
  }

  update(force?: boolean): Promise<any> {
    return new Promise((resolve, reject) => {
      this.comparativesProvider.get(force).then(response => {
        this.comparatives = response;
        resolve();
      }).catch(error => {
        this.error.handle({error: error, log: 'error', text: 'Ha ocurrido un error. Intente nuevamente más tarde.'});
        reject(error);
      })
    })
  }

  doRefresh(refresher) {
    this.update(true).then(() => {
      refresher.complete();
    }).catch(() => {
      refresher.complete();
    })
  }

}
