import {Component, EventEmitter} from '@angular/core';
import {SelectOptionsProvider} from "../../providers/providers";

@Component({
  selector: 'round-card',
  templateUrl: 'round-card.html',
  inputs: ['round', 'showHeader', 'expandable', 'showMore'],
  outputs: ['contentClicked', 'moreClicked']
})
export class RoundCardComponent {
  round: any;
  showHeader: boolean;
  expandable: boolean;
  showMore: boolean;
  expanded: boolean;
  contentClicked = new EventEmitter;
  moreClicked = new EventEmitter;
  movements: any[];
  types: any[];
  parOptions: any[];
  finishLoading: boolean;

  constructor(private selectOptions: SelectOptionsProvider) {
    this.finishLoading = false;
    this.expanded = false;
    let promises = [];
    promises.push(this.selectOptions.getMovements().then(response => {
      this.movements = response;
    }));
    promises.push(this.selectOptions.getFieldTypes().then(response => {
      this.types = response;
    }));
    promises.push(this.selectOptions.getParOptions().then(response => {
      this.parOptions = response;
    }));
    Promise.all(promises).then(() => {
      this.finishLoading = true
    })
  }

  getMovementByValue(value: any) {
    for (let i = 0; i < this.movements.length; i++) {
      if (this.movements[i].value == value) {
        return this.movements[i];
      }
    }
    return false;
  }

  getTypesByValue(value: any) {
    for (let i = 0; i < this.types.length; i++) {
      if (this.types[i].value == value) {
        return this.types[i];
      }
    }
    return false;
  }

  getParOptionsByValue(value: any) {
    for (let i = 0; i < this.parOptions.length; i++) {
      if (this.parOptions[i].value == value) {
        return this.parOptions[i];
      }
    }
    return false;
  }

  expandClicked() {
    this.expanded = !this.expanded;
  }

}
