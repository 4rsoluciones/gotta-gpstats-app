import {Component} from '@angular/core';
import {IonicPage, LoadingController, NavController} from 'ionic-angular';
import {ErrorProvider, SelectOptionsProvider, SettingsProvider} from "../../providers/providers";

@IonicPage()
@Component({
  selector: 'page-edit-profile',
  templateUrl: 'edit-profile.html',
})
export class EditProfilePage {
  countries: any[];
  editedSettings: any = {};
  playerTypes: any[];
  tours: any[];
  handicapOptions: any[];
  loading: boolean;
  formStatus: string;

  constructor(private navCtrl: NavController,
              private loadingCtrl: LoadingController,
              private error: ErrorProvider,
              private selectOptions: SelectOptionsProvider,
              private settingsProvider: SettingsProvider) {
    this.handicapOptions = [
      {
        "value": 36,
        "title": "36"
      },
      {
        "value": 35,
        "title": "35"
      },
      {
        "value": 34,
        "title": "34"
      },
      {
        "value": 33,
        "title": "33"
      },
      {
        "value": 32,
        "title": "32"
      },
      {
        "value": 31,
        "title": "31"
      },
      {
        "value": 30,
        "title": "30"
      },
      {
        "value": 29,
        "title": "29"
      },
      {
        "value": 28,
        "title": "28"
      },
      {
        "value": 27,
        "title": "27"
      },
      {
        "value": 26,
        "title": "26"
      },
      {
        "value": 25,
        "title": "25"
      },
      {
        "value": 24,
        "title": "24"
      },
      {
        "value": 23,
        "title": "23"
      },
      {
        "value": 22,
        "title": "22"
      },
      {
        "value": 21,
        "title": "21"
      },
      {
        "value": 20,
        "title": "20"
      },
      {
        "value": 19,
        "title": "19"
      },
      {
        "value": 18,
        "title": "18"
      },
      {
        "value": 17,
        "title": "17"
      },
      {
        "value": 16,
        "title": "16"
      },
      {
        "value": 15,
        "title": "15"
      },
      {
        "value": 14,
        "title": "14"
      },
      {
        "value": 13,
        "title": "13"
      },
      {
        "value": 12,
        "title": "12"
      },
      {
        "value": 11,
        "title": "11"
      },
      {
        "value": 10,
        "title": "10"
      },
      {
        "value": 9,
        "title": "9"
      },
      {
        "value": 8,
        "title": "8"
      },
      {
        "value": 7,
        "title": "7"
      },
      {
        "value": 6,
        "title": "6"
      },
      {
        "value": 5,
        "title": "5"
      },
      {
        "value": 4,
        "title": "4"
      },
      {
        "value": 3,
        "title": "3"
      },
      {
        "value": 2,
        "title": "2"
      },
      {
        "value": 1,
        "title": "1"
      },
      {
        "value": 0,
        "title": "0"
      },
      {
        "value": -1,
        "title": "-1"
      },
      {
        "value": -2,
        "title": "-2"
      },
      {
        "value": -3,
        "title": "-3"
      },
      {
        "value": -4,
        "title": "-4"
      },
      {
        "value": -5,
        "title": "-5"
      }
    ];
  }

  ionViewCanEnter() {
    let promises = [];
    promises.push(this.selectOptions.getCountries().then(response => {
      this.countries = response;
    }));
    promises.push(this.selectOptions.getPlayerTypes().then(response => {
      this.playerTypes = response;
    }));
    promises.push(this.selectOptions.getTours().then(response => {
      this.tours = response;
    }));
    return Promise.all(promises);
  }

  ionViewWillEnter() {
    this.update();
  }

  update() {
    this.loading = true;
    this.settingsProvider.get().then(settings => {
      if (settings) {
        this.editedSettings = {
          country: settings.country,
          handicap: this.getHandicapByValue(settings.handicap),
          player_condition: this.getPlayerTypeByValue(settings.player_condition),
          tour: settings.tour
        };
      }
      this.formStatus = this.getFormStatus();
      this.loading = false;
    }).catch(error => {
      this.loading = false;
      this.error.handle({error: error, log: 'error', text: 'Ha ocurrido un error. Intente nuevamente más tarde.'});
    });
  }

  getHandicapByValue(value: any) {
    for (let i = 0; i < this.handicapOptions.length; i++) {
      if (this.handicapOptions[i].value == value) {
        return i;
      }
    }
    return -1;
  }

  getPlayerTypeByValue(value: any) {
    for (let i = 0; i < this.playerTypes.length; i++) {
      if (this.playerTypes[i].value == value) {
        return i;
      }
    }
    return -1;
  }

  inputChanged() {
    this.formStatus = this.getFormStatus();
  }

  getFormStatus(): string {
    if (!this.editedSettings.country) {
      return 'invalid-country';
    }
    if (this.editedSettings.player_condition == undefined) {
      return 'invalid-player-condition';
    }
    if ((this.handicapOptions[this.editedSettings.handicap].value == 'player.amateur') && (this.editedSettings.handicap == undefined)) {
      return 'invalid-handicap';
    }
    if ((this.handicapOptions[this.editedSettings.handicap].value == 'player.professional') && (!this.editedSettings.tour)) {
      return 'invalid-tour';
    }
    return 'valid';
  }

  saveClicked() {
    this.formStatus = this.getFormStatus();
    if (this.getFormStatus() == 'valid') {
      let loading = this.loadingCtrl.create();
      loading.present();
      let settings: any = {
        country: this.editedSettings.country,
        player_condition: this.playerTypes[this.editedSettings.player_condition].value
      };
      if (this.playerTypes[this.editedSettings.player_condition].value == 'player.amateur') {
        settings.handicap = this.handicapOptions[this.editedSettings.handicap].value;
      }
      if (this.playerTypes[this.editedSettings.player_condition].value == 'player.professional') {
        settings.tour = this.editedSettings.tour;
      }
      this.settingsProvider.set(settings).then(() => {
        loading.dismissAll();
        this.navCtrl.pop();
      }).catch(error => {
        loading.dismissAll();
        this.error.handle({error: error, log: 'error', text: 'Hubo un error al guardar los datos del perfil'});
      });
    }
  }

}
