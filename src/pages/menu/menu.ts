import {Component} from '@angular/core';
import {AlertController, Events, IonicPage, NavController} from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {

  constructor(private alertCtrl: AlertController,
              private navCtrl: NavController,
              private events: Events) {

  }

  statsClicked() {
    this.navCtrl.push('ComparativesPage');
  }

  serviciosClicked() {
    this.navCtrl.push('ServicesPage');
  }

  myDataClicked() {
    this.navCtrl.push('MyDataPage');
  }

  tosClicked() {
    this.navCtrl.push('TosPage');
  }

  logoutClicked() {
    let alert = this.alertCtrl.create({
      title: 'Confirmar cerrar sesión',
      message: '¿Está seguro que desea cerrar sesión? Los datos que no se sincronizaron serán eliminados.',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel'
        },
        {
          text: 'Cerrar Sesión',
          handler: () => {
            this.events.publish('logout');
          }
        }
      ]
    });
    alert.present();
  }

}
