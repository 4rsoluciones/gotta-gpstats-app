import {BaseEntity, Column, Entity, OneToMany, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {Field} from "./field";
import {InitialState} from "./initialState";
import {FinalState} from "./finalState";
import {Hole} from "./hole";

@Entity('round')
export class Round extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({nullable: true})
  is_completed: boolean;

  @Column({nullable: true})
  is_completed_locally: boolean;

  @OneToOne(() => Field, field => field.round, {
    cascadeInsert: true,
    cascadeUpdate: true
  })
  field: Field;

  @Column()
  game_condition: string;

  @Column({nullable: true})
  handicap: string;

  @Column()
  performed_at: string;

  @Column()
  player_condition: string;

  @Column()
  round_number: number;

  @Column()
  round_type: string;

  @Column({nullable: true})
  tour: number;

  @Column({nullable: true})
  tournament_type: string;

  @Column()
  played_holes: number;

  @OneToOne(() => InitialState, initialState => initialState.round, {
    cascadeInsert: true,
    cascadeUpdate: true,
    nullable: true
  })
  pre_competitive_questionnaire: InitialState;

  @OneToOne(() => FinalState, finalState => finalState.round, {
    cascadeInsert: true,
    cascadeUpdate: true,
    nullable: true
  })
  post_competitive_questionnaire: FinalState;

  @OneToMany(() => Hole, hole => hole.round, {
    cascadeInsert: true,
    cascadeUpdate: true
  })
  holes: Hole[];

  /**
   * si esta propiedad es null, todavia no se envió la vuelta al servicio.
   * Si ya se mando el servicio este valor es el id en el backend.
   */
  @Column({nullable: true, unique: true})
  synchronized: number;

  /**
   * -1: se debe eliminar
   * 0: no hay modificaciones
   * 1: hay modificaciones y se debe enviar al backend
   * 2: faltan actualizar los hoyos por defecto
   */
  @Column()
  modified: number;

}
