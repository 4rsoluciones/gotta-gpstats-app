import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {BenefitPage} from './benefit';
import {PipesModule} from "../../pipes/pipes.module";

@NgModule({
  declarations: [
    BenefitPage,
  ],
  imports: [
    IonicPageModule.forChild(BenefitPage),
    PipesModule
  ],
})
export class BenefitPageModule {
}
