import {ApiProvider} from "./api/api";
import {BenefitsProvider} from "./benefits/benefits";
import {ComparativesProvider} from "./comparatives/comparatives";
import {CoursesProvider} from "./courses/courses";
import {DatabaseProvider} from "./database/database";
import {ErrorProvider} from "./error/error";
import {NearMeProvider} from "./near-me/near-me";
import {Oauth2Provider} from "./oauth2/oauth2";
import {RoundProvider} from "./round/round";
import {RoundsProvider} from "./rounds/rounds";
import {SelectOptionsProvider} from "./select-options/select-options";
import {SettingsProvider} from "./settings/settings";
import {ServicesProvider} from "./services/services";
import {SyncProvider} from "./sync/sync";
import {UserProvider} from "./user/user";

export {
  ApiProvider,
  BenefitsProvider,
  ComparativesProvider,
  CoursesProvider,
  DatabaseProvider,
  ErrorProvider,
  NearMeProvider,
  Oauth2Provider,
  RoundProvider,
  RoundsProvider,
  SelectOptionsProvider,
  SettingsProvider,
  ServicesProvider,
  SyncProvider,
  UserProvider
}
