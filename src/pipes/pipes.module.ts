import {NgModule} from '@angular/core';
import {OrdinalPipe} from './ordinal/ordinal';
import { BaseUrlPipe } from './base-url/base-url';

@NgModule({
  declarations: [OrdinalPipe,
    BaseUrlPipe],
  imports: [],
  exports: [OrdinalPipe,
    BaseUrlPipe]
})
export class PipesModule {
}
