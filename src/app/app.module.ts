import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, LOCALE_ID, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';
import {registerLocaleData} from "@angular/common";
import localeEsAr from '@angular/common/locales/es-AR';

import {MyApp} from './app.component';
import {
  ApiProvider,
  BenefitsProvider,
  ComparativesProvider,
  CoursesProvider,
  DatabaseProvider,
  ErrorProvider,
  NearMeProvider,
  Oauth2Provider,
  RoundProvider,
  RoundsProvider,
  SelectOptionsProvider,
  ServicesProvider,
  SettingsProvider,
  SyncProvider,
  UserProvider
} from '../providers/providers';
import {OptionsPage} from "../pages/options/options";
import {InAppBrowser} from "@ionic-native/in-app-browser";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {Network} from "@ionic-native/network";
import {HttpModule} from "@angular/http";
import {Diagnostic} from "@ionic-native/diagnostic";
import {Geolocation} from "@ionic-native/geolocation";
import {GoogleMaps} from "@ionic-native/google-maps";

registerLocaleData(localeEsAr);

@NgModule({
  declarations: [
    MyApp,
    OptionsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      mode: 'md',
      statusbarPadding: false,
      swipeBackEnabled: false,
      backButtonIcon: 'ios-arrow-round-back'
    }),
    HttpModule,
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    OptionsPage
  ],
  providers: [
    Diagnostic,
    Geolocation,
    GoogleMaps,
    InAppBrowser,
    StatusBar,
    SplashScreen,
    Network,
    InAppBrowser,
    {provide: LOCALE_ID, useValue: 'es-AR'},
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApiProvider,
    BenefitsProvider,
    ComparativesProvider,
    CoursesProvider,
    DatabaseProvider,
    ErrorProvider,
    NearMeProvider,
    {provide: HTTP_INTERCEPTORS, useClass: Oauth2Provider, multi: true},
    RoundProvider,
    RoundsProvider,
    SelectOptionsProvider,
    ServicesProvider,
    SettingsProvider,
    SyncProvider,
    UserProvider
  ]
})
export class AppModule {
}
