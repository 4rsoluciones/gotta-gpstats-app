import {Component, ViewChild} from '@angular/core';
import {InfiniteScroll, IonicPage, NavController} from 'ionic-angular';
import {BenefitsProvider, ErrorProvider} from "../../providers/providers";
import {Benefit} from "../../models/benefit";

@IonicPage()
@Component({
  selector: 'page-benefits',
  templateUrl: 'benefits.html',
})
export class BenefitsPage {
  @ViewChild(InfiniteScroll) infiniteScroll: InfiniteScroll;
  loading: boolean;
  benefits: Array<Benefit>;
  limit: number = 20;

  constructor(private navCtrl: NavController,
              private benefitsProvider: BenefitsProvider,
              private error: ErrorProvider) {

  }

  ionViewDidEnter() {
    this.loading = true;
    this.update(false).then(() => {
      this.loading = false;
    }).catch(() => {
      this.loading = false;
    })
  }

  update(force?: boolean): Promise<any> {
    return new Promise((resolve, reject) => {
      this.benefitsProvider.getBenefits(force).then(benefits => {
        this.benefits = benefits;
        resolve();
      }).catch(error => {
        this.error.handle(error);
        reject(error);
      })
    })
  }

  getMoreBenefits(offset?: number): Promise<Benefit[]> {
    return new Promise((resolve, reject) => {
      this.benefitsProvider.getMore(this.limit, offset ? offset : 0).then((benefits: Benefit[]) => {
        if (!this.benefits) {
          this.benefits = benefits;
        } else {
          this.benefits = this.benefits.concat(benefits);
        }
        this.infiniteScroll.enable(benefits.length > 0);
        resolve();
      }).catch(error => {
        this.error.handle(error);
        reject(error);
      })
    })
  }

  doRefresh(refresher) {
    this.update(true).then(() => {
      refresher.complete();
    }).catch(() => {
      refresher.complete();
    })
  }

  doInfinite() {
    return this.getMoreBenefits(this.benefits.length);
  }

  benefitClicked(benefit: Benefit) {
    this.navCtrl.push('BenefitPage', {benefit: benefit});
  }

}
