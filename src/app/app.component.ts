import {Component} from '@angular/core';
import {Events, LoadingController, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {FirstRunPage, MainPage} from "../pages/pages";
import {
  BenefitsProvider,
  ComparativesProvider,
  DatabaseProvider,
  ErrorProvider,
  NearMeProvider,
  RoundProvider,
  RoundsProvider,
  ServicesProvider,
  SettingsProvider,
  SyncProvider,
  UserProvider
} from "../providers/providers";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;

  constructor(private platform: Platform,
              private statusBar: StatusBar,
              private splashScreen: SplashScreen,
              private events: Events,
              private loadingCtrl: LoadingController,
              private benefits: BenefitsProvider,
              private comparatives: ComparativesProvider,
              private error: ErrorProvider,
              private db: DatabaseProvider,
              private nearMe: NearMeProvider,
              private round: RoundProvider,
              private rounds: RoundsProvider,
              private services: ServicesProvider,
              private settings: SettingsProvider,
              private sync: SyncProvider,
              private user: UserProvider) {

    this.user.getLoginStatus().subscribe(status => {
      if (status) {
        let loading = this.loadingCtrl.create();
        loading.present();
        this.user.hasGpstatsService().then((hasGpstats: boolean) => {
          loading.dismiss();
          if (hasGpstats) {
            this.sync.beginSync();
            this.rootPage = MainPage;
          } else {
            this.error.presentToast('Aún no tenés vigente el servicio de GPStats. Si ya lo contrataste intenta nuevamente.');
            this.rootPage = 'NoServicePage';
          }
        }).catch(error => {
          loading.dismiss();
          this.error.handle({
            error: error,
            text: 'Hubo un error al verificar si tenés vigente el servicio de GPStats. Si ya lo contrataste intenta nuevamente.',
            log: 'error'
          });
          this.rootPage = 'NoServicePage';
        })
      } else {
        this.rootPage = FirstRunPage;
      }
    });

    this.events.subscribe('logout', () => {
      this.user.logout();
      window.localStorage.clear();
      this.user.checkLoginStatus();
      this.clearProviders();
    });

    this.events.subscribe('no-service', () => {
      this.user.checkLoginStatus();
    });

    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleLightContent();
      this.splashScreen.hide();

      this.db.init().then(() => {
        this.redirect();
      })

    });
  }

  redirect() {
    this.user.checkLoginStatus();
  }

  private clearProviders() {
    this.benefits.clear();
    this.comparatives.clear();
    this.db.reset();
    this.nearMe.clear();
    this.round.reset();
    this.rounds.clear();
    this.services.clear();
    this.settings.clear();
    this.sync.clear();
  }

}
