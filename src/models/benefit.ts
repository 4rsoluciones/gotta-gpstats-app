export class Benefit {
  id: number;
  img: string;
  logo: string;
  title: string;
  description: string;
  used: boolean;
}
