import {Pipe, PipeTransform} from '@angular/core';

const ordinals: string[] = ['ma', 'er', 'da', 'ra', 'ta', 'ta', 'ta', 'ma', 'va', 'na'];

@Pipe({
  name: 'ordinal',
})
export class OrdinalPipe implements PipeTransform {

  transform(n: number) {
    let v = n % 10;
    return n + (ordinals[v]||ordinals[0]);
  }
}
