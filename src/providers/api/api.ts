import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {environment} from "../../environment/environment";
import "rxjs/add/operator/timeout";
import {Field} from "../../models/field";
import {FinalState} from "../../models/finalState";
import {InitialState} from "../../models/initialState";
import {Round} from "../../models/round";
import {Hole} from "../../models/hole";
import {Play} from "../../models/play";
import {PhysicalQuestionnaire} from "../../models/physycalQuestionnaire";
import {EmotionalQuestionnaire} from "../../models/emotionalQuestionnaire";
import {FoodQuestionnaire} from "../../models/foodQuestionnaire";
import {Service} from "../../models/service";
import {NearMePlace} from "../../models/nearMePlace";
import {Benefit} from "../../models/benefit";
import {Observable} from "rxjs/Observable";
import {HoleToDelete} from "../../models/holeToDelete";
import {Events} from "ionic-angular";

@Injectable()
export class ApiProvider {
  production: boolean;
  apiConfig: any;

  constructor(private http: HttpClient,
              private events: Events) {
    this.production = environment.production;
    this.apiConfig = environment.apiConfig;
  }

  getCentralizadorConfig() {
    return this.production ? this.apiConfig.centralizador.prod : this.apiConfig.centralizador.dev;
  }

  getGpstatsConfig() {
    return this.production ? this.apiConfig.gpstats.prod : this.apiConfig.gpstats.dev;
  }

  log(info) {
    if (!this.production) {
      switch (info.log) {
        case 'log': {
          console.log(info.text, info.data);
          break;
        }
        case 'error': {
          console.error(info.text, info.data);
          break;
        }
      }
    }
  }

  /**
   * Todos los datos devueltos en el cuerpo del error deben estar en formato json
   * El formato de los errores debe estar estandarizado para todos los errores
   * @param error
   * @returns {Promise<any>}
   */
  handleError(error: any) {
    this.log({log: 'error', text: '[Web Service] URL ' + error.url + ' ERROR', data: error});
    let errorResponse: any = {
      error: error,
      text: 'Ha ocurrido un error. Por favor, inténtelo nuevamente.'
    };
    switch (error.status) {
      case 0: {
        errorResponse = {
          error: 'timeout',
          text: 'Verifique su conexión a internet e inténtelo nuevamente.'
        };
        break;
      }
      case 400: {
        try {
          if (error.errors.errors.indexOf('Este valor ya se ha utilizado.') > -1) {
            errorResponse = {
              error: 'exists',
              text: 'No se puede crear porque ya existe.'
            }
          }
        } catch (e) {}
        try {
          if (error.error.errors.children.number.errors.indexOf('Este valor ya se ha utilizado.') > -1) {
            errorResponse = {
              error: 'exists',
              text: 'No se puede crear porque ya existe.'
            }
          }
        } catch (e) {}
        break;
      }
      case 401: {
        errorResponse = {
          error: 'unauthenticated',
          text: error.json().text ? error.json().text : 'El usuario o contraseña son incorrectos.'
        };
        break;
      }
      case 403: {
        errorResponse = {
          error: 'unauthorized',
          text: 'Aún no tenés vigente el servicio de GPStats.'
        };
        this.events.publish('no-service');
        break;
      }
      case 404: {
        errorResponse = {
          error: 'not-found',
          text: 'Lo que intenta buscar no existe'
        };
        break;
      }
      default: {
        if (error.name == 'TimeoutError') {
          errorResponse = {
            error: 'timeout',
            text: 'Verifique su conexión a internet e inténtelo nuevamente.'
          };
          break;
        }
      }
    }
    return errorResponse;
  }

  getGpstatsSubscriptionStatus(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.http.get(this.getCentralizadorConfig().url + 'api/platforms/gpstats/services')
        .timeout(this.getCentralizadorConfig().timeout)
        .toPromise()
        .then(response => {
          resolve(response['length'] > 0);
        })
        .catch(error => {
          reject(this.handleError(error));
        })
    })
  }

  getCourses(params: { country: string, search: string }): Observable<Field[]> {
    const search = new HttpParams()
      .set('country', params.country)
      .set('terms', params.search);
    return this.http.get(this.getGpstatsConfig().url + 'api/gpstats/player/default-fields', {params: search})
      .timeout(this.getGpstatsConfig().timeout)
      .map(response => {
        let fields: Field[] = [];
        for (let i = 0; i < response['length']; i++) {
          let field = new Field();
          field.country = response[i].country;
          field.hole_count = response[i].hole_count;
          field.par = response[i].par;
          field.route = response[i].route;
          field.sport_club_name = response[i].sport_club_name;
          field.start_point = response[i].start_point;
          field.topographic_movements = response[i].topographic_movements;
          field.type = response[i].type;
          fields.push(field);
        }
        return fields;
      })
  }

  newCourse(params: Field): Promise<Field> {
    return new Promise((resolve, reject) => {
      this.http.post(this.getGpstatsConfig().url + 'api/gpstats/player/round/default-field', {default_field: params})
        .timeout(this.getGpstatsConfig().timeout)
        .toPromise()
        .then(response => {
          let field = new Field();
          field.synchronized = response['id'];
          field.country = response['country'];
          field.hole_count = response['hole_count'];
          field.par = response['par'];
          field.route = response['route'];
          field.sport_club_name = response['sport_club_name'];
          field.start_point = response['start_point'];
          field.topographic_movements = response['topographic_movements'];
          field.type = response['type'];
          resolve(field);
        })
        .catch(error => {
          reject(this.handleError(error));
        })
    })
  }

  getRounds(params: { incomplete?: boolean, limit?: number, offset?: number }): Promise<Round[]> {
    let search = new HttpParams()
      .set('limit', String(params.limit ? params.limit : 20))
      .set('offset', String(params.offset ? params.offset : 0));
    if (params.hasOwnProperty('incomplete')) {
      search = search.set('incomplete', params.incomplete ? 'true' : 'false');
    }
    return new Promise((resolve, reject) => {
      this.http.get(this.getGpstatsConfig().url + 'api/gpstats/player/rounds', {params: search})
        .timeout(this.getGpstatsConfig().timeout)
        .toPromise()
        .then(response => {
          let rounds: Round[] = [];
          for (let i = 0; i < response['length']; i++) {
            let round = new Round();
            let field = new Field();
            field.route = response[i].field.route;
            field.sport_club_name = response[i].field.sport_club_name;
            field.start_point = response[i].field.start_point;
            round.field = field;
            round.synchronized = response[i].id;
            round.is_completed = response[i].is_completed;
            if (!params.incomplete) {
              round.is_completed_locally = response[i].is_completed;
            }
            round.performed_at = response[i].performed_at.split('T')[0];
            round.round_number = response[i].round_number;
            round.round_type = response[i].round_type;
            round.tournament_type = response[i].tournament_type;
            round.played_holes = response[i].played_holes;
            rounds.push(round);
          }
          resolve(rounds);
        })
        .catch(error => {
          reject(this.handleError(error));
        })
    })
  }

  newRound(params: Round): Promise<Round> {
    return new Promise((resolve, reject) => {
      let newRound = new Round();
      let newField = new Field();
      newField.country = params.field.country;
      newField.hole_count = params.field.hole_count;
      newField.par = params.field.par;
      newField.route = params.field.route;
      newField.sport_club_name = params.field.sport_club_name;
      newField.start_point = params.field.start_point;
      newField.topographic_movements = params.field.topographic_movements;
      newField.type = params.field.type;
      newRound.field = newField;
      newRound.game_condition = params.game_condition;
      if (params.player_condition == 'player.professional') {
        newRound.tour = params.tour;
      }
      if (params.player_condition == 'player.amateur') {
        newRound.handicap = params.handicap;
      }
      newRound.performed_at = params.performed_at;
      newRound.played_holes = params.played_holes;
      newRound.player_condition = params.player_condition;
      newRound.round_number = params.round_number;
      newRound.round_type = params.round_type;
      newRound.tournament_type = params.tournament_type;
      this.http.post(this.getGpstatsConfig().url + 'api/gpstats/player/round/save', {round: newRound})
        .timeout(this.getGpstatsConfig().timeout)
        .toPromise()
        .then(response => {
          let round: Round = new Round();
          let field = new Field();
          field.country = response['field'].country;
          field.hole_count = response['field'].hole_count;
          field.par = response['field'].par;
          field.route = response['field'].route;
          field.sport_club_name = response['field'].sport_club_name;
          field.start_point = response['field'].start_point;
          field.topographic_movements = response['field'].topographic_movements;
          field.type = response['field'].type;
          round.field = field;
          round.game_condition = response['game_condition'];
          round.player_condition = response['player_condition'];
          if (round.player_condition == 'player.professional') {
            round.tour = response['tour'].id;
          }
          if (round.player_condition == 'player.amateur') {
            round.handicap = response['handicap'];
          }
          round.synchronized = response['id'];
          round.is_completed = response['is_completed'];
          round.performed_at = response['performed_at'].split('T')[0];
          round.played_holes = response['played_holes'];
          round.round_number = response['round_number'];
          round.round_type = response['round_type'];
          round.tournament_type = response['tournament_type'];
          resolve(round);
        })
        .catch(error => {
          reject(this.handleError(error));
        })
    })
  }

  getDefaultHoles(field: Field): Promise<Hole[]> {
    const params = new HttpParams()
      .set('country', field.country)
      .set('sportClubName', field.sport_club_name)
      .set('route', field.route)
      .set('startPoint', field.start_point);
    return new Promise((resolve, reject) => {
      this.http.get(this.getGpstatsConfig().url + 'api/gpstats/player/default-holes', {params: params})
        .timeout(this.getGpstatsConfig().timeout)
        .toPromise()
        .then(response => {
          let holes: Hole[] = [];
          for (let i = 0; i < response['length']; i++) {
            let hole: Hole = new Hole();
            hole.number = response[i].number;
            hole.par = response[i].par;
            hole.design = response[i].design;
            holes.push(hole);
          }
          resolve(holes);
        })
        .catch(error => {
          reject(this.handleError(error));
        })
    })
  }

  getRound(params: { id: number }): Promise<Round> {
    return new Promise((resolve, reject) => {
      this.http.get(this.getGpstatsConfig().url + 'api/gpstats/player/round/' + params.id)
        .timeout(this.getGpstatsConfig().timeout)
        .toPromise()
        .then(response => {
          let round: Round = new Round();
          let field = new Field();
          field.country = response['field'].country;
          field.hole_count = response['field'].hole_count;
          field.par = response['field'].par;
          field.route = response['field'].route;
          field.sport_club_name = response['field'].sport_club_name;
          field.start_point = response['field'].start_point;
          field.topographic_movements = response['field'].topographic_movements;
          field.type = response['field'].type;
          round.field = field;
          round.game_condition = response['game_condition'];
          round.player_condition = response['player_condition'];
          if (round.player_condition == 'player.professional') {
            round.tour = response['tour'].id;
          }
          if (round.player_condition == 'player.amateur') {
            round.handicap = response['handicap'];
          }
          round.synchronized = response['id'];
          round.is_completed = response['is_completed'];
          round.is_completed_locally = response['player_confirmed'];
          round.performed_at = response['performed_at'].split('T')[0];
          round.played_holes = response['played_holes'];
          round.round_number = response['round_number'];
          round.round_type = response['round_type'];
          round.tournament_type = response['tournament_type'];
          let initialState;
          if (response['pre_competitive_questionnaire']) {
            initialState = new InitialState();
            initialState.is_warmed_up = response['pre_competitive_questionnaire'].is_warmed_up;
            initialState.has_practiced = response['pre_competitive_questionnaire'].has_practiced;
            initialState.wants_to_play = response['pre_competitive_questionnaire'].wants_to_play;
            initialState.has_strategy = response['pre_competitive_questionnaire'].has_strategy;
            initialState.modified = 0;
          }
          round.pre_competitive_questionnaire = initialState;
          let finalState;
          if (response['post_competitive_questionnaire']) {
            finalState = new FinalState();
            finalState.feeling_status = response['post_competitive_questionnaire'].feeling_status;
            finalState.modified = 0;
          }
          round.post_competitive_questionnaire = finalState;
          let holes = [];
          let auxHoles = response['holes'];
          for (let i = 0; i < auxHoles.length; i++) {
            let hole = new Hole();
            hole.number = auxHoles[i].number;
            hole.par = auxHoles[i].par;
            hole.design = auxHoles[i].design;
            hole.description = auxHoles[i].description;
            hole.penalityShotsCount = auxHoles[i].penality_shots_count;
            let strikes = [];
            let auxStrikes = auxHoles[i].strikes;
            for (let j = 0; j < auxStrikes.length; j++) {
              let strike = new Play();
              strike.order = j + 1;
              if (j == 0) {
                strike.type = 'strike_type.first';
              } else {
                if (strikes[j - 1].type == 'strike_type.putt') {
                  strike.type = 'strike_type.putt';
                } else if (strikes[j - 1].result != 'inside.green') {
                  strike.type = 'strike_type.other';
                } else {
                  strike.type = 'strike_type.putt';
                }
              }
              strike.distance = auxStrikes[j].distance;
              strike.result = auxStrikes[j].result;
              strike.reason = auxStrikes[j].reason;
              strike.ballFlight = auxStrikes[j].ballFlight;
              strike.club = auxStrikes[j].club;
              strike.strikeLies = auxStrikes[j].strikeLies;
              strike.landSlopes = auxStrikes[j].landSlopes;
              let eq: EmotionalQuestionnaire = new EmotionalQuestionnaire();
              eq.feelsAnnoyed = !!auxStrikes[j].emotional_questionnaire.feels_annoyed;
              eq.feelsDistrustful = !!auxStrikes[j].emotional_questionnaire.feels_distrustful;
              eq.feelsEnjoying = !!auxStrikes[j].emotional_questionnaire.feels_enjoying;
              eq.feelsFocused = !!auxStrikes[j].emotional_questionnaire.feels_focused;
              eq.feelsFrustrated = !!auxStrikes[j].emotional_questionnaire.feels_frustrated;
              eq.feelsTrustful = !!auxStrikes[j].emotional_questionnaire.feels_trustful;
              eq.feelsUptight = !!auxStrikes[j].emotional_questionnaire.feels_uptight;
              strike.emotionalQuestionnaire = eq;
              let fq: FoodQuestionnaire = new FoodQuestionnaire();
              fq.drankSportsDrink = !!auxStrikes[j].food_questionnaire.drank_sports_drink;
              fq.drankWater = !!auxStrikes[j].food_questionnaire.drank_water;
              fq.swallowedCerealOrNuts = !!auxStrikes[j].food_questionnaire.swallowed_cereal_or_nuts;
              fq.swallowedFruit = !!auxStrikes[j].food_questionnaire.swallowed_fruit;
              fq.swallowedSandwich = !!auxStrikes[j].food_questionnaire.swallowed_sandwich;
              strike.foodQuestionnaire = fq;
              let pq: PhysicalQuestionnaire = new PhysicalQuestionnaire();
              pq.feelsEnergetic = !!auxStrikes[j].physical_questionnaire.feels_energetic;
              pq.feelsExhausted = !!auxStrikes[j].physical_questionnaire.feels_exhausted;
              pq.feelsGettingTired = !!auxStrikes[j].physical_questionnaire.feels_getting_tired;
              pq.feelsPainful = !!auxStrikes[j].physical_questionnaire.feels_painful;
              pq.feelsTired = !!auxStrikes[j].physical_questionnaire.feels_tired;
              strike.physicalQuestionnaire = pq;
              strikes.push(strike);
            }
            hole.strikes = strikes;
            holes.push(hole);
          }
          round.holes = holes;
          resolve(round);
        })
        .catch(error => {
          reject(this.handleError(error));
        })
    })
  }

  editRoundInfo(params: Round): Promise<Round> {
    return new Promise((resolve, reject) => {
      let round = new Round();
      let field = new Field();
      field.country = params.field.country;
      field.hole_count = params.field.hole_count;
      field.par = params.field.par;
      field.route = params.field.route;
      field.sport_club_name = params.field.sport_club_name;
      field.start_point = params.field.start_point;
      field.topographic_movements = params.field.topographic_movements;
      field.type = params.field.type;
      round.field = field;
      round.game_condition = params.game_condition;
      round.performed_at = params.performed_at;
      round.round_number = params.round_number;
      round.round_type = params.round_type;
      round.tour = params.tour;
      round.tournament_type = params.tournament_type;
      this.http.patch(this.getGpstatsConfig().url + 'api/gpstats/player/round/save/' + params.synchronized, {round: round})
        .toPromise()
        .then(response => {
          let round: Round = new Round();
          let field = new Field();
          field.country = response['field'].country;
          field.hole_count = response['field'].hole_count;
          field.par = response['field'].par;
          field.route = response['field'].route;
          field.sport_club_name = response['field'].sport_club_name;
          field.start_point = response['field'].start_point;
          field.topographic_movements = response['field'].topographic_movements;
          field.type = response['field'].type;
          round.field = field;
          round.game_condition = response['game_condition'];
          round.player_condition = response['player_condition'];
          if (round.player_condition == 'player.professional') {
            round.tour = response['tour'].id;
          }
          if (round.player_condition == 'player.amateur') {
            round.handicap = response['handicap'];
          }
          round.synchronized = response['id'];
          round.is_completed = response['is_completed'];
          round.performed_at = response['performed_at'].split('T')[0];
          round.played_holes = response['played_holes'];
          round.round_number = response['round_number'];
          round.round_type = response['round_type'];
          round.tournament_type = response['tournament_type'];
          resolve(round);
        })
        .catch(error => {
          reject(this.handleError(error));
        })
    })
  }

  setInitialState(params: InitialState): Promise<InitialState> {
    return new Promise((resolve, reject) => {
      let initialState = new InitialState();
      initialState.is_warmed_up = params.is_warmed_up;
      initialState.has_practiced = params.has_practiced;
      initialState.wants_to_play = params.wants_to_play;
      initialState.has_strategy = params.has_strategy;
      this.http.put(this.getGpstatsConfig().url + 'api/gpstats/player/round/pre-competitive-questionnaire/save/' + params.round.synchronized, {pre_competitive_questionnaire: initialState})
        .toPromise()
        .then(response => {
          let initialState: InitialState = new InitialState();
          initialState.is_warmed_up = response['pre_competitive_questionnaire'].is_warmed_up;
          initialState.has_practiced = response['pre_competitive_questionnaire'].has_practiced;
          initialState.wants_to_play = response['pre_competitive_questionnaire'].wants_to_play;
          initialState.has_strategy = response['pre_competitive_questionnaire'].has_strategy;
          let round: Round = new Round();
          let field = new Field();
          field.country = response['field'].country;
          field.hole_count = response['field'].hole_count;
          field.par = response['field'].par;
          field.route = response['field'].route;
          field.sport_club_name = response['field'].sport_club_name;
          field.start_point = response['field'].start_point;
          field.topographic_movements = response['field'].topographic_movements;
          field.type = response['field'].type;
          round.field = field;
          round.game_condition = response['game_condition'];
          round.player_condition = response['player_condition'];
          if (round.player_condition == 'player.professional') {
            round.tour = response['tour'].id;
          }
          if (round.player_condition == 'player.amateur') {
            round.handicap = response['handicap'];
          }
          round.synchronized = response['id'];
          round.is_completed = response['is_completed'];
          round.performed_at = response['performed_at'].split('T')[0];
          round.played_holes = response['played_holes'];
          round.round_number = response['round_number'];
          round.round_type = response['round_type'];
          round.tournament_type = response['tournament_type'];
          initialState.round = round;
          resolve(initialState);
        })
        .catch(error => {
          reject(this.handleError(error));
        })
    })
  }

  setFinalState(params: FinalState): Promise<FinalState> {
    return new Promise((resolve, reject) => {
      let finalState = new FinalState();
      finalState.feeling_status = params.feeling_status;
      this.http.put(this.getGpstatsConfig().url + 'api/gpstats/player/round/post-competitive-questionnaire/save/' + params.round.synchronized, {post_competitive_questionnaire: finalState})
        .toPromise()
        .then(response => {
          let finalState: FinalState = new FinalState();
          finalState.feeling_status = response['post_competitive_questionnaire'] ? response['post_competitive_questionnaire'].feeling_status : null;
          let round: Round = new Round();
          let field = new Field();
          field.country = response['field'].country;
          field.hole_count = response['field'].hole_count;
          field.par = response['field'].par;
          field.route = response['field'].route;
          field.sport_club_name = response['field'].sport_club_name;
          field.start_point = response['field'].start_point;
          field.topographic_movements = response['field'].topographic_movements;
          field.type = response['field'].type;
          round.field = field;
          round.game_condition = response['game_condition'];
          round.player_condition = response['player_condition'];
          if (round.player_condition == 'player.professional') {
            round.tour = response['tour'].id;
          }
          if (round.player_condition == 'player.amateur') {
            round.handicap = response['handicap'];
          }
          round.synchronized = response['id'];
          round.is_completed = response['is_completed'];
          round.performed_at = response['performed_at'].split('T')[0];
          round.played_holes = response['played_holes'];
          round.round_number = response['round_number'];
          round.round_type = response['round_type'];
          round.tournament_type = response['tournament_type'];
          finalState.round = round;
          resolve(finalState);
        })
        .catch(error => {
          reject(this.handleError(error));
        })
    })
  }

  deleteRound(params: {id: number}): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.delete(this.getGpstatsConfig().url + 'api/gpstats/player/round/delete/' + params.id)
        .toPromise()
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(this.handleError(error));
        })
    })
  }

  setDefaultHole(params: Hole): Promise<Hole> {
    let hole: any = {};
    hole.number = params.number;
    hole.par = params.par;
    hole.design = params.design;
    hole.country = params.round.field.country;
    hole.route = params.round.field.route;
    hole.sportClubName = params.round.field.sport_club_name;
    hole.startPoint = params.round.field.start_point;
    return new Promise((resolve, reject) => {
      this.http.post(this.getGpstatsConfig().url + 'api/gpstats/player/round/default-hole', {default_hole: hole})
        .timeout(this.getGpstatsConfig().timeout)
        .toPromise()
        .then(response => {
          let ret: Hole = new Hole();
          ret.number = response['number'];
          ret.par = response['par'];
          ret.design = response['design'];
          resolve(ret);
        })
        .catch(error => {
          reject(this.handleError(error));
        })
    })
  }

  setHolePlay(params: Hole): Promise<Hole> {
    let hole: Hole = new Hole();
    params.description ? hole.description = params.description : '';
    hole.design = params.design;
    hole.number = params.number;
    hole.par = params.par;
    hole.penalityShotsCount = params.penalityShotsCount;
    let strikes: Play[] = [];
    for (let i = 0; i < params.strikes.length; i++) {
      let strike: Play = new Play();
      params.strikes[i].ballFlight ? strike.ballFlight = params.strikes[i].ballFlight : '';
      params.strikes[i].club ? strike.club = params.strikes[i].club : '';
      params.strikes[i].distance ? strike.distance = params.strikes[i].distance : '';
      params.strikes[i].landSlopes ? strike.landSlopes = params.strikes[i].landSlopes : '';
      params.strikes[i].reason ? strike.reason = params.strikes[i].reason : '';
      params.strikes[i].result ? strike.result = params.strikes[i].result : '';
      params.strikes[i].strikeLies ? strike.strikeLies = params.strikes[i].strikeLies : '';
      params.strikes[i].type ? strike.type = params.strikes[i].type : '';
      if (params.strikes[i].emotionalQuestionnaire) {
        let eq: EmotionalQuestionnaire = new EmotionalQuestionnaire();
        eq.feelsAnnoyed = !!params.strikes[i].emotionalQuestionnaire.feelsAnnoyed;
        eq.feelsDistrustful = !!params.strikes[i].emotionalQuestionnaire.feelsDistrustful;
        eq.feelsEnjoying = !!params.strikes[i].emotionalQuestionnaire.feelsEnjoying;
        eq.feelsFocused = !!params.strikes[i].emotionalQuestionnaire.feelsFocused;
        eq.feelsFrustrated = !!params.strikes[i].emotionalQuestionnaire.feelsFrustrated;
        eq.feelsTrustful = !!params.strikes[i].emotionalQuestionnaire.feelsTrustful;
        eq.feelsUptight = !!params.strikes[i].emotionalQuestionnaire.feelsUptight;
        strike.emotionalQuestionnaire = eq;
      }
      if (params.strikes[i].foodQuestionnaire) {
        let fq: FoodQuestionnaire = new FoodQuestionnaire();
        fq.drankSportsDrink = !!params.strikes[i].foodQuestionnaire.drankSportsDrink;
        fq.drankWater = !!params.strikes[i].foodQuestionnaire.drankWater;
        fq.swallowedCerealOrNuts = !!params.strikes[i].foodQuestionnaire.swallowedCerealOrNuts;
        fq.swallowedFruit = !!params.strikes[i].foodQuestionnaire.swallowedFruit;
        fq.swallowedSandwich = !!params.strikes[i].foodQuestionnaire.swallowedSandwich;
        strike.foodQuestionnaire = fq;
      }
      if (params.strikes[i].physicalQuestionnaire) {
        let pq: PhysicalQuestionnaire = new PhysicalQuestionnaire();
        pq.feelsEnergetic = !!params.strikes[i].physicalQuestionnaire.feelsEnergetic;
        pq.feelsExhausted = !!params.strikes[i].physicalQuestionnaire.feelsExhausted;
        pq.feelsGettingTired = !!params.strikes[i].physicalQuestionnaire.feelsGettingTired;
        pq.feelsPainful = !!params.strikes[i].physicalQuestionnaire.feelsPainful;
        pq.feelsTired = !!params.strikes[i].physicalQuestionnaire.feelsTired;
        strike.physicalQuestionnaire = pq;
      }
      strikes.push(strike);
    }
    hole.strikes = strikes;
    return new Promise((resolve, reject) => {
      this.http.post(this.getGpstatsConfig().url + 'api/gpstats/player/round/' + params.round.synchronized + '/hole', {hole: hole})
        .timeout(this.getGpstatsConfig().timeout)
        .toPromise()
        .then(response => {
          let hole: Hole = new Hole();
          hole.number = response['number'];
          hole.par = response['par'];
          hole.design = response['design'];
          hole.description = response['description'];
          hole.penalityShotsCount = response['penality_shots_count'];
          let strikes = [];
          let auxStrikes = response['strikes'];
          for (let j = 0; j < auxStrikes.length; j++) {
            let strike = new Play();
            strike.order = j + 1;
            if (j == 0) {
              strike.type = 'strike_type.first';
            } else {
              if (strikes[j - 1].type == 'strike_type.putt') {
                strike.type = 'strike_type.putt';
              } else if (strikes[j - 1].result != 'inside.green') {
                strike.type = 'strike_type.other';
              } else {
                strike.type = 'strike_type.putt';
              }
            }
            strike.distance = auxStrikes[j].distance;
            strike.result = auxStrikes[j].result;
            strike.reason = auxStrikes[j].reason;
            strike.ballFlight = auxStrikes[j].ballFlight;
            strike.club = auxStrikes[j].club;
            strike.strikeLies = auxStrikes[j].strikeLies;
            strike.landSlopes = auxStrikes[j].landSlopes;
            let eq: EmotionalQuestionnaire = new EmotionalQuestionnaire();
            eq.feelsAnnoyed = !!auxStrikes[j].emotional_questionnaire.feels_annoyed;
            eq.feelsDistrustful = !!auxStrikes[j].emotional_questionnaire.feels_distrustful;
            eq.feelsEnjoying = !!auxStrikes[j].emotional_questionnaire.feels_enjoying;
            eq.feelsFocused = !!auxStrikes[j].emotional_questionnaire.feels_focused;
            eq.feelsFrustrated = !!auxStrikes[j].emotional_questionnaire.feels_frustrated;
            eq.feelsTrustful = !!auxStrikes[j].emotional_questionnaire.feels_trustful;
            eq.feelsUptight = !!auxStrikes[j].emotional_questionnaire.feels_uptight;
            strike.emotionalQuestionnaire = eq;
            let fq: FoodQuestionnaire = new FoodQuestionnaire();
            fq.drankSportsDrink = !!auxStrikes[j].food_questionnaire.drank_sports_drink;
            fq.drankWater = !!auxStrikes[j].food_questionnaire.drank_water;
            fq.swallowedCerealOrNuts = !!auxStrikes[j].food_questionnaire.swallowed_cereal_or_nuts;
            fq.swallowedFruit = !!auxStrikes[j].food_questionnaire.swallowed_fruit;
            fq.swallowedSandwich = !!auxStrikes[j].food_questionnaire.swallowed_sandwich;
            strike.foodQuestionnaire = fq;
            let pq: PhysicalQuestionnaire = new PhysicalQuestionnaire();
            pq.feelsEnergetic = !!auxStrikes[j].physical_questionnaire.feels_energetic;
            pq.feelsExhausted = !!auxStrikes[j].physical_questionnaire.feels_exhausted;
            pq.feelsGettingTired = !!auxStrikes[j].physical_questionnaire.feels_getting_tired;
            pq.feelsPainful = !!auxStrikes[j].physical_questionnaire.feels_painful;
            pq.feelsTired = !!auxStrikes[j].physical_questionnaire.feels_tired;
            strike.physicalQuestionnaire = pq;
            strikes.push(strike);
          }
          hole.strikes = strikes;
          resolve(hole);
        })
        .catch(error => {
          reject(this.handleError(error));
        })
    })
  }

  deleteHolePlay(holeToDelete: HoleToDelete): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.delete(this.getGpstatsConfig().url + 'api/gpstats/player/round/' + holeToDelete.round + '/delete/hole/' + holeToDelete.number)
        .timeout(this.getGpstatsConfig().timeout)
        .toPromise()
        .then(() => {
          resolve(holeToDelete);
        })
        .catch(error => {
          reject(this.handleError(error));
        })
    })
  }

  getServices(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(this.getCentralizadorConfig().url + 'api/services/by-platform')
        .timeout(this.getCentralizadorConfig().timeout)
        .toPromise()
        .then(response => {
          let platforms = response['success'];
          let services: any = {};
          for (let key of Object.keys(platforms)) {
            let platform = platforms[key];
            services[key] = [];
            for (let i = 0; i < platform.length; i++) {
              let service: Service = new Service();
              service.id = platform[i].id;
              service.name = platform[i].name;
              services[key].push(service);
            }
          }
          resolve(services);
        })
        .catch(error => {
          reject(this.handleError(error));
        })
    })
  }

  getSettings(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(this.getGpstatsConfig().url + 'api/gpstats/player/profile')
        .timeout(this.getGpstatsConfig().timeout)
        .toPromise()
        .then(response => {
          let settings = {
            country: response['profile'].country,
            handicap: response['profile'].handicap,
            player_condition: response['profile'].condition,
            tour: response['profile'].tour ? response['profile'].tour.id : null
          };
          resolve(settings);
        })
        .catch(error => {
          reject(this.handleError(error));
        })
    })
  }

  setSettings(params): Promise<any> {
    let profile = {
      country: params.country,
      handicap: params.handicap,
      condition: params.player_condition,
      tour: params.tour
    };
    return new Promise((resolve, reject) => {
      this.http.put(this.getGpstatsConfig().url + 'api/gpstats/player/profile', {profile: profile})
        .timeout(this.getGpstatsConfig().timeout)
        .toPromise()
        .then(response => {
          let settings = {
            country: response['profile'].country,
            handicap: response['profile'].handicap,
            player_condition: response['profile'].condition,
            tour: response['profile'].tour ? response['profile'].tour.id : null
          };
          resolve(settings);
        })
        .catch(error => {
          reject(this.handleError(error));
        })
    })
  }

  getAccount(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(this.getCentralizadorConfig().url + 'api/user')
        .timeout(this.getCentralizadorConfig().timeout)
        .toPromise()
        .then(response => {
          let account = response['success'];
          let accountInfo: any = {
            username: account.hasOwnProperty('username') ? account['username'] : null,
            email: account.hasOwnProperty('email') ? account['email'] : null,
            state_id: account['profile'].state_id ? account['profile'].state_id : null,
            city: account['profile'].city ? account['profile'].city : null,
            first_name: account['profile'].first_name ? account['profile'].first_name : null,
            last_name: account['profile'].last_name ? account['profile'].last_name : null,
            company_name: account['profile'].company_name ? account['profile'].company_name : null,
            birth_date: account['profile'].birth_date ? account['profile'].birth_date : null,
            gender: account['profile'].gender ? account['profile'].gender : null,
            mobile_phonenumber: account['profile'].mobile_phonenumber ? account['profile'].mobile_phonenumber : null
          };
          resolve(accountInfo);
        })
        .catch(error => {
          reject(this.handleError(error));
        })
    })
  }

  editAccount(params): Promise<any> {
    return new Promise((resolve, reject) => {
      let account: any = {
        username: params.username,
        birth_date: params.birth_date,
        city: params.city,
        company_name: params.company_name,
        first_name: params.first_name,
        gender: params.gender,
        last_name: params.last_name,
        mobile_phonenumber: params.mobile_phonenumber,
        state_id: params.state_id
      };
      this.http.put(this.getCentralizadorConfig().url + 'api/user', account)
        .timeout(this.getCentralizadorConfig().timeout)
        .toPromise()
        .then(response => {
          let account = response['success'];
          let accountInfo: any = {
            username: account.hasOwnProperty('username') ? account['username'] : null,
            email: account.hasOwnProperty('email') ? account['email'] : null,
            state_id: account['profile'].state_id ? account['profile'].state_id : null,
            city: account['profile'].city ? account['profile'].city : null,
            first_name: account['profile'].first_name ? account['profile'].first_name : null,
            last_name: account['profile'].last_name ? account['profile'].last_name : null,
            company_name: account['profile'].company_name ? account['profile'].company_name : null,
            birth_date: account['profile'].birth_date ? account['profile'].birth_date : null,
            gender: account['profile'].gender ? account['profile'].gender : null,
            mobile_phonenumber: account['profile'].mobile_phonenumber ? account['profile'].mobile_phonenumber : null
          };
          resolve(accountInfo);
        })
        .catch(error => {
          reject(this.handleError(error));
        })
    })
  }

  getStats(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(this.getGpstatsConfig().url + 'api/gpstats/player/comparatives')
        .timeout(this.getGpstatsConfig().timeout)
        .toPromise()
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(this.handleError(error));
        })
    })
  }

  getMapFilters(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(this.getCentralizadorConfig().url + 'api/business/categories')
        .timeout(this.getCentralizadorConfig().timeout)
        .toPromise()
        .then(response => {
          let filters = [];
          for (let i = 0; i < response['length']; i++) {
            let filter = {
              id: response[i].id,
              title: response[i].name,
              selected: true
            };
            filters.push(filter);
          }
          resolve(filters);
        })
        .catch(error => {
          reject(this.handleError(error));
        })
    })
  }

  getNearMe(params: {lat: string, lng: string, rad: string, categories: string}): Promise<NearMePlace[]> {
    return new Promise((resolve, reject) => {
      let httpParams = {
        latitude: params.lat,
        longitude: params.lng,
        range: params.rad,
        category_id: params.categories
      };
      this.http.post(this.getCentralizadorConfig().url + 'api/nearMe', httpParams)
        .timeout(this.getCentralizadorConfig().timeout)
        .toPromise()
        .then(response => {
          let places: NearMePlace[] = [];
          for (let i = 0; i < response['length']; i++) {
            let place: NearMePlace = new NearMePlace();
            place.id = parseInt(response[i].id);
            place.img = response[i].image;
            place.logo = response[i].logo;
            place.category = response[i].category;
            place.title = response[i].name;
            place.description = response[i].description;
            place.address = response[i].direction;
            place.lat = parseFloat(response[i].latitude);
            place.lng = parseFloat(response[i].longitude);
            places.push(place);
          }
          resolve(places);
        })
        .catch(error => {
          reject(this.handleError(error));
        })
    })
  }

  getBenefits(params: {limit?: string, offset?: string}): Promise<Benefit[]> {
    return new Promise((resolve, reject) => {
      let httpParams = {
        limit: params.limit ? params.limit : '20',
        start: params.offset ? params.offset : '0'
      };
      this.http.post(this.getCentralizadorConfig().url + 'api/benefits', httpParams)
        .timeout(this.getCentralizadorConfig().timeout)
        .toPromise()
        .then(response => {
          let benefits: Benefit[] = [];
          response = response['results'];
          for (let i = 0; i < response['length']; i++) {
            let benefit: Benefit = new Benefit();
            benefit.id = parseInt(response[i].id);
            benefit.img = response[i].image;
            benefit.logo = response[i].logo;
            benefit.title = response[i].name;
            benefit.description = response[i].description;
            benefit.used = !!response[i].changed;
            benefits.push(benefit);
          }
          resolve(benefits);
        })
        .catch(error => {
          reject(this.handleError(error));
        })
    })
  }

  useCoupon(params: {couponId: string}): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(this.getCentralizadorConfig().url + 'api/exchang/coupon/' + params.couponId)
        .timeout(this.getCentralizadorConfig().timeout)
        .toPromise()
        .then(response => {
          resolve();
        })
        .catch(error => {
          reject(this.handleError(error));
        })
    })
  }

}
