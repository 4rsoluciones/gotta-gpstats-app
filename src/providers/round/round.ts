import {Injectable} from '@angular/core';
import {Round} from "../../models/round";
import {Field} from "../../models/field";
import {DatabaseProvider} from "../database/database";
import {SyncProvider} from "../sync/sync";
import {ApiProvider} from "../api/api";
import {RoundsProvider} from "../rounds/rounds";
import {InitialState} from "../../models/initialState";
import {FinalState} from "../../models/finalState";
import {Hole} from "../../models/hole";
import {Events} from "ionic-angular";
import {ErrorProvider} from "../error/error";

@Injectable()
export class RoundProvider {
  round: Round;

  constructor(private db: DatabaseProvider,
              private api: ApiProvider,
              private events: Events,
              private error: ErrorProvider,
              private roundsProvider: RoundsProvider,
              private sync: SyncProvider) {
    this.reset();
    this.events.subscribe('sync:round-updated', (data) => {
      this.roundUpdated(data).then((data) => {
        if (data) {
          this.events.publish('round:updated');
        }
      }).catch(error => {
        this.error.handle({error: error, log: 'error'});
      })
    });
  }

  reset() {
    this.round = new Round();
  }

  setCourse(field: Field) {
    this.round.field = field;
  }

  getCourse(): Field {
    return Object.assign({}, this.round.field);
  }

  setActiveRound(round: Round): Promise<any> {
    return new Promise((resolve, reject) => {
      this.db.getRound(round).then((response: Round) => {
        if (response) {
          response.holes = this.assembleHoles(response.holes);
          this.round = Object.assign({}, response);
          resolve();
        } else {
          this.api.getRound({id: round.synchronized}).then((round: Round) => {
            round.holes = this.assembleHoles(round.holes);
            this.round = round;
            resolve();
          }).catch(error => {
            reject(error);
          })
        }
      }).catch(error => {
        reject(error);
      })
    })
  }

  private assembleHoles(holes: Array<Hole>): Array<Hole> {
    let retHoles: Array<Hole> = [];
    for (let i = 0; i < holes.length; i++) {
      retHoles[holes[i].number - 1] = (holes[i].modified != -1) ? holes[i] : null;
    }
    return retHoles;
  }

  roundUpdated(data) {
    if (this.round && this.round.hasOwnProperty('id')) {
      if (this.round.id == data.id) {
        return this.setActiveRound(this.round);
      }
    }
    return Promise.resolve(false);
  }

  getRoundInfo() {
    let roundInfo: Round = Object.assign({}, this.round);
    delete roundInfo.pre_competitive_questionnaire;
    delete roundInfo.post_competitive_questionnaire;
    delete roundInfo.holes;
    return roundInfo;
  }

  editRoundInfo(round: Round) {
    this.round.field = round.field;
    this.round.game_condition = round.game_condition;
    this.round.performed_at = round.performed_at;
    this.round.round_number = round.round_number;
    this.round.round_type = round.round_type;
    this.round.tour = round.tour;
    this.round.tournament_type = round.tournament_type;
    this.db.setRoundInfoToEdit(this.getRoundInfo()).then(() => {
      this.sync.beginSync();
    })
  }

  saveInitialState(state: InitialState) {
    this.round.pre_competitive_questionnaire = Object.assign({}, state);
    state.round = this.getRoundInfo();
    this.db.savePreCompetitiveQuestionnaire(state).then(() => {
      this.sync.beginSync();
    })
  }

  getInitialState(): InitialState {
    if (this.round.pre_competitive_questionnaire) {
      return Object.assign({}, this.round.pre_competitive_questionnaire);
    } else {
      return null;
    }
  }

  saveFinalState(state: FinalState) {
    this.round.post_competitive_questionnaire = Object.assign({}, state);
    state.round = this.getRoundInfo();
    this.db.savePostCompetitiveQuestionnaire(state).then(() => {
      this.sync.beginSync();
    })
  }

  getFinalState(): FinalState {
    if (this.round.post_competitive_questionnaire) {
      return Object.assign({}, this.round.post_competitive_questionnaire);
    } else {
      return null;
    }
  }

  getHoles(): Hole[] {
    return Object.assign([], this.round.holes);
  }

  getHole(number: number): Hole {
    let index = number - 1;
    let hole: Hole;
    if (this.round.holes) {
      if (this.round.holes[index]) {
        hole = Object.assign({}, this.round.holes[index]);
      }
    }
    if (!hole) {
      hole = new Hole();
      hole.number = number;
    }
    return hole;
  }

  saveDefaultHole(hole: Hole) {
    let holeToSave: Hole = new Hole();
    holeToSave.number = hole.number;
    holeToSave.par = hole.par;
    holeToSave.design = hole.design;
    holeToSave.round = hole.round;
    this.db.saveDefaultHole(holeToSave).then(() => {
      this.sync.beginSync();
    })
  }

  setHoleInfo(hole: Hole) {
    let index = hole.number - 1;
    if (!this.round.holes) {
      this.round.holes = [];
    }
    if (!this.round.holes[index]) {
      let newHole = new Hole();
      newHole.number = hole.number;
      this.round.holes[index] = newHole;
    }
    this.round.holes[index].par = hole.par;
    this.round.holes[index].design = hole.design;
    hole.round = this.getRoundInfo();
    this.db.saveHole(hole);
  }

  setHoleData(hole: Hole) {
    let index = hole.number - 1;
    if (!this.round.holes) {
      this.round.holes = [];
    }
    this.round.holes[index] = hole;
    hole.round = this.getRoundInfo();
    this.db.saveHole(hole, true).then(() => {
      this.sync.beginSync();
    })
  }

  deleteHolePlay(holeNumber: number): Promise<any> {
    return new Promise((resolve, reject) => {
      let hole = Object.assign({}, this.getHole(holeNumber));
      hole.round = this.getRoundInfo();
      this.db.setHolePlayToDelete(hole).then(() => {
        this.sync.beginSync();
        this.setActiveRound(this.round).then(() => {
          resolve();
        }).catch(error => {
          reject(error);
        })
      }).catch(error => {
        reject(error);
      })
    })
  }

  deleteRound() {
    let round = Object.assign({}, this.round);
    this.reset();
    return this.roundsProvider.deleteRound(round);
  }

  getNextStep() {
    if (this.round.is_completed_locally || !this.round.hasOwnProperty('id')) {
      return 'completed';
    } else {
      if (!this.round.pre_competitive_questionnaire) {
        return 'state-initial';
      }
      if (this.round.holes && this.round.holes.length > 0) {
        let startIndex = 0;
        for (let i = 0; i < this.round.played_holes; i++) {
          if (this.round.holes[i] && this.round.holes[i].strikes) {
            startIndex = i;
            break;
          }
        }
        for (let i = 0; i < this.round.played_holes; i++) {
          let index = (i + startIndex) % this.round.played_holes;
          if (!this.round.holes[index] || !this.round.holes[index].strikes || !this.round.holes[index].strikes.length) {
            return 'hole-' + index;
          }
        }
        if (!this.round.post_competitive_questionnaire) {
          return 'state-final';
        }
        return 'readyToConfirm';
      } else {
        if (!this.round.pre_competitive_questionnaire) {
          return 'state-initial';
        }
        return 'hole-0';
      }
    }
  }

  confirmRound(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.db.setRoundCompletedLocally(this.round).then(response => {
        this.sync.beginSync();
        resolve(response);
      }).catch(error => {
        reject(error);
      })
    })
  }

}
