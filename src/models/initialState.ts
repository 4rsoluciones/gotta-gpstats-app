import {BaseEntity, Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {Round} from "./round";

@Entity('initial-state')
export class InitialState extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  is_warmed_up: boolean;

  @Column()
  has_practiced: boolean;

  @Column()
  wants_to_play: boolean;

  @Column()
  has_strategy: boolean;

  @OneToOne(() => Round, round => round.pre_competitive_questionnaire, {
    onDelete: "CASCADE"
  })
  @JoinColumn()
  round: Round;

  /**
   * 0: no hay modificaciones
   * 1: hay modificaciones y se debe enviar al backend
   */
  @Column()
  modified: number;

}
