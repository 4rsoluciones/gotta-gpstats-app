import {Component} from '@angular/core';
import {IonicPage, ViewController} from 'ionic-angular';
import {RoundProvider, SelectOptionsProvider} from "../../providers/providers";
import {FinalState} from "../../models/finalState";

@IonicPage()
@Component({
  selector: 'page-load-final-state',
  templateUrl: 'load-final-state.html',
})
export class LoadFinalStatePage {
  options: any[];
  selected: number;
  clicked: boolean;

  constructor(private roundProvider: RoundProvider,
              private selectOptions: SelectOptionsProvider,
              private viewCtrl: ViewController) {
    this.selectOptions.getFinalStateOptions().then(response => {
      this.options = response;
    });
    this.clicked = false;
  }

  optionClicked(i) {
    if (!this.clicked) {
      this.clicked = true;
      this.selected = i;
      let state = new FinalState();
      state.feeling_status = this.options[this.selected].value;
      this.roundProvider.saveFinalState(state);
      setTimeout(() => {
        let nextStep = this.roundProvider.getNextStep();
        if (nextStep == 'readyToConfirm') {
          this.viewCtrl.dismiss({next: 'readyToConfirm'});
        } else {
          this.viewCtrl.dismiss();
        }
      }, 1500);
    }
  }

  dismissClicked() {
    this.viewCtrl.dismiss();
  }

}
