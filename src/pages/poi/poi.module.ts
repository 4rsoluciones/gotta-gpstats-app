import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {PoiPage} from './poi';
import {PipesModule} from "../../pipes/pipes.module";

@NgModule({
  declarations: [
    PoiPage,
  ],
  imports: [
    IonicPageModule.forChild(PoiPage),
    PipesModule
  ],
})
export class PoiPageModule {
}
