GPStats App

## Para preparar el entorno de trabajo:

```npm install```

Modificar el archivo `node_modules/typescript/lib/typescript.js` de la siguiente manera:

Reemplazar la funcion `nodeCanBeDecorated(node)` para que quede así:

```
  function nodeCanBeDecorated(node) {
    switch (node.kind) {
      case 229 /* ClassDeclaration */:
        // classes are valid targets
        return true;
      case 149 /* PropertyDeclaration */:
        // property declarations are valid if their parent is a class declaration.
        // return node.parent.kind === 229 /* ClassDeclaration */;
        return (node.parent && node.parent.kind === 229) || (node.original && node.original.parent && node.original.parent.kind === 229);
      case 153 /* GetAccessor */:
      case 154 /* SetAccessor */:
      case 151 /* MethodDeclaration */:
        // if this method has a body and its parent is a class declaration, this is a valid target.
        return node.body !== undefined &&
          // && node.parent.kind === 229 /* ClassDeclaration */;
          (node.parent && node.parent.kind === 229) || (node.original && node.original.parent && node.original.parent.kind === 229);
      case 146 /* Parameter */:
        // if the parameter's parent has a body and its grandparent is a class declaration, this is a valid target;
        // return node.parent.body !== undefined
        //     && (node.parent.kind === 152 /* Constructor */
        //         || node.parent.kind === 151 /* MethodDeclaration */
        //         || node.parent.kind === 154 /* SetAccessor */)
        //     && node.parent.parent.kind === 229 /* ClassDeclaration */;

        var parent = node.parent || (node.original && node.original.parent);
        return parent && parent.body !== undefined &&
          (parent.kind === 152
            || parent.kind === 151
            || parent.kind === 154) && parent.parent.kind === 229;
    }
    return false;
  }
```

### Para compilar para ios (desde PC)

Eliminar la carpeta `www`
Ejecutar el comando `ionic build --prod`
Copiar dentro de la carpeta `www` el archivo `config.xml` y la carpeta `resources`
