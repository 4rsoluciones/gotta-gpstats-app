import {Injectable} from '@angular/core';
import {Benefit} from "../../models/benefit";
import {ApiProvider} from "../api/api";

@Injectable()
export class BenefitsProvider {
  benefits: Array<Benefit>;
  more: boolean;

  constructor(private api: ApiProvider) {
    this.init();
  }

  init() {
    this.more = true;
  }

  clear() {
    delete this.benefits;
    this.init();
  }

  getBenefits(force?: boolean, limit?: number): Promise<Benefit[]> {
    return new Promise((resolve, reject) => {
      if (force) {
        this.more = true;
        delete this.benefits;
      }
      if (this.benefits && (this.benefits.length > 0)) {
        if (limit) {
          resolve(this.benefits.slice(0, limit));
        } else {
          resolve(this.benefits);
        }
      } else {
        this.api.getBenefits({}).then((benefits: Benefit[]) => {
          this.benefits = benefits;
          this.more = (benefits.length > 0);
          if (limit) {
            resolve(this.benefits.slice(0, limit));
          } else {
            resolve(this.benefits);
          }
        }).catch(error => {
          reject(error);
        })
      }
    })
  }

  getMore(limit: number, offset: number): Promise<Benefit[]> {
    if (this.more) {
      return new Promise((resolve, reject) => {
        if (this.benefits.length > (limit + offset)) {
          resolve(this.benefits.slice(offset, limit + offset));
        } else {
          this.api.getBenefits({
            limit: (limit + offset - this.benefits.length).toString(),
            offset: this.benefits.length.toString()
          }).then((benefits: Benefit[]) => {
            this.benefits = this.benefits.concat(benefits);
            this.more = (benefits.length > 0);
            resolve(this.benefits.slice(offset, limit + offset));
          }).catch(error => {
            reject(error);
          })
        }
      })
    } else {
      return Promise.resolve([]);
    }
  }

  canjear(benefit: Benefit): Promise<any> {
    return new Promise((resolve, reject) => {
      this.api.useCoupon({couponId: benefit.id.toString()}).then(() => {
        benefit.used = true;
        resolve(benefit);
      }).catch(error => {
        reject(error);
      });
    })
  }

}
