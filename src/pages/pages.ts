export const FirstRunPage = 'OnboardingPage';

export const MainPage = 'TabsPage';

export const Tab1Root = 'HomePage';
export const Tab2Root = 'RoundsPage';
export const Tab3Root = 'NearMePage';
export const Tab4Root = 'BenefitsPage';
export const Tab5Root = 'MenuPage';
