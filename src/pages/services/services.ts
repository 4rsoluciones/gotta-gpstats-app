import {Component} from '@angular/core';
import {IonicPage} from 'ionic-angular';
import {ErrorProvider, ServicesProvider} from "../../providers/providers";
import {Service} from "../../models/service";
import {InAppBrowser} from "@ionic-native/in-app-browser";

@IonicPage()
@Component({
  selector: 'page-services',
  templateUrl: 'services.html',
})
export class ServicesPage {
  loading: boolean;
  services: any;
  platforms: Array<string>;

  constructor(private error: ErrorProvider,
              private servicesProvider: ServicesProvider,
              private iab: InAppBrowser) {

  }

  ionViewWillEnter() {
    this.loading = true;
    this.servicesProvider.get().then(services => {
      this.services = services;
      this.platforms = Object.keys(this.services);
      this.loading = false;
    }).catch(error => {
      this.loading = false;
      this.error.handle({error: error, log: 'error', text: 'Ha ocurrido un error. Intente nuevamente más tarde.'});
    });
  }

  doRefresh(refresher) {
    this.servicesProvider.get(true).then(services => {
      this.services = services;
      this.platforms = Object.keys(this.services);
      refresher.complete();
    }).catch(error => {
      refresher.complete();
      this.error.handle({error: error, log: 'error', text: 'Ha ocurrido un error. Intente nuevamente más tarde.'});
    })
  }

  serviceClicked(service: Service, platform: string) {
    this.iab.create(this.servicesProvider.getServiceUrl(service, platform), '_system');
  }

}
