import {Component} from '@angular/core';
import {IonicPage, LoadingController} from 'ionic-angular';
import {ApiProvider, ErrorProvider, UserProvider} from "../../providers/providers";
import {InAppBrowser} from "@ionic-native/in-app-browser";

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  loginData: any;
  EMAIL_REGEXP = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;

  constructor(private api: ApiProvider,
              private errorHandler: ErrorProvider,
              private user: UserProvider,
              private loadingCtrl: LoadingController,
              private iab: InAppBrowser) {
    this.loginData = {};
  }

  loginClicked() {
    if (this.isFormValid()) {
      let loading = this.loadingCtrl.create();
      loading.present();
      window.localStorage['username'] = this.loginData.email;
      window.localStorage['password'] = this.loginData.password;
      this.user.login().then(() => {
        loading.dismiss();
      }).catch(error => {
        loading.dismiss();
        this.errorHandler.handle({error: error, text: 'Usuario o contraseña incorrectos', log: 'error'});
      })
    }
  }

  forgotPasswordClicked() {
    this.iab.create(this.api.getCentralizadorConfig().url + 'password/reset', '_system');
  }

  isFormValid(): boolean {
    if (!this.loginData.email) {
      this.errorHandler.handle({text: 'Debe ingresar un email'});
      return false;
    }
    if (!this.EMAIL_REGEXP.test(this.loginData.email)) {
      this.errorHandler.handle({text: 'Debe ingresar un email válido'});
      return false;
    }
    if (!this.loginData.password) {
      this.errorHandler.handle({text: 'Debe ingresar una contraseña'});
      return false;
    }
    return true;
  }

  signUpClicked() {
    this.iab.create(this.api.getCentralizadorConfig().url + 'register', '_system');
  }

}
