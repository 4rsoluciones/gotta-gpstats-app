import {Injectable} from '@angular/core';
import {ApiProvider} from "../api/api";
import {Field} from "../../models/field";
import {DatabaseProvider} from "../database/database";
import {SyncProvider} from "../sync/sync";
import {Observable} from "rxjs/Observable";

@Injectable()
export class CoursesProvider {

  constructor(private api: ApiProvider,
              private db: DatabaseProvider,
              private sync: SyncProvider) {

  }

  searchCourse(country: string, name: string): Observable<Field[]> {
    return this.api.getCourses({country: country, search: name});
  }

  saveDefault(field: Field) {
    field.synchronized = 0;
    this.db.saveField(field).then(() => {
      this.sync.beginSync();
    })
  }

}
