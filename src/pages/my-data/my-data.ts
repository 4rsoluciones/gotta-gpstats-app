import {Component} from '@angular/core';
import {IonicPage} from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-my-data',
  templateUrl: 'my-data.html',
})
export class MyDataPage {
  tab1Root: string = 'ProfilePage';
  tab2Root: string = 'AccountPage';

  constructor() {

  }

}
