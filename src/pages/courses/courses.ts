import {Component} from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {
  CoursesProvider,
  ErrorProvider,
  RoundProvider,
  SelectOptionsProvider,
  SettingsProvider
} from "../../providers/providers";

@IonicPage()
@Component({
  selector: 'page-courses',
  templateUrl: 'courses.html',
})
export class CoursesPage {
  countries: any[];
  movements: any[];
  types: any[];
  parOptions: any[];
  fields: any[];
  searchCountry: any;
  searchName: string;
  searchTimeout: any;
  searchObservable: any;
  loading: boolean;
  neverSearched: boolean;
  selected: number;
  callback: any;
  selectOpts = {
    cssClass: 'secondary-select'
  };

  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private coursesProvider: CoursesProvider,
              private roundProvider: RoundProvider,
              private settings: SettingsProvider,
              private selectOptions: SelectOptionsProvider,
              private modalCtrl: ModalController,
              private error: ErrorProvider) {
    let promises = [];
    promises.push(this.selectOptions.getCountries().then(response => {
      this.countries = response;
    }));
    promises.push(this.selectOptions.getMovements().then(response => {
      this.movements = response;
    }));
    promises.push(this.selectOptions.getFieldTypes().then(response => {
      this.types = response;
    }));
    promises.push(this.selectOptions.getParOptions().then(response => {
      this.parOptions = response;
    }));
    promises.push(this.settings.get().then(response => {
      this.searchCountry = response.country;
    }));
    Promise.all(promises).then(() => {
      // this.searchCountry = this.countries[0].name;
    });
    this.loading = false;
    this.neverSearched = true;
    this.callback = this.navParams.get('callback');
  }

  searchChanged() {
    if (this.searchTimeout) {
      clearTimeout(this.searchTimeout);
    }
    if (this.searchName.trim() != '') {
      this.searchTimeout = setTimeout(() => {
        this.loading = true;
        if (this.searchObservable) {
          this.searchObservable.unsubscribe();
        }
        this.searchObservable = this.coursesProvider.searchCourse(this.searchCountry, this.searchName).subscribe(fields => {
          this.neverSearched = false;
          this.loading = false;
          this.fields = fields;
        }, error => {
          this.neverSearched = false;
          this.loading = false;
          error.log = 'error';
          this.error.handle(error);
        });
      }, 300);
    } else {
      this.fields = [];
    }
  }

  courseSelected() {
    this.callback().then(() => {
      this.navCtrl.pop();
    })
  }

  courseClicked(i) {
    this.selected = i;
    this.roundProvider.setCourse(this.fields[i]);
    setTimeout(() => {
      this.courseSelected();
    }, 1000);
  }

  newCourseClicked() {
    let newCourseModal = this.modalCtrl.create('NewCoursePage');
    newCourseModal.onDidDismiss(data => {
      if (data) {
        switch (data.next) {
          case 'continue-round': {
            this.courseSelected();
            break;
          }
          case 'goroot': {
            this.navCtrl.popToRoot();
            break;
          }
        }
      }
    });
    newCourseModal.present();
  }

}
