import {Injectable} from '@angular/core';
import {Round} from "../../models/round";
import {SyncProvider} from "../sync/sync";
import {DatabaseProvider} from "../database/database";
import {ApiProvider} from "../api/api";

@Injectable()
export class RoundsProvider {
  roundsOffset: number;
  moreRounds: boolean;
  rounds: Array<Round>;
  limit: number;

  constructor(private sync: SyncProvider,
              private db: DatabaseProvider,
              private api: ApiProvider) {
    this.init();
  }

  init() {
    this.roundsOffset = 0;
    this.moreRounds = true;
    this.rounds = [];
    this.limit = 20;
  }

  clear() {
    delete this.roundsOffset;
    delete this.moreRounds;
    delete this.rounds;
    delete this.limit;
    this.init();
  }

  getIncompleteRounds(params: { limit?: number }): Promise<Round[]> {
    return new Promise((resolve, reject) => {
      if (window.localStorage['synchronized'] && JSON.parse(window.localStorage['synchronized'])) {
        this.db.getRounds(params).then((rounds: Round[]) => {
          rounds.sort((a: Round, b: Round) => {
            return (new Date(b.performed_at).getTime() - new Date(a.performed_at).getTime());
          });
          if (params.limit) {
            resolve(rounds.slice(0, params.limit));
          } else {
            resolve(rounds);
          }
        }).catch(error => {
          reject(error);
        })
      } else {
        this.sync.updateIncompleteRounds().then(() => {
          window.localStorage['synchronized'] = true;
          this.db.getRounds(params).then((rounds: Round[]) => {
            rounds.sort((a: Round, b: Round) => {
              return (new Date(b.performed_at).getTime() - new Date(a.performed_at).getTime());
            });
            if (params.limit) {
              resolve(rounds.slice(0, params.limit));
            } else {
              resolve(rounds);
            }
          }).catch(error => {
            reject(error);
          })
        }).catch(error => {
          window.localStorage['synchronized'] = false;
          reject(error);
        })
      }
    })
  }

  newRound(newRound: Round): Promise<Round> {
    newRound.synchronized = null;
    newRound.field.synchronized = -1;
    newRound.modified = 0;
    newRound.is_completed_locally = false;
    newRound.is_completed = false;
    return new Promise((resolve, reject) => {
      this.db.saveRound(newRound).then(response => {
        this.sync.beginSync();
        resolve(response);
      }).catch(error => {
        reject(error);
      })
    })
  }

  getRounds(params: { incomplete?: boolean, limit?: number, force?: boolean }): Promise<Round[]> {
    return new Promise((resolve, reject) => {
      if (params.force) {
        this.moreRounds = true;
        this.rounds = [];
      }
      if (this.rounds && (this.rounds.length > 0)) {
        if (params.limit) {
          resolve(this.rounds.slice(0, params.limit));
        } else {
          resolve(this.rounds);
        }
      } else {
        if (this.moreRounds) {
          this.api.getRounds({incomplete: false}).then(response => {
            response.sort((a: Round, b: Round) => {
              return (new Date(b.performed_at).getTime() - new Date(a.performed_at).getTime());
            });
            this.roundsOffset = response.length;
            this.moreRounds = (response.length > 0);
            this.rounds = response;
            if (params.limit) {
              resolve(this.rounds.slice(0, params.limit));
            } else {
              resolve(this.rounds);
            }
          }).catch(error => {
            reject(error);
          })
        } else {
          resolve([]);
        }
      }
    })
  }

  getMoreRounds(): Promise<Round[]> {
    if (this.moreRounds) {
      return new Promise((resolve, reject) => {
        if (this.rounds.length > (this.limit + this.roundsOffset)) {
          resolve(this.rounds.slice(this.roundsOffset, this.limit + this.roundsOffset));
        } else {
          this.api.getRounds({incomplete: false, limit: this.limit, offset: this.roundsOffset}).then(response => {
            response.sort((a: Round, b: Round) => {
              return (new Date(b.performed_at).getTime() - new Date(a.performed_at).getTime());
            });
            this.roundsOffset += response.length;
            this.moreRounds = (response.length > 0);
            this.rounds = this.rounds.concat(response);
            resolve(response);
          }).catch(error => {
            reject(error);
          })
        }
      })
    } else {
      return Promise.resolve([]);
    }
  }

  deleteRound(round: Round) {
    return new Promise((resolve, reject) => {
      if (round.hasOwnProperty('id')) {
        this.db.setRoundToDelete(round).then(() => {
          this.sync.beginSync();
          resolve();
        }).catch(error => {
          reject(error);
        });
        for (let i = 0; i < this.rounds.length; i++) {
          if (this.rounds[i].id == round.id) {
            this.rounds.splice(i, 1);
          }
        }
      } else {
        this.api.deleteRound({id: round.synchronized}).then(() => {
          for (let i = 0; i < this.rounds.length; i++) {
            if (this.rounds[i].synchronized == round.synchronized) {
              this.rounds.splice(i, 1);
            }
          }
          resolve();
        }).catch(error => {
          reject(error);
        })
      }
    })
  }

}
