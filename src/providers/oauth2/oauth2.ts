import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse
} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {Events} from "ionic-angular";
import {Http} from "@angular/http";
import {environment} from "../../environment/environment";

@Injectable()
export class Oauth2Provider implements HttpInterceptor {
  config: any;
  canRefresh: any;
  production: boolean;
  apiConfig: any;

  constructor(private http: Http,
              private events: Events) {
    this.canRefresh = true;
    this.production = environment.production;
    this.apiConfig = environment.apiConfig;
  }

  getCentralizadorConfig() {
    return this.production ? this.apiConfig.centralizador.prod : this.apiConfig.centralizador.dev;
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    let dupReq: any = req.clone({headers: req.headers.append('Access-Control-Allow-Origin', '*')});


    this.config = this.getCentralizadorConfig();

    if (window.localStorage['OAUTH2']) {
      let auth2Data: any = JSON.parse(window.localStorage['OAUTH2']);

      dupReq = req.clone({
        setHeaders: {'Authorization': 'Bearer ' + auth2Data.access_token, 'Accept': 'application/json'}
      });

    }
    else if (dupReq.url.indexOf(this.config.tokenUrl) != -1) {
      let username: string;
      let password: string;
      username = window.localStorage['username'];
      password = window.localStorage['password'];
      window.localStorage.removeItem('username');
      window.localStorage.removeItem('password');

      if (username && password) {
        dupReq.body = {
          username: username,
          password: password,
          grant_type: 'password',
          client_id: this.config.clientId,
          client_secret: this.config.clientSecret
        };
      }
    }

    return next.handle(dupReq)
      .map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse && event.type != 0 && event.url.indexOf(this.config.tokenUrl) != -1 && event.status == 200) {
          window.localStorage['OAUTH2'] = JSON.stringify(event.body);
        }
        return event;
      })
      .catch((err: any) => {
        if (err instanceof HttpErrorResponse && err.status != 0) {
          if (err.status === 401 && err.url.indexOf(this.config.tokenUrl) === -1) {
            return this.refreshToken(req, next);
          }
          else {
            return Observable.throw(err);
          }
        }
        else {
          return Observable.throw(err);
        }
      });
  }

  refreshToken(req: HttpRequest<any>, next: HttpHandler): Promise<any> {
    if (this.canRefresh) {
      this.canRefresh = false;
      return new Promise((resolve, reject) => {
        let auth2Data: any = JSON.parse(window.localStorage['OAUTH2']);
        this.http.post(this.config.url + this.config.tokenUrl, {
          refresh_token: auth2Data.refresh_token,
          grant_type: 'refresh_token',
          client_id: this.config.clientId,
          client_secret: this.config.clientSecret,
          scope: ''
        }).toPromise().then(res => {
          auth2Data = JSON.parse(window.localStorage['OAUTH2']);
          auth2Data = res.json();
          window.localStorage['OAUTH2'] = JSON.stringify(auth2Data);
          let dupReq = req.clone({
            setHeaders: {'Authorization': 'Bearer ' + auth2Data.access_token, 'Accept': 'application/json'}
          });
          this.canRefresh = true;
          next.handle(dupReq).subscribe(res => {
            if (res instanceof HttpResponse && res.type != 0)
              resolve(res);
          });
        }).catch(error => {
          this.canRefresh = true;
          this.events.publish('logout');
          let body = error.json();
          body.text = 'Su sesión ha caducado.';
          error._body = JSON.stringify(body);
          error.status = 401;
          reject(error);
        });
      })
    } else {
      return Promise.reject({});
    }
  }

}
