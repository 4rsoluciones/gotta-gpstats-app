import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {NewCoursePage} from './new-course';
import {HorizontalSpinnerModule} from "@4r/horizontal-spinner/horizontal-spinner.module";

@NgModule({
  declarations: [
    NewCoursePage,
  ],
  imports: [
    IonicPageModule.forChild(NewCoursePage),
    HorizontalSpinnerModule
  ],
})
export class NewCoursePageModule {
}
