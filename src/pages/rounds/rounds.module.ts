import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {RoundsPage} from './rounds';
import {PipesModule} from "../../pipes/pipes.module";

@NgModule({
  declarations: [
    RoundsPage,
  ],
  imports: [
    IonicPageModule.forChild(RoundsPage),
    PipesModule
  ],
})
export class RoundsPageModule {
}
