import {BaseEntity, Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {Play} from "./play";

@Entity('emotional-questionnaire')
export class EmotionalQuestionnaire extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  feelsUptight: boolean;

  @Column()
  feelsDistrustful: boolean;

  @Column()
  feelsAnnoyed: boolean;

  @Column()
  feelsFrustrated: boolean;

  @Column()
  feelsTrustful: boolean;

  @Column()
  feelsEnjoying: boolean;

  @Column()
  feelsFocused: boolean;

  @OneToOne(() => Play, play => play.emotionalQuestionnaire, {
    onDelete: "CASCADE"
  })
  @JoinColumn()
  play: Play;

}
