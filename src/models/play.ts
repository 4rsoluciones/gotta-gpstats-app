import {BaseEntity, Column, Entity, ManyToOne, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {Hole} from "./hole";
import {EmotionalQuestionnaire} from "./emotionalQuestionnaire";
import {FoodQuestionnaire} from "./foodQuestionnaire";
import {PhysicalQuestionnaire} from "./physycalQuestionnaire";

@Entity('play')
export class Play extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  order: number;

  @Column()
  type: string;

  @Column()
  distance: string;

  @Column()
  result: string;

  @Column({nullable: true})
  reason: string;

  @Column({nullable: true})
  ballFlight: string;

  @Column({nullable: true})
  club: string;

  @Column('simple-array', {nullable: true})
  strikeLies: Array<string>;

  @Column('simple-array', {nullable: true})
  landSlopes: Array<string>;

  @OneToOne(() => EmotionalQuestionnaire, emotionalQuestionnaire => emotionalQuestionnaire.play, {
    cascadeInsert: true,
    cascadeUpdate: true
  })
  emotionalQuestionnaire: EmotionalQuestionnaire;

  @OneToOne(() => FoodQuestionnaire, foodQuestionnaire => foodQuestionnaire.play, {
    cascadeInsert: true,
    cascadeUpdate: true
  })
  foodQuestionnaire: FoodQuestionnaire;

  @OneToOne(() => PhysicalQuestionnaire, physicalQuestionnaire => physicalQuestionnaire.play, {
    cascadeInsert: true,
    cascadeUpdate: true
  })
  physicalQuestionnaire: PhysicalQuestionnaire;

  @ManyToOne(() => Hole, hole => hole.strikes, {
    cascadeInsert: true,
    cascadeUpdate: true,
    onDelete: "CASCADE"
  })
  hole: Hole;

}
