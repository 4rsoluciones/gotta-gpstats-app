import {Component, ViewChild} from '@angular/core';
import {Content, IonicPage, ModalController, NavController, Slides, ToastController} from 'ionic-angular';
import {ErrorProvider, NearMeProvider} from "../../providers/providers";
import {NearMePlace} from "../../models/nearMePlace";
import {Diagnostic} from "@ionic-native/diagnostic";
import {Geolocation} from "@ionic-native/geolocation";
import {GoogleMap, GoogleMapOptions, GoogleMaps, GoogleMapsEvent, ILatLng, Marker} from "@ionic-native/google-maps";

@IonicPage()
@Component({
  selector: 'page-near-me',
  templateUrl: 'near-me.html',
})
export class NearMePage {
  @ViewChild(Content) content: Content;
  @ViewChild('nearMeSlides') slides: Slides;
  map: GoogleMap;
  viewLoaded: boolean;
  nearMePlaces: any[];
  slidesUp: boolean;
  filters: any[];
  markerMe: Marker;
  markersPlaces: Array<Marker>;
  mapZoom: number;

  constructor(private modalCtrl: ModalController,
              private navCtrl: NavController,
              private toastCtrl: ToastController,
              private diagnostic: Diagnostic,
              private geolocation: Geolocation,
              private nearMeProvider: NearMeProvider,
              private error: ErrorProvider) {
    this.nearMeProvider.getFilters().then(response => {
      this.filters = response;
    });
    this.nearMePlaces = [];
  }

  ionViewDidLoad() {
    this.initMapAndLocation().then(() => {
      this.updateNearMe(true);
      this.viewLoaded = true;
    }).catch(error => {
      this.error.handle({error: error, log: 'error'});
    })
  }

  ionViewWillEnter() {
    if (this.viewLoaded) {
      this.initMap().then(() => {
        this.updateLocationOnMap();
        this.updateNearMe();
        this.resetMapCamera(true);
      }).catch(error => {
        this.error.handle({error: error, log: 'error'});
      })
    }
  }

  ionViewDidLeave() {
    this.map.remove();
    delete this.markerMe;
    delete this.markersPlaces;
  }

  initMapAndLocation(): Promise<any> {
    return new Promise((resolve, reject) => {
      let promises = [];
      promises.push(this.initMap());
      promises.push(this.updateLocation());
      Promise.all(promises).then(() => {
        this.updateLocationOnMap();
        this.resetMapCamera();
        resolve();
      }).catch(error => {
        reject(error);
      })
    })
  }

  resetMapCamera(noAnimate?: boolean) {
    this.map.off(GoogleMapsEvent.CAMERA_MOVE_END);
    this.map.animateCamera({target: this.markerMe.getPosition(), zoom: 9.58, duration: (noAnimate ? 1 : 1000)}).then(() => {
      this.mapZoom = 9.58;
      this.watchRadius();
    })
  }

  initMap(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.loadMap().then(() => {
        this.addMarkerMe().then(() => {
          resolve();
        }).catch(error => {
          this.error.handle({
            error: error,
            log: 'error',
            text: 'Hubo un error al agregar su posición en el mapa. Intente nuevamente más tarde.'
          });
          reject(error);
        })
      }).catch(error => {
        this.error.handle({
          error: error,
          log: 'error',
          text: 'Hubo un error al cargar el mapa. Intente nuevamente más tarde.'
        });
        reject(error);
      });
    })
  }

  addMarkerMe() {
    return new Promise((resolve, reject) => {
      this.map.addMarker({
        position: {
          lat: 0,
          lng: 0
        },
        icon: {
          url: "assets/imgs/me-marker.png",
          size: {
            width: 60,
            height: 70
          }
        },
        visible: false
      }).then((marker: Marker) => {
        this.markerMe = marker;
        resolve();
      }).catch(error => {
        reject(error);
      })
    })
  }

  loadMap(): Promise<any> {
    return new Promise((resolve, reject) => {
      let mapOptions: GoogleMapOptions = {
        controls: {
          compass: true,
          mapToolbar: false,
          zoom: false
        },
        gestures: {
          scroll: true,
          tilt: true,
          rotate: true,
          zoom: true
        }
      };
      this.map = GoogleMaps.create('map', mapOptions);
      this.map.one(GoogleMapsEvent.MAP_READY).then(() => {
        resolve();
      }).catch(error => {
        reject(error);
      });
    })
  }

  watchRadius() {
    this.map.on(GoogleMapsEvent.CAMERA_MOVE_END).subscribe(() => {
      if (this.mapZoom > (this.map.getCameraZoom() * 1.1)) { // Tolerancia del 10%
        this.zoomChanged();
      }
    })
  }

  zoomChanged() {
    this.mapZoom = this.map.getCameraZoom();
    this.nearMeProvider.setRadius(this.getRadius());
    this.updateNearMe(true);
  }

  updateLocation(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.getPermissions().then(() => {
        this.getLocation().then(() => {
          resolve();
        }).catch(error => {
          this.error.handle({
            error: error,
            log: 'error',
            text: 'No se pudo obtener su ubicación. Intente nuevamente más tarde'
          });
          reject(error);
        })
      }).catch(error => {
        this.error.handle({
          error: error,
          log: 'error',
          text: 'No se pudo obtener su ubicación. Intente nuevamente más tarde'
        });
        reject(error);
      })
    })
  }

  getPermissions(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.diagnostic.isLocationEnabled().then(response => {
        if (response) {
          this.diagnostic.isLocationAvailable().then(response => {
            if (response) {
              resolve();
            } else {
              this.diagnostic.requestLocationAuthorization().then(() => {
                resolve();
              }).catch(error => {
                reject(error);
              });
            }
          }).catch(error => {
            reject(error);
          })
        } else {
          this.toast('Por favor habilite la ubicación e intente nuevamente.');
        }
      }).catch(error => {
        reject(error);
      })
    })
  }

  getLocation(): Promise<any> {
    return new Promise((resolve, reject) => {
      let locationSubscription = this.geolocation.watchPosition()
        .filter((p) => p.coords !== undefined)
        .subscribe(position => {
          this.nearMeProvider.setPosition(position.coords.latitude, position.coords.longitude);
          locationSubscription.unsubscribe();
          resolve();
        }, error => {
          locationSubscription.unsubscribe();
          reject(error);
        })
    })
  }

  updateLocationOnMap() {
    let pos = this.nearMeProvider.getPosition();
    this.markerMe.setPosition(pos);
    this.markerMe.setVisible(true);
  }

  updateNearMe(force?: boolean): Promise<any> {
    return new Promise((resolve, reject) => {
      this.nearMePlaces = [];
      this.nearMeProvider.getPlaces(force).then(response => {
        this.nearMePlaces = response;
        this.updateMarkers();
        resolve();
      }).catch(error => {
        this.error.handle(error);
        reject(error);
      })
    })
  }

  updateMarkers(): Promise<any> {
    if (this.markersPlaces) {
      for (let i = 0; i < this.markersPlaces.length; i++) {
        this.markersPlaces[i].remove();
      }
    }
    this.markersPlaces = [];
    let promises = [];
    for (let i = 0; i < this.nearMePlaces.length; i++) {
      promises.push(this.addPlaceToMap(this.nearMePlaces[i]));
    }
    return Promise.all(promises);
  }

  addPlaceToMap(place: NearMePlace): Promise<any> {
    return this.map.addMarker({
      position: {lat: place.lat, lng: place.lng},
      disableAutoPan: false,
      draggable: false,
      icon: {
        url: "assets/imgs/places-marker.png",
        size: {
          width: 60,
          height: 70
        }
      },
      "place": place
    }).then(marker => {
      this.markersPlaces.push(marker);
      marker.on(GoogleMapsEvent.MARKER_CLICK)
        .subscribe(() => {
          this.placeMarkerClicked(marker);
        });
    })
  }

  placeMarkerClicked(marker) {
    this.goPlace(marker.get('place'));
  }

  locateClicked() {
    this.updateLocation().then(() => {
      this.updateLocationOnMap();
      this.resetMapCamera();
      this.updateNearMe(true);
    });
  }

  getRadius(): number {
    let centerDeg = this.markerMe.getPosition();
    let cornerDeg = this.getFurthestPoint(this.markerMe.getPosition());
    let center = this.toRad(centerDeg);
    let corner = this.toRad(cornerDeg);
    let r = 6371000;// Radio de la tierra en metros
    return r * Math.acos(Math.sin(center.lat) * Math.sin(corner.lat) + Math.cos(center.lat) * Math.cos(corner.lat) * Math.cos(corner.lng - center.lng));
  }

  toRad(p: ILatLng): ILatLng {
    let rad = 57.2958;// factor para pasar grados a radianes
    return {lat: p.lat / rad, lng: p.lng / rad};
  }

  getFurthestPoint(o: ILatLng): ILatLng {
    let pos = this.map.getCameraPosition();
    let corners = [pos['farLeft'], pos['farRight'], pos['nearLeft'], pos['nearRight'], pos['northeast'], pos['southwest']];
    let corner: ILatLng = {lat: corners[0].lat, lng: corners[0].lng};
    for (let i = 1; i < corners.length; i++) {
      if (this.getDistance(o, corner) < this.getDistance(o, corners[i])) {
        corner = corners[i];
      }
    }
    return corner;
  }

  getDistance(p1: ILatLng, p2: ILatLng): number {
    return Math.sqrt(Math.pow(p2.lat - p1.lat, 2) + Math.pow(p2.lng - p1.lng, 2));
  }

  showFiltersClicked() {
    this.slidesUp = !this.slidesUp;
    this.content.resize();
    setTimeout(() => {
      this.map.setDiv('map');
    }, 300);
    setTimeout(() => {
      this.map.setDiv('map');
    }, 500);
    setTimeout(() => {
      this.placesSlideChanged();
    }, 1000)
  }

  closeFiltersClicked() {
    this.slidesUp = false;
    this.content.resize();
    setTimeout(() => {
      this.map.setDiv('map');
    }, 300);
    setTimeout(() => {
      this.map.setDiv('map');
    }, 500);
  }

  placesSlideChanged() {
    let placeId = this.nearMePlaces[this.slides.getActiveIndex()].id;
    let icon = {
      url: "assets/imgs/places-marker.png",
      size: {
        width: 60,
        height: 70
      }
    };
    let iconSelected = {
      url: "assets/imgs/selected-places-marker.png",
      size: {
        width: 60,
        height: 70
      }
    };
    for (let i = 0; i < this.markersPlaces.length; i++) {
      if (this.markersPlaces[i].get('place').id == placeId) {
        this.markersPlaces[i].setIcon(iconSelected);
      } else {
        this.markersPlaces[i].setIcon(icon);
      }
    }
  }

  filtersClicked() {
    let filtersModal = this.modalCtrl.create('SelectPage', {
      title: 'FILTROS',
      subtitle: 'CATEGORÍAS',
      items: this.filters
    });
    filtersModal.onWillDismiss(data => {
      if (data) {
        this.nearMeProvider.setFilters(data);
        this.updateNearMe(true);
      }
    });
    filtersModal.present();
  }

  placeClicked(place: NearMePlace) {
    this.goPlace(place);
  }

  goPlace(place: NearMePlace) {
    this.navCtrl.push('PoiPage', {place: place});
  }

  toast(msg: string) {
    this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    }).present();
  }

}
