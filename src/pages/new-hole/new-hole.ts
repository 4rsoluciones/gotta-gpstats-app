import {Component} from '@angular/core';
import {IonicPage, ModalController, NavParams, ViewController} from 'ionic-angular';
import {ErrorProvider, RoundProvider, SelectOptionsProvider} from "../../providers/providers";
import {Hole} from "../../models/hole";

@IonicPage()
@Component({
  selector: 'page-new-hole',
  templateUrl: 'new-hole.html',
})
export class NewHolePage {
  roundInfo: any;
  hole: Hole;
  parTypes: any[];
  disenioTypes: any[];
  selectedPar: any;
  selectedDisenio: any;
  edit: boolean;

  constructor(private navParams: NavParams,
              private modalCtrl: ModalController,
              private viewCtrl: ViewController,
              private error: ErrorProvider,
              private selectOptions: SelectOptionsProvider,
              private roundProvider: RoundProvider) {
    let holeNumber = this.navParams.get('hole');
    this.edit = this.navParams.get('edit');
    this.roundInfo = this.roundProvider.getRoundInfo();
    this.hole = this.roundProvider.getHole(holeNumber);
    let promises = [];
    promises.push(this.selectOptions.getHolePar().then(response => {
      this.parTypes = response;
    }));
    promises.push(this.selectOptions.getHoleDesigns().then(response => {
      this.disenioTypes = response;
    }));
    if (this.edit) {
      Promise.all(promises).then(() => {
        this.selectedPar = this.getHoleParByValue(this.hole.par);
        this.selectedDisenio = this.getHoleDesignByValue(this.hole.design);
      })
    }
  }

  getHoleParByValue(value: any) {
    for (let i = 0; i < this.parTypes.length; i++) {
      if (this.parTypes[i].value == value) {
        return i;
      }
    }
    return -1;
  }

  getHoleDesignByValue(value: any) {
    for (let i = 0; i < this.disenioTypes.length; i++) {
      if (this.disenioTypes[i].value == value) {
        return i;
      }
    }
    return -1;
  }

  parClicked() {
    if ((this.selectedPar != undefined) && this.parTypes[this.selectedPar].value == 3) {
      this.selectedDisenio = 1;
    }
  }

  designClicked() {
    if ((this.selectedPar != undefined) && this.parTypes[this.selectedPar].value == 3) {
      this.selectedDisenio = 1;
    }
  }

  saveClicked() {
    if (this.isValid()) {
      this.hole.par = this.parTypes[this.selectedPar].value;
      this.hole.design = this.disenioTypes[this.selectedDisenio].value;
      let alertModal = this.modalCtrl.create('AlertPage', {
        image: 'assets/imgs/iconos/hoyo-creado.png',
        title: '¡Hoyo creado!',
        subtitle: 'Gracias por colaborar con GPStats.\n¡Seguí cargando tu jugada en el hoyo!',
        okButton: 'CARGAR JUGADA',
        cancelButton: 'Continuar más tarde.'
      });
      this.roundProvider.setHoleInfo(this.hole);
      if (!this.edit) {
        this.roundProvider.saveDefaultHole(this.hole);
      }
      alertModal.onDidDismiss(data => {
        if (data) {
          let showPlayPage = this.navParams.get('showPlayPage');
          showPlayPage(this.hole.number).then(() => {
            this.viewCtrl.dismiss();
          }).catch(error => {
            this.error.handle({error: error, log: 'error', text: 'Ha ocurrido un error. Intente nuevamente más tarde.'});
            this.viewCtrl.dismiss();
          })
        } else {
          this.viewCtrl.dismiss();
        }
      });
      alertModal.present();
    } else {
      this.error.handle({text: 'Debe seleccionar par y diseño para cargar el hoyo'});
    }
  }

  isValid(): boolean {
    if (this.selectedPar == undefined) {
      return false;
    }
    if (this.selectedDisenio == undefined) {
      return false;
    }
    return true;
  }

}
