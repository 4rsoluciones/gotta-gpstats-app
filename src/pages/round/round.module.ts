import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {RoundPage} from './round';
import {HorizontalSpinnerModule} from "@4r/horizontal-spinner/horizontal-spinner.module";
import {PipesModule} from "../../pipes/pipes.module";
import {ComponentsModule} from "../../components/components.module";

@NgModule({
  declarations: [
    RoundPage,
  ],
  imports: [
    IonicPageModule.forChild(RoundPage),
    ComponentsModule,
    HorizontalSpinnerModule,
    PipesModule
  ],
})
export class RoundPageModule {
}
