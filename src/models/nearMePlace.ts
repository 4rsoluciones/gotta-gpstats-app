export class NearMePlace {
  id: number;
  img: string;
  logo: string;
  category: string;
  title: string;
  description: string;
  address: string;
  lat: number;
  lng: number;
}
