import {Component} from "@angular/core";
import {NavParams, ViewController} from "ionic-angular";

@Component({
  selector: 'page-options',
  templateUrl: 'options.html'
})
export class OptionsPage {
  options: any[];
  selected: any[];

  constructor(private navParams: NavParams,
              private viewCtrl: ViewController) {
    this.options = this.navParams.get('options');
    this.selected = [];
  }

  ionViewDidEnter() {
    let elem = this.viewCtrl.contentRef().nativeElement;
    let popoverViewport = elem.parentElement.parentElement;
    popoverViewport.style.maxHeight = '100%';
    elem.style.height = popoverViewport.offsetHeight + 'px';
    elem.lastChild.style.position = 'absolute';
  }

  optionClicked(index) {
    if (this.selected.indexOf(index) == -1) {
      this.selected.push(index);
    } else {
      this.selected.splice(this.selected.indexOf(index), 1);
    }
  }

  okClicked() {
    let answers = [];
    for (let i = 0; i < this.selected.length; i++) {
      answers.push(this.options[this.selected[i]]);
    }
    this.viewCtrl.dismiss(answers);
  }

}
