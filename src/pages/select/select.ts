import {Component} from '@angular/core';
import {IonicPage, NavParams, ViewController} from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-select',
  templateUrl: 'select.html',
})
export class SelectPage {
  title: string;
  subtitle: string;
  items: any[];

  constructor(private viewCtrl: ViewController,
              private navParams: NavParams) {
    this.title = this.navParams.get('title');
    this.subtitle = this.navParams.get('subtitle');
    this.items = this.navParams.get('items');
  }

  closeClicked() {
    this.viewCtrl.dismiss();
  }

  saveClicked() {
    this.viewCtrl.dismiss(this.items);
  }

}
