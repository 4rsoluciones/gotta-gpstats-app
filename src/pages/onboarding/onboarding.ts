import {Component, ViewChild} from '@angular/core';
import {IonicPage, NavController, Slides} from 'ionic-angular';
import {InAppBrowser} from "@ionic-native/in-app-browser";
import {ApiProvider} from "../../providers/providers";

@IonicPage()
@Component({
  selector: 'page-onboarding',
  templateUrl: 'onboarding.html',
})
export class OnboardingPage {
  @ViewChild(Slides) slides: Slides;

  constructor(private navCtrl: NavController,
              private iab: InAppBrowser,
              private api: ApiProvider) {

  }

  skipClicked() {
    this.slides.slideTo(3);
  }

  loginClicked() {
    this.navCtrl.push('LoginPage');
  }

  signupClicked() {
    this.iab.create(this.api.getCentralizadorConfig().url + 'register', '_system');
  }

}
