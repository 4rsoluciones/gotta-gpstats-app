import {Component} from '@angular/core';
import {IonicPage, LoadingController, NavParams} from 'ionic-angular';
import {NearMePlace} from "../../models/nearMePlace";
import {GoogleMap, GoogleMapOptions, GoogleMaps, GoogleMapsEvent} from "@ionic-native/google-maps";
import {ErrorProvider} from "../../providers/providers";

@IonicPage()
@Component({
  selector: 'page-poi',
  templateUrl: 'poi.html',
})
export class PoiPage {
  map: GoogleMap;
  place: NearMePlace;
  loading: any;

  constructor(private navParams: NavParams,
              private error: ErrorProvider,
              private loadingCtrl: LoadingController) {
    this.place = this.navParams.get('place');
  }

  ionViewDidEnter() {
    this.loading = this.loadingCtrl.create();
    this.loading.present();
    this.loadMap(this.place.lat, this.place.lng).then(() => {
      this.addPlaceToMap(this.place).then(() => {
        setTimeout(() => {
          this.replaceMapWithImage().then(() => {
            this.loading.dismissAll();
          }).catch(error => {
            this.loading.dismissAll();
            this.error.handle({error: error, log: 'error', text: 'Hubo un error al cargar la imagen del mapa.'});
          })
        }, 1000)
      }).catch(error => {
        this.loading.dismissAll();
        this.error.handle({error: error, log: 'error', text: 'Hubo un error al agregar el lugar al mapa.'});
      })
    }).catch(error => {
      this.loading.dismissAll();
      this.error.handle({error: error, log: 'error', text: 'Hubo un error al cargar el mapa.'});
    })
  }

  ionViewWillLeave() {
    this.loading.dismissAll();
  }

  loadMap(lat, lng): Promise<any> {
    return new Promise((resolve, reject) => {
      let mapOptions: GoogleMapOptions = {
        controls: {
          compass: true,
          myLocationButton: false,
          indoorPicker: false,
          mapToolbar: false,
          zoom: false
        },
        gestures: {
          scroll: false,
          tilt: false,
          zoom: false,
          rotate: false
        },
        camera: {
          target: {
            lat: lat + .01,
            lng: lng
          },
          zoom: 12
        }
      };
      this.map = GoogleMaps.create('poi-map', mapOptions);
      this.map.one(GoogleMapsEvent.MAP_READY).then(() => {
        resolve();
      }).catch(error => {
        reject(error);
      });
    })
  }

  addPlaceToMap(place: NearMePlace): Promise<any> {
    return this.map.addMarker({
      position: {lat: place.lat, lng: place.lng},
      disableAutoPan: false,
      draggable: false,
      icon: {
        url: "assets/imgs/places-marker.png",
        size: {
          width: 60,
          height: 70
        }
      }
    })
  }

  replaceMapWithImage(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.map.toDataURL({uncompress: true}).then(response => {
        this.map.remove();
        document.getElementById('map_image').setAttribute('src', response);
        resolve();
      }).catch(error => {
        this.map.remove();
        reject(error);
      })
    })
  }

}
