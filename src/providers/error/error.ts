import {Injectable} from '@angular/core';
import {ToastController} from "ionic-angular";
import {environment} from "../../environment/environment";

@Injectable()
export class ErrorProvider {
  production: boolean;

  constructor(private toastCtrl: ToastController) {
    this.production = environment.production;
  }

  presentToast(msg: string, closeButton = false) {
    this.toastCtrl.create({
      message: msg,
      duration: closeButton ? null : 3000,
      showCloseButton: closeButton,
      closeButtonText: "X"
    }).present().catch(error => {
      if (!this.production) {
        console.error('ERROR:', error);
      }
    })
  }

  handle(error) {
    if (!this.production) {
      switch (error.log) {
        case 'log': {
          console.log('INFO:', error.error || error);
          break;
        }
        case 'error': {
          console.trace('ERROR:', error.error || error);
          break;
        }
      }
    }
    switch (error.error) {
      case 'authentication': {
        //TODO desloguear y mandar a login
        break;
      }
      default: {
        if (error.text) {
          this.presentToast(error.text);
        }
      }
    }
  }

}
