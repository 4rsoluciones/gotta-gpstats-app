import {Injectable} from '@angular/core';
import {ApiProvider} from "../api/api";
import {NearMePlace} from "../../models/nearMePlace";

@Injectable()
export class NearMeProvider {
  config: {
    lat?: number,
    lng?: number,
    radio?: number,
    filters?: any[]
  };
  places: Array<NearMePlace>;
  filtersPromise: Promise<any>;
  gettingFilters: boolean;

  constructor(private api: ApiProvider) {
    this.init();
  }

  init() {
    this.gettingFilters = false;
    this.config = {
      radio: 50000,
    };
  }

  clear() {
    delete this.config;
    delete this.places;
    delete this.filtersPromise;
    delete this.gettingFilters;
    this.init();
  }

  getFilters() {
    if (!this.gettingFilters) {
      this.gettingFilters = true;
      this.filtersPromise = new Promise((resolve, reject) => {
        if (this.config.filters) {
          resolve(Object.assign([], this.config.filters));
        } else {
          this.api.getMapFilters().then(response => {
            this.config.filters = response;
            resolve(Object.assign([], this.config.filters));
          }).catch(error => {
            reject(error);
          })
        }
      })
    }
    return this.filtersPromise;
  }

  setRadius(radio: number) {
    this.config.radio = radio;
  }

  setPosition(lat: number, lng: number) {
    this.config.lat = lat;
    this.config.lng = lng;
  }

  getPosition() {
    return Object.assign({}, {lat: this.config.lat, lng: this.config.lng});
  }

  setFilters(filters: any[]) {
    for (let i = 0; i < filters.length; i++) {
      let id = filters[i].id;
      for (let j = 0; j < this.config.filters.length; j++) {
        if (this.config.filters[j].id == id) {
          this.config.filters[j].selected = filters[i].selected;
          break;
        }
      }
    }
  }

  getPlaces(force?: boolean, limit?: number): Promise<NearMePlace[]> {
    return new Promise((resolve, reject) => {
      let promises = [];
      promises.push(this.getFilters());
      if (force) {
        delete this.places;
      }
      if (this.places && (this.places.length > 0)) {
        promises.push(Promise.resolve(this.places));
      } else {
        promises.push(this.api.getNearMe({
          lat: this.config.lat.toString(),
          lng: this.config.lng.toString(),
          rad: this.config.radio.toString(),
          categories: this.getSelectedFiltersIds()
        }).then((places: NearMePlace[]) => {
          this.places = places;
        }));
      }
      Promise.all(promises).then(() => {
        if (limit) {
          resolve(this.places.slice(0, limit));
        } else {
          resolve(this.places);
        }
      }).catch(error => {
        reject(error);
      });
    })
  }

  getSelectedFiltersIds(): string {
    let catIds = '';
    if (this.config.filters) {
      for (let i = 0; i < this.config.filters.length; i++) {
        if (this.config.filters[i].selected) {
          if (catIds.length > 0) {
            catIds += ',';
          }
          catIds += this.config.filters[i].id;
        }
      }
      return catIds;
    } else {
      return null;
    }
  }

}
