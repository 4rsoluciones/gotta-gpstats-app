import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {NearMePage} from './near-me';
import {ComponentsModule} from "../../components/components.module";

@NgModule({
  declarations: [
    NearMePage,
  ],
  imports: [
    IonicPageModule.forChild(NearMePage),
    ComponentsModule
  ],
})
export class NearMePageModule {
}
