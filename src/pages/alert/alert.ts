import {Component} from '@angular/core';
import {IonicPage, NavParams, ViewController} from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-alert',
  templateUrl: 'alert.html',
})
export class AlertPage {
  image: string;
  title: string;
  subtitle: string;
  okButton: string;
  cancelButton: string;

  constructor(private navParams: NavParams,
              private viewCtrl: ViewController) {
    this.image = this.navParams.get('image');
    this.title = this.navParams.get('title');
    this.subtitle = this.navParams.get('subtitle');
    this.okButton = this.navParams.get('okButton');
    this.cancelButton = this.navParams.get('cancelButton');
  }

  okButtonClicked() {
    this.viewCtrl.dismiss(true);
  }

  cancelButtonClicked() {
    this.viewCtrl.dismiss(false);
  }

}
