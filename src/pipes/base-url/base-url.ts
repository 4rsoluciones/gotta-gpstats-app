import {Pipe, PipeTransform} from '@angular/core';
import {environment} from "../../environment/environment";

@Pipe({
  name: 'baseUrl',
})
export class BaseUrlPipe implements PipeTransform {

  transform(value: string, backend: string) {
    if (environment.production) {
      return environment.apiConfig[backend].prod.url + 'storage/' + value;
    } else {
      return environment.apiConfig[backend].dev.url + 'storage/' + value;
    }
  }
}
