import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RoundCardComponent} from './round-card/round-card';
import {CommonModule} from "@angular/common";
import {IonicModule} from "ionic-angular";
import {NearMeCardComponent} from './near-me-card/near-me-card';
import {PipesModule} from "../pipes/pipes.module";

@NgModule({
  declarations: [
    RoundCardComponent,
    NearMeCardComponent
  ],
  imports: [CommonModule, IonicModule, PipesModule],
  exports: [
    RoundCardComponent,
    NearMeCardComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ComponentsModule {
}
