import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {LoadInitialStatePage} from './load-initial-state';

@NgModule({
  declarations: [
    LoadInitialStatePage,
  ],
  imports: [
    IonicPageModule.forChild(LoadInitialStatePage),
  ],
})
export class LoadInitialStateModule {
}
