import {Injectable} from '@angular/core';
import {ApiProvider} from "../api/api";

@Injectable()
export class SettingsProvider {
  settings: any;

  constructor(private api: ApiProvider) {

  }

  clear() {
    delete this.settings;
  }

  persist() {
    window.localStorage['profile'] = JSON.stringify(this.settings);
  }

  getSaved() {
    let settings = window.localStorage['profile'];
    if (settings) {
      this.settings = JSON.parse(settings);
    }
    return this.settings;
  }

  get(force?: boolean): Promise<any> {
    return new Promise((resolve, reject) => {
      this.getSaved();
      if (force) {
        delete this.settings;
      }
      if (this.settings) {
        resolve(Object.assign({}, this.settings));
      } else {
        this.api.getSettings().then(settings => {
          this.settings = settings;
          if (this.settings) {
            this.persist();
            resolve(Object.assign({}, this.settings));
          } else {
            let settings = this.getSaved();
            if (settings) {
              resolve(settings);
            } else {
              reject(null);
            }
          }
        }).catch(error => {
          let settings = this.getSaved();
          if (settings) {
            resolve(settings);
          } else {
            reject(error);
          }
        })
      }
    })
  }

  set(settings): Promise<any> {
    return new Promise((resolve, reject) => {
      this.api.setSettings(settings).then(settings => {
        this.settings = settings;
        this.persist();
        resolve(Object.assign({}, this.settings));
      }).catch(error => {
        let settings = this.getSaved();
        if (settings) {
          resolve(settings);
        } else {
          reject(error);
        }
      })
    })
  }

}
