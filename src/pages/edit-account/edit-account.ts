import {Component} from '@angular/core';
import {IonicPage, LoadingController, NavController} from 'ionic-angular';
import {ErrorProvider, SelectOptionsProvider, UserProvider} from "../../providers/providers";
import {FormBuilder, FormGroup} from "@angular/forms";

@IonicPage()
@Component({
  selector: 'page-edit-account',
  templateUrl: 'edit-account.html',
})
export class EditAccountPage {
  countries: any[];
  states: any[];
  genders: any[];
  loading: boolean;
  accountForm: FormGroup;

  constructor(private fb: FormBuilder,
              private loadingCtrl: LoadingController,
              private navCtrl: NavController,
              private error: ErrorProvider,
              private selectOptions: SelectOptionsProvider,
              private user: UserProvider) {
    this.accountForm = this.fb.group({
      username: [],
      email: [{value: null, disabled: true}],
      country: [],
      state_id: [],
      city: [],
      first_name: [],
      last_name: [],
      company_name: [],
      birth_date: [],
      gender: [],
      mobile_phonenumber: []
    });
    this.genders = [
      {
        id: 'male',
        name: 'Masculino'
      },
      {
        id: 'female',
        name: 'Femenino'
      }
    ]
  }

  ionViewCanEnter() {
    let promises = [];
    promises.push(this.selectOptions.getAccountCountries().then(response => {
      this.countries = response;
    }));
    return Promise.all(promises);
  }

  ionViewWillEnter() {
    this.update();
  }

  update() {
    this.loading = true;
    this.user.getAccountInfo().then(accountInfo => {
      this.accountForm.patchValue(accountInfo);
      this.countrySelected();
      this.loading = false;
    }).catch(error => {
      this.loading = false;
      this.error.handle({error: error, log: 'error', text: 'Ha ocurrido un error. Intente nuevamente más tarde.'});
    });
  }

  countrySelected() {
    this.selectOptions.getStates(this.accountForm.value.country).then(states => {
      this.states = states;
    })
  }

  saveClicked() {
    let loading = this.loadingCtrl.create();
    loading.present();
    this.user.editAccountInfo(this.accountForm.value).then(() => {
      loading.dismissAll();
      this.navCtrl.pop();
    }).catch(error => {
      loading.dismissAll();
      this.error.handle({error: error, log: 'error', text: 'Ha ocurrido un error. Intente nuevamente más tarde.'});
    })
  }

}
