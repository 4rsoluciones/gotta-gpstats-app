import {BaseEntity, Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {Play} from "./play";

@Entity('food-questionnaire')
export class FoodQuestionnaire extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  drankWater: boolean;

  @Column()
  drankSportsDrink: boolean;

  @Column()
  swallowedCerealOrNuts: boolean;

  @Column()
  swallowedSandwich: boolean;

  @Column()
  swallowedFruit: boolean;

  @OneToOne(() => Play, play => play.foodQuestionnaire, {
    onDelete: "CASCADE"
  })
  @JoinColumn()
  play: Play;

}
