import {Component} from '@angular/core';
import {
  ActionSheetController, IonicPage, LoadingController, ModalController, NavController, NavParams,
  ToastController
} from 'ionic-angular';
import {
  ErrorProvider,
  RoundProvider,
  RoundsProvider,
  SelectOptionsProvider,
  SettingsProvider
} from "../../providers/providers";
import {Field} from "../../models/field";
import {Round} from "../../models/round";

@IonicPage()
@Component({
  selector: 'page-new-round',
  templateUrl: 'new-round.html',
})
export class NewRoundPage {
  movements: any[];
  types: any[];
  parOptions: any[];
  tours: any[];
  conditions: any[];
  gameTypes: any[];
  tournamentTypes: any[];
  roundNumbers: any[];
  playedHoles: any[];
  course: any;
  field: Field;
  newRound: any;
  formStatus: string;
  edit: boolean;
  selectOpts = {
    cssClass: 'secondary-select'
  };
  pickerOpts = {
    cssClass: 'secondary-picker'
  };
  settings: any;

  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private loadingCtrl: LoadingController,
              private selectOptions: SelectOptionsProvider,
              private toastCtrl: ToastController,
              private settingsProvider: SettingsProvider,
              private roundProvider: RoundProvider,
              private roundsProvider: RoundsProvider,
              private error: ErrorProvider,
              private actionSheetCtrl: ActionSheetController,
              private modalCtrl: ModalController) {
    this.tournamentTypes = [
      {
        value: '18',
        title: '18 Hoyos',
        subtitle: '1 Vuelta'
      }, {
        value: '36',
        title: '36 Hoyos',
        subtitle: '2 Vueltas'
      }, {
        value: '54',
        title: '54 Hoyos',
        subtitle: '3 Vueltas'
      }, {
        value: '72',
        title: '72 Hoyos',
        subtitle: '4 Vueltas'
      }
    ];
    this.roundNumbers = [
      {
        value: 1,
        title: '1'
      }, {
        value: 2,
        title: '2'
      }, {
        value: 3,
        title: '3'
      }, {
        value: 4,
        title: '4'
      }
    ];
    this.playedHoles = [
      {
        value: 9,
        title: '9'
      }, {
        value: 18,
        title: '18'
      }
    ];
    this.edit = this.navParams.get('edit');
    this.newRound = {
      date: new Date().toISOString().split('T')[0]
    };
  }

  ionViewCanEnter(): Promise<any> {
    return new Promise((resolve, reject) => {
      let loading = this.loadingCtrl.create();
      let loadingTimeout = setTimeout(() => {
        loading.present();
      }, 200);
      this.settingsProvider.get().then(settings => {
        this.settings = settings;
        clearTimeout(loadingTimeout);
        loading.dismissAll();
        resolve();
      }).catch(error => {
        clearTimeout(loadingTimeout);
        loading.dismissAll();
        this.error.handle({error: error, text: 'Debe completar sus datos de perfil antes de crear una nueva vuelta.', log: 'error'});
        reject();
      })
    })
  }

  ionViewDidLoad() {
    let promises = [];
    promises.push(this.selectOptions.getMovements().then(response => {
      this.movements = response;
    }));
    promises.push(this.selectOptions.getFieldTypes().then(response => {
      this.types = response;
    }));
    promises.push(this.selectOptions.getParOptions().then(response => {
      this.parOptions = response;
    }));
    promises.push(this.selectOptions.getGameConditions().then(response => {
      this.conditions = response;
    }));
    promises.push(this.selectOptions.getGameTypes().then(response => {
      this.gameTypes = response;
    }));
    promises.push(this.selectOptions.getTours().then(response => {
      this.tours = response;
    }));
    if (this.edit) {
      this.newRound = {};
      Promise.all(promises).then(() => {
        let roundInfo = this.roundProvider.getRoundInfo();
        this.newRound = {
          date: roundInfo.performed_at,
          tour: roundInfo.tour,
          conditions: this.getGameConditionByValue(roundInfo.game_condition),
          type: this.getTypeByValue(roundInfo.round_type),
          player_condition: roundInfo.player_condition,
          roundNumber: this.getRoundNumberByValue(roundInfo.round_number),
          handicap: roundInfo.handicap,
          tournamentType: this.getTournamentTypeByValue(roundInfo.tournament_type),
          playedHoles: this.getPlayedHolesByValue(roundInfo.played_holes)
        };
        this.field = roundInfo.field;
      })
    } else {
      this.newRound = {
        date: new Date().toISOString().split('T')[0],
        tour: this.settings.tour
      };
    }
  }

  inputChanged() {
    this.formStatus = this.getFormStatus();
  }

  getFormStatus(): string {
    if (!this.newRound.date) {
      return 'invalid-date';
    }
    if (!this.field) {
      return 'invalid-field';
    }
    if ((this.settings.player_condition == 'player.professional') && !this.newRound.tour) {
      return 'invalid-tour';
    }
    if (this.newRound.conditions == undefined) {
      return 'invalid-conditions';
    }
    if (this.newRound.type == undefined) {
      return 'invalid-type';
    }
    if ((this.newRound.type != undefined) && (this.gameTypes[this.newRound.type].value == 'round.type.tournament')) {
      if (this.newRound.tournamentType == undefined) {
        return 'invalid-tournamentType';
      }
      if (this.newRound.roundNumber == undefined) {
        return 'invalid-roundNumber';
      }
      if (this.roundNumbers[this.newRound.roundNumber].value > (this.tournamentTypes[this.newRound.tournamentType].value / 18)) {
        this.toastCtrl.create({
          message: 'El número de vuelta no puede ser mayor a la cantidad de vueltas del torneo',
          duration: 3000
        }).present();
        return 'invalid-roundNumber';
      }
    }
    if (this.newRound.playedHoles == undefined) {
      return 'invalid-playedHoles';
    }
    return 'valid';
  }

  getGameConditionByValue(value: any) {
    for (let i = 0; i < this.conditions.length; i++) {
      if (this.conditions[i].value == value) {
        return i;
      }
    }
    return -1;
  }

  getTypeByValue(value: any) {
    for (let i = 0; i < this.gameTypes.length; i++) {
      if (this.gameTypes[i].value == value) {
        return i;
      }
    }
    return -1;
  }

  getRoundNumberByValue(value: any) {
    for (let i = 0; i < this.roundNumbers.length; i++) {
      if (this.roundNumbers[i].value == value) {
        return i;
      }
    }
    return -1;
  }

  getTournamentTypeByValue(value: any) {
    for (let i = 0; i < this.tournamentTypes.length; i++) {
      if (this.tournamentTypes[i].value == value) {
        return i;
      }
    }
    return -1;
  }

  getPlayedHolesByValue(value: any) {
    for (let i = 0; i < this.playedHoles.length; i++) {
      if (this.playedHoles[i].value == value) {
        return i;
      }
    }
    return -1;
  }

  courseSelected = () => {
    return new Promise((resolve, reject) => {
      this.field = this.roundProvider.getCourse();
      this.inputChanged();
      resolve();
    });
  };

  selectCourseClicked() {
    this.navCtrl.push('CoursesPage', {
      callback: this.courseSelected
    }).catch(error => {
      this.error.handle({error: error, log: 'error', text: 'Hubo un error al ir a la vista para buscar canchas.'});
    });
  }

  createClicked() {
    if (this.getFormStatus() == 'valid') {
      let newRound = new Round();
      newRound.field = this.field;
      newRound.game_condition = this.conditions[this.newRound.conditions].value;
      if (this.settings.player_condition == 'player.amateur') {
        newRound.handicap = this.settings.handicap;
      }
      newRound.performed_at = this.newRound.date;
      newRound.player_condition = this.settings.player_condition;
      newRound.round_number = this.gameTypes[this.newRound.type].value == 'round.type.tournament' ? this.roundNumbers[this.newRound.roundNumber].value : 1;
      newRound.round_type = this.gameTypes[this.newRound.type].value;
      if (this.settings.player_condition == 'player.professional') {
        newRound.tour = this.newRound.tour;
      }
      newRound.tournament_type = this.gameTypes[this.newRound.type].value == 'round.type.tournament' ? this.tournamentTypes[this.newRound.tournamentType].value : null;
      newRound.played_holes = this.playedHoles[this.newRound.playedHoles].value;
      let alertModal = this.modalCtrl.create('AlertPage', {
        image: 'assets/imgs/iconos/vuelta-creada.png',
        title: '¡Vuelta creada!',
        subtitle: 'Guardamos los datos generales de tu vuelta. Completá ahora tu estado inicial.',
        okButton: 'CONTINUAR AHORA',
        cancelButton: 'Continuar más adelante.'
      });
      this.roundsProvider.newRound(newRound).then((round: Round) => {
        alertModal.onDidDismiss(data => {
          if (data) {
            this.roundProvider.setActiveRound(round).then(() => {
              this.loadInitialState().then(() => {
                this.goRoundPage();
              }).catch(error => {
                this.error.handle({
                  error: error,
                  log: 'error',
                  text: 'Hubo un error al intentar cargar el estado inicial.'
                });
                this.goRoundPage();
              })
            }).catch(error => {
              this.error.handle({error: error, log: 'error', text: 'Hubo un error al setear los datos de la vuelta.'});
            })
          } else {
            this.navCtrl.popToRoot();
          }
        });
        alertModal.present();
      });
    }
  }

  goRoundPage() {
    this.navCtrl.push('RoundPage').then(() => {
      this.navCtrl.remove(this.navCtrl.length() - 2).catch(error => {
        this.error.handle({error: error, log: 'error'});
      })
    }).catch(error => {
      this.error.handle({error: error, log: 'error', text: 'Hubo un error al ir a la vista de la vuelta.'});
    })
  }

  fieldMoreClicked() {
    let actionSheet = this.actionSheetCtrl.create({
      cssClass: 'round-action-sheet',
      buttons: [
        {
          text: 'Cambiar cancha',
          icon: 'md-create',
          handler: () => {
            this.selectCourseClicked();
          }
        }
      ]
    });
    actionSheet.present();
  }

  playedHolesClicked() {
    if (this.edit) {
      this.toastCtrl.create({
        message: 'No se pueden editar los hoyos jugados. Para cambiar esto debe crear una nueva vuelta.',
        duration: 3000
      }).present();
    }
  }

  editClicked() {
    if (this.getFormStatus() == 'valid') {
      let newRound = new Round();
      newRound.field = this.field;
      newRound.game_condition = this.conditions[this.newRound.conditions].value;
      newRound.performed_at = this.newRound.date;
      newRound.round_number = this.gameTypes[this.newRound.type].value == 'round.type.tournament' ? this.roundNumbers[this.newRound.roundNumber].value : 1;
      newRound.round_type = this.gameTypes[this.newRound.type].value;
      newRound.tournament_type = this.gameTypes[this.newRound.type].value == 'round.type.tournament' ? this.tournamentTypes[this.newRound.tournamentType].value : null;
      this.roundProvider.editRoundInfo(newRound);
      this.toastCtrl.create({
        message: 'Se han editado los datos de la vuelta',
        duration: 3000
      }).present();
      this.navCtrl.pop();
    }
  }

  loadInitialState() {
    return new Promise((resolve, reject) => {
      let initialStateModal = this.modalCtrl.create('LoadInitialStatePage');
      initialStateModal.onDidDismiss(() => {
        resolve();
      });
      initialStateModal.present().catch(error => {
        reject(error);
      })
    })
  }

  tourClicked() {
    this.toastCtrl.create({
      duration: 3000,
      message: 'El tour se debe modificar desde la configuración del perfil'
    }).present();
  }

}
