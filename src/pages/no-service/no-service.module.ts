import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {NoServicePage} from './no-service';

@NgModule({
  declarations: [
    NoServicePage,
  ],
  imports: [
    IonicPageModule.forChild(NoServicePage),
  ],
})
export class NoServicePageModule {
}
