import {Component, ViewChild} from '@angular/core';
import {
  ActionSheetController,
  AlertController,
  Events,
  IonicPage,
  LoadingController,
  NavController,
  Slides,
  ToastController
} from 'ionic-angular';
import {
  BenefitsProvider,
  ErrorProvider,
  NearMeProvider,
  RoundProvider,
  RoundsProvider
} from "../../providers/providers";
import {NearMePlace} from "../../models/nearMePlace";
import {Benefit} from "../../models/benefit";
import {Diagnostic} from "@ionic-native/diagnostic";
import {Geolocation} from "@ionic-native/geolocation";
import {Round} from "../../models/round";

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  @ViewChild('nearMeSlides') nearMeSlides: Slides;

  loadingRounds: boolean;
  loadingRoundsPromise: Promise<any>;
  loadingNearMe: boolean;
  loadingBenefits: boolean;
  rounds: any[];
  nearMePlaces: any[];
  benefits: any[];

  constructor(private navCtrl: NavController,
              private events: Events,
              private roundsProvider: RoundsProvider,
              private roundProvider: RoundProvider,
              private nearMeProvider: NearMeProvider,
              private toastCtrl: ToastController,
              private diagnostic: Diagnostic,
              private geolocation: Geolocation,
              private benefitsProvider: BenefitsProvider,
              private alertCtrl: AlertController,
              private actionSheetCtrl: ActionSheetController,
              private loadingCtrl: LoadingController,
              private error: ErrorProvider) {
    this.rounds = [];
    this.nearMePlaces = [];
    this.benefits = [];
    this.events.subscribe('complete-round-synced', () => {
      this.updateRounds({force: true});
    })
  }

  ionViewDidLoad() {
    this.updateNearMe();
    this.updateBenefits();
  }

  ionViewDidEnter() {
    this.updateRounds();
  }

  doRefresh(refresher) {
    let promises = [];
    promises.push(this.updateRounds({force: true}));
    promises.push(this.updateNearMe());
    promises.push(this.updateBenefits());
    Promise.all(promises).then(() => {
      refresher.complete();
    }).catch(() => {
      refresher.complete();
    })
  }

  updateRounds(params?: { force?: boolean }): Promise<any> {
    if (!this.loadingRounds) {
      this.loadingRounds = true;
      let promises = [];
      this.rounds = [];
      promises.push(this.roundsProvider.getIncompleteRounds({limit: 4}).then(response => {
        this.rounds = response.concat(this.rounds);
      }));
      promises.push(this.roundsProvider.getRounds({force: params ? params.force : null, limit: 4}).then(response => {
        this.rounds = this.rounds.concat(response);
      }));
      this.loadingRoundsPromise = Promise.all(promises);
      this.loadingRoundsPromise.then(() => {
        this.rounds.sort((a: Round, b: Round) => {
          return (new Date(b.performed_at).getTime() - new Date(a.performed_at).getTime());
        });
        this.rounds = this.rounds.slice(0, 4);
        this.loadingRounds = false;
      }).catch(error => {
        this.error.handle(error);
        this.loadingRounds = false;
      })
    }
    return this.loadingRoundsPromise;
  }

  updateLocation(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.getPermissions().then(() => {
        this.getLocation().then(() => {
          resolve();
        }).catch(error => {
          this.error.handle({
            error: error,
            log: 'error',
            text: 'No se pudo obtener su ubicación. Intente nuevamente más tarde'
          });
          reject(error);
        })
      }).catch(error => {
        this.error.handle({
          error: error,
          log: 'error',
          text: 'No se pudo obtener su ubicación. Intente nuevamente más tarde'
        });
        reject(error);
      })
    })
  }

  getPermissions(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.diagnostic.isLocationEnabled().then(response => {
        if (response) {
          this.diagnostic.isLocationAvailable().then(response => {
            if (response) {
              resolve();
            } else {
              this.diagnostic.requestLocationAuthorization().then(() => {
                resolve();
              }).catch(error => {
                reject(error);
              });
            }
          }).catch(error => {
            reject(error);
          })
        } else {
          this.toast('Por favor habilite la ubicación e intente nuevamente.');
        }
      }).catch(error => {
        reject(error);
      })
    })
  }

  getLocation(): Promise<any> {
    return new Promise((resolve, reject) => {
      let locationSubscription = this.geolocation.watchPosition()
        .filter((p) => p.coords !== undefined)
        .subscribe(position => {
          this.nearMeProvider.setPosition(position.coords.latitude, position.coords.longitude);
          locationSubscription.unsubscribe();
          resolve();
        }, error => {
          locationSubscription.unsubscribe();
          reject(error);
        })
    })
  }

  updateNearMe(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.loadingNearMe = true;
      this.updateLocation().then(() => {
        this.nearMeProvider.getPlaces(false, 4).then(response => {
          this.nearMePlaces = response;
          this.loadingNearMe = false;
          resolve();
        }).catch(error => {
          this.error.handle(error);
          this.loadingNearMe = false;
          reject(error);
        })
      }).catch(error => {
        this.loadingNearMe = false;
        reject(error);
      })
    })
  }

  updateBenefits(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.loadingBenefits = true;
      this.benefitsProvider.getBenefits(false, 4).then(response => {
        this.benefits = response;
        this.loadingBenefits = false;
        resolve();
      }).catch(error => {
        this.error.handle(error);
        this.loadingBenefits = false;
        reject(error);
      })
    })
  }

  myRoundsClicked() {
    this.navCtrl.parent.select(1);
  }

  roundClicked(round) {
    let loading = this.loadingCtrl.create();
    let timeout = setTimeout(() => {
      loading.present();
    }, 500);
    this.roundProvider.setActiveRound(round).then(() => {
      this.navCtrl.push('RoundPage').then(() => {
        clearTimeout(timeout);
        loading.dismissAll();
      }).catch(error => {
        clearTimeout(timeout);
        loading.dismissAll();
        this.error.handle({error: error, text: 'Ha ocurrido un error. Intente nuevamente más tarde.', log: 'error'})
      })
    }).catch(error => {
      clearTimeout(timeout);
      loading.dismissAll();
      this.error.handle(error);
    })
  }

  roundCardMoreClicked(round) {
    let actionSheet = this.actionSheetCtrl.create({
      cssClass: 'round-action-sheet',
      buttons: [
        {
          text: (!round.is_completed_locally ? 'Continuar carga' : 'Ver vuelta'),
          icon: (!round.is_completed_locally ? 'md-create' : 'ios-information-circle'),
          handler: () => {
            this.roundClicked(round);
          }
        }, {
          text: 'Descartar',
          icon: 'ios-trash',
          handler: () => {
            let alert = this.alertCtrl.create({
              title: 'Confirmar',
              message: '¿Está seguro que desea eliminar esta vuelta?',
              buttons: [
                {
                  text: 'Cancelar',
                  role: 'cancel'
                },
                {
                  text: 'Eliminar',
                  handler: () => {
                    this.deleteRound(round);
                  }
                }
              ]
            });
            alert.present();
          }
        }
      ]
    });
    actionSheet.present();
  }

  deleteRound(round) {
    let loading = this.loadingCtrl.create();
    let timeout = setTimeout(() => {
      loading.present();
    }, 500);
    this.roundsProvider.deleteRound(round).then(() => {
      clearTimeout(timeout);
      loading.dismissAll();
      this.updateRounds();
    }).catch(error => {
      clearTimeout(timeout);
      loading.dismissAll();
      this.error.handle({error: error, log: 'error', text: 'Hubo un error al eliminar la vuelta. Intente nuevamente.'});
      this.updateRounds();
    })
  }

  newRoundClicked() {
    this.navCtrl.push('NewRoundPage').catch(error => {
      this.error.handle({error: error, text: 'Ha ocurrido un error. Intente nuevamente más tarde.', log: 'error'})
    })
  }

  mapClicked() {
    this.navCtrl.parent.select(2);
  }

  placeClicked(place: NearMePlace) {
    this.navCtrl.push('PoiPage', {place: place});
  }

  benefitsClicked() {
    this.navCtrl.parent.select(3);
  }

  benefitClicked(benefit: Benefit) {
    this.navCtrl.push('BenefitPage', {benefit: benefit});
  }

  toast(msg: string) {
    this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    }).present();
  }

}
