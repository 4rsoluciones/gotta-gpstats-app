import {Component} from '@angular/core';
import {IonicPage, ModalController} from 'ionic-angular';
import {ErrorProvider, SelectOptionsProvider, SettingsProvider} from "../../providers/providers";

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  countries: any[];
  playerTypes: any[];
  tours: any[];
  settings: any;
  loading: boolean;
  loaded: boolean;

  constructor(private modalCtrl: ModalController,
              private error: ErrorProvider,
              private selectOptions: SelectOptionsProvider,
              private settingsProvider: SettingsProvider) {
    this.loaded = false;
  }

  ionViewCanEnter() {
    let promises = [];
    promises.push(this.selectOptions.getCountries().then(response => {
      this.countries = response;
    }));
    promises.push(this.selectOptions.getPlayerTypes().then(response => {
      this.playerTypes = response;
    }));
    promises.push(this.selectOptions.getTours().then(response => {
      this.tours = response;
    }));
    return Promise.all(promises);
  }

  ionViewWillEnter() {
    this.update();
  }

  update() {
    this.loading = true;
    this.settingsProvider.get(!this.loaded).then(settings => {
      this.settings = settings;
      this.loaded = true;
      this.loading = false;
    }).catch(error => {
      this.loading = false;
      this.error.handle({error: error, log: 'error', text: 'Ha ocurrido un error. Intente nuevamente más tarde.'});
    });
  }

  getCountryByValue(value: any) {
    for (let i = 0; i < this.countries.length; i++) {
      if (this.countries[i].code == value) {
        return i;
      }
    }
    return -1;
  }

  getPlayerTypeByValue(value: any) {
    for (let i = 0; i < this.playerTypes.length; i++) {
      if (this.playerTypes[i].value == value) {
        return i;
      }
    }
    return -1;
  }

  getTourByValue(value: any) {
    for (let i = 0; i < this.tours.length; i++) {
      if (this.tours[i].id == value) {
        return i;
      }
    }
    return -1;
  }

  editProfileClicked() {
    let editProfileModal = this.modalCtrl.create('EditProfilePage');
    editProfileModal.onWillDismiss(() => {
      this.update();
    });
    editProfileModal.present();
  }

}
