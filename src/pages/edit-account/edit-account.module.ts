import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {EditAccountPage} from './edit-account';
import {HorizontalSpinnerModule} from "@4r/horizontal-spinner/horizontal-spinner.module";

@NgModule({
  declarations: [
    EditAccountPage,
  ],
  imports: [
    IonicPageModule.forChild(EditAccountPage),
    HorizontalSpinnerModule
  ],
})
export class EditAccountPageModule {
}
