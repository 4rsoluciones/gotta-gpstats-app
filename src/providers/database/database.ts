import {Injectable} from '@angular/core';
import {Connection, createConnection} from "typeorm";
import {Field} from "../../models/field";
import {Round} from "../../models/round";
import {InitialState} from "../../models/initialState";
import {FinalState} from "../../models/finalState";
import {Hole} from "../../models/hole";
import {Play} from "../../models/play";
import {ConnectionOptions} from "typeorm/connection/ConnectionOptions";
import {Platform} from "ionic-angular";
import {environment} from "../../environment/environment";
import {EmotionalQuestionnaire} from "../../models/emotionalQuestionnaire";
import {FoodQuestionnaire} from "../../models/foodQuestionnaire";
import {PhysicalQuestionnaire} from "../../models/physycalQuestionnaire";
import {HoleToDelete} from "../../models/holeToDelete";

@Injectable()
export class DatabaseProvider {
  production: boolean;
  db: any;
  tables = {
    names: [
      'field',
      'final-state',
      'initial-state',
      'hole',
      'hole-to-delete',
      'play',
      'round',
      'emotional-questionnaire',
      'food-questionnaire',
      'physical-questionnaire'
    ],
    entities: [
      Field,
      FinalState,
      InitialState,
      Hole,
      HoleToDelete,
      Play,
      Round,
      EmotionalQuestionnaire,
      FoodQuestionnaire,
      PhysicalQuestionnaire
    ]
  };
  log;

  constructor(private platform: Platform) {
    this.production = environment.production;
    if (this.production) {
      this.log = [];
    } else {
      this.log = ['error', 'query', 'schema'];
    }
  }

  init(): Promise<any> {
    return new Promise((resolve, reject) => {
      let dbOptions: ConnectionOptions;
      if (this.platform.is('cordova')) {
        dbOptions = {
          type: 'cordova',
          database: this.production ? 'gpstats-rounds-v1' : 'test',
          location: 'default',
          logging: this.log,
          entities: this.tables.entities
        };
      } else {
        dbOptions = {
          type: 'sqljs',
          location: 'default',
          logging: this.log,
          synchronize: true,
          entities: this.tables.entities
        };
      }
      createConnection(dbOptions).then((connection: Connection) => {
        this.db = connection;
        this.createIfNotExists().then(() => {
          resolve();
        }).catch(error => {
          reject(error);
        })
      }).catch(error => {
        reject(error);
      })
    })
  }

  createIfNotExists(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.dbCreated().then(() => {
        resolve();
      }).catch(() => {
        this.db.synchronize(true).then(() => {
          resolve();
        }).catch(error => {
          reject(error);
        })
      })
    })
  }

  dbCreated(): Promise<any> {
    let promises = [];
    for (let i = 0; i < this.tables.names.length; i++) {
      promises.push(this.db.getRepository(this.tables.names[i]).count());
    }
    return Promise.all(promises);
  }

  reset() {
    return new Promise((resolve, reject) => {
      this.db.dropDatabase().then(() => {
        this.createIfNotExists().then(() => {
          resolve();
        }).catch(error => {
          reject(error);
        })
      }).catch(error => {
        reject(error);
      })
    })
  }

  saveField(field: Field): Promise<any> {
    return this.db.getRepository('field').save(field);
  }

  getDefaultFields(): Promise<Field[]> {
    return this.db.getRepository('field').find({where: {synchronized: 0}});
  }

  removeField(field: Field): Promise<Field> {
    return new Promise((resolve, reject) => {
      if (field && field.id) {
        this.db.getRepository('field').deleteById(field.id).then(() => {
          resolve(field);
        }).catch(error => {
          reject(error);
        })
      } else {
        resolve(field);
      }
    })
  }

  saveRound(round: Round): Promise<Round> {
    return this.db.getRepository('round').save(round);
  }

  getRounds(params: { limit?: number }): Promise<Round[]> {
    if (params.limit) {
      return this.db.getRepository('round')
        .createQueryBuilder('round')
        .innerJoinAndSelect('round.field', 'field')
        .where('(round.modified = 0 OR round.modified = 1 OR round.modified = 2)')
        .take(params.limit)
        .getMany()
    } else {
      return this.db.getRepository('round')
        .createQueryBuilder('round')
        .innerJoinAndSelect('round.field', 'field')
        .where('(round.modified = 0 OR round.modified = 1 OR round.modified = 2)')
        .getMany()
    }
  }

  getNewRounds(): Promise<Round[]> {
    return this.db.getRepository('round')
      .createQueryBuilder('round')
      .innerJoinAndSelect('round.field', 'field')
      .where('round.synchronized = 0 OR round.synchronized IS null')
      .andWhere('(round.modified = 0 OR round.modified = 1 OR round.modified = 2)')
      .getMany()
  }

  setRoundSynchronized(round: Round): Promise<any> {
    return this.db.createQueryBuilder()
      .update('round')
      .set({
        synchronized: round.synchronized,
        modified: (round.modified ? round.modified : 0),
        is_completed: round.is_completed
      })
      .where('id = :id', {id: round.id})
      .execute();
  }

  setRoundCompleted(round: Round): Promise<any> {
    return this.db.createQueryBuilder()
      .update('round')
      .set({is_completed: round.is_completed})
      .where('id = :id', {id: round.id})
      .execute();
  }

  setRoundCompletedLocally(round: Round): Promise<any> {
    return this.db.createQueryBuilder()
      .update('round')
      .set({is_completed_locally: true})
      .where('id = :id', {id: round.id})
      .execute();
  }

  getRoundsWithDefaultHolesToUpdate(): Promise<Round[]> {
    return this.db.getRepository('round')
      .createQueryBuilder('round')
      .innerJoinAndSelect('round.field', 'field')
      .where('round.modified = 2')
      .getMany()
  }

  setRoundInfoToEdit(round: Round): Promise<any> {
    delete round.pre_competitive_questionnaire;
    delete round.post_competitive_questionnaire;
    delete round.holes;
    round.modified = 1;
    return this.db.createQueryBuilder()
      .update('round')
      .set(round)
      .where('id = :id', {id: round.id})
      .execute();
  }

  getRoundsInfoToUpdate(): Promise<Round[]> {
    return this.db.getRepository('round').find({relations: ['field'], where: {modified: 1}});
  }

  setRoundToDelete(round: Round): Promise<any> {
    return this.db.createQueryBuilder()
      .update('round')
      .set({modified: -1})
      .where('id = :id', {id: round.id})
      .execute();
  }

  getRoundsToDelete(): Promise<Round[]> {
    return this.db.getRepository('round').find({where: {modified: -1}});
  }

  removeRound(round: Round): Promise<Round> {
    return new Promise((resolve, reject) => {
      this.db.getRepository('round').deleteById(round.id).then(() => {
        resolve(round);
      }).catch(error => {
        reject(error);
      })
    })
  }

  savePreCompetitiveQuestionnaire(state: InitialState): Promise<any> {
    state.modified = 1;
    return new Promise((resolve, reject) => {
      this.db.getRepository('round').findOneById(state.round.id, {relations: ['pre_competitive_questionnaire']}).then((round: Round) => {
        if (round.hasOwnProperty('pre_competitive_questionnaire')) {
          this.db.createQueryBuilder()
            .update('initial-state')
            .set(state)
            .where('id = :id', {id: round.pre_competitive_questionnaire.id})
            .execute()
            .then(() => {
              resolve();
            })
            .catch(error => {
              reject(error);
            })
        } else {
          this.db.getRepository('initial-state').save(state).then(() => {
            resolve();
          }).catch(error => {
            reject(error);
          })
        }
      })
    })
  }

  getPreCompetitiveQuestionnairesToUpdate(): Promise<InitialState[]> {
    return this.db.getRepository('initial-state').find({relations: ['round'], where: {modified: 1}});
  }

  setPreCompetitiveQuestionnaireSynchronized(state: InitialState): Promise<any> {
    return this.db.createQueryBuilder()
      .update('initial-state')
      .set({modified: 0})
      .where('id = :id', {id: state.id})
      .execute();
  }

  savePostCompetitiveQuestionnaire(state: FinalState): Promise<any> {
    state.modified = 1;
    return new Promise((resolve, reject) => {
      this.db.getRepository('round').findOneById(state.round.id, {relations: ['post_competitive_questionnaire']}).then((round: Round) => {
        if (round.hasOwnProperty('post_competitive_questionnaire')) {
          this.db.createQueryBuilder()
            .update('final-state')
            .set(state)
            .where('id = :id', {id: round.post_competitive_questionnaire.id})
            .execute()
            .then(() => {
              resolve();
            })
            .catch(error => {
              reject(error);
            })
        } else {
          this.db.getRepository('final-state').save(state).then(() => {
            resolve();
          }).catch(error => {
            reject(error);
          })
        }
      })
    })
  }

  getPostCompetitiveQuestionnairesToUpdate(): Promise<FinalState[]> {
    return this.db.getRepository('final-state')
      .createQueryBuilder('final-state')
      .innerJoinAndSelect('final-state.round', 'round', 'round.is_completed_locally = :is_completed_locally', {is_completed_locally: 1})
      .where('final-state.modified = 1')
      .getMany();
  }

  setPostCompetitiveQuestionnaireSynchronized(state: FinalState): Promise<any> {
    return this.db.createQueryBuilder()
      .update('final-state')
      .set({modified: 0})
      .where('id = :id', {id: state.id})
      .execute();
  }

  saveDefaultHole(hole: Hole): Promise<any> {
    hole.modified = 2;
    return new Promise((resolve, reject) => {
      this.db.getRepository('hole').save(hole).then(() => {
        resolve();
      }).catch(error => {
        reject(error);
      })
    })
  }

  getDefaultHolesToUpdate(): Promise<Hole[]> {
    return this.db.getRepository('hole').find({relations: ['round', 'round.field'], where: {modified: 2}});
  }

  setHolePlayToDelete(hole: Hole): Promise<any> {
    return new Promise((resolve, reject) => {
      let promises = [];
      if (hole.round && !!hole.round.synchronized) {
        let holeToDelete: HoleToDelete = new HoleToDelete();
        holeToDelete.number = hole.number;
        holeToDelete.round = hole.round.synchronized;
        promises.push(this.db.getRepository('hole-to-delete').save(holeToDelete));
      }
      promises.push(this.resetHole(hole));
      Promise.all(promises).then(() => {
        resolve();
      }).catch(error => {
        reject(error);
      })
    })
  }

  getHolePlaysToDelete(): Promise<HoleToDelete[]> {
    return this.db.getRepository('hole-to-delete').createQueryBuilder('hole-to-delete').getMany();
  }

  resetHole(hole: Hole): Promise<any> {
    return new Promise((resolve, reject) => {
      let updatedHole: Hole = new Hole();
      updatedHole.number = hole.number;
      updatedHole.par = hole.par;
      updatedHole.design = hole.design;
      updatedHole.round = hole.round;
      this.removeHole(hole).then(() => {
        this.saveHole(updatedHole).then(() => {
          resolve();
        }).catch(error => {
          reject(error);
        })
      }).catch(error => {
        reject(error);
      })
    })
  }

  removeHoleToDelete(holeToDelete: HoleToDelete): Promise<any> {
    return new Promise((resolve, reject) => {
      let repo = this.db.getRepository('hole-to-delete');
      repo.deleteById(holeToDelete.id).then(() => {
        resolve();
      }).catch(error => {
        reject(error);
      })
    })
  }

  saveHole(hole: Hole, completed?: boolean): Promise<any> {
    hole.modified = completed ? 1 : 0;
    return new Promise((resolve, reject) => {
      this.db.getRepository('round').findOneById(hole.round.id, {relations: ['holes']}).then((round: Round) => {
        let index: number = -1;
        if (round.hasOwnProperty('holes')) {
          for (let i = 0; i < round.holes.length; i++) {
            if (round.holes[i].number == hole.number) {
              index = i;
            }
          }
        }
        if (index > -1) {
          let repo = this.db.getRepository('hole');
          repo.findOneById(round.holes[index].id).then((savedHole: Hole) => {
            hole.id = savedHole.id;
            repo.save(hole).then(() => {
              resolve();
            }).catch(error => {
              reject(error);
            })
          }).catch(error => {
            reject(error);
          })
        } else {
          this.db.getRepository('hole').save(hole).then(() => {
            resolve();
          }).catch(error => {
            reject(error);
          })
        }
      })
    })
  }

  getHolesToUpdate(): Promise<Hole[]> {
    return new Promise((resolve, reject) => {
      this.db.getRepository('hole').find({
        relations: ['strikes', 'strikes.emotionalQuestionnaire', 'strikes.foodQuestionnaire', 'strikes.physicalQuestionnaire', 'round'],
        where: {modified: 1}
      }).then((holes: Hole[]) => {
        for (let i = 0; i < holes.length; i++) {
          let auxStrikes = [];
          let currIdx = 1;
          for (let j = 0; j < holes[i].strikes.length; j++) {
            for (let play of holes[i].strikes) {
              if (play.order == currIdx) {
                auxStrikes.push(play);
                currIdx++;
                break;
              }
            }
          }
          holes[i].strikes = auxStrikes;
        }
        resolve(holes);
      }).catch(error => {
        reject(error);
      })
    })
  }

  removeHole(hole: Hole): Promise<any> {
    return new Promise((resolve, reject) => {
      if (hole && hole.id) {
        this.db.getRepository('hole').deleteById(hole.id).then(() => {
          resolve(hole);
        }).catch(error => {
          reject(error);
        })
      } else {
        resolve(hole);
      }
    })
  }

  setHoleSynchronized(hole: Hole): Promise<any> {
    return this.db.createQueryBuilder()
      .update('hole')
      .set({modified: 0})
      .where('id = :id', {id: hole.id})
      .execute();
  }

  getRound(round: Round): Promise<Round> {
    return new Promise((resolve, reject) => {
      this.db.getRepository('round').findOneById(round.id, {relations: ['field', 'pre_competitive_questionnaire', 'post_competitive_questionnaire', 'holes', 'holes.strikes']}).then(response => {
        resolve(response);
      }).catch(error => {
        reject(error);
      })
    })
  }

  clean(): Promise<any> {
    let promises = [];
    promises.push(this.removeCompletedRounds());
    return Promise.all(promises);
  }

  removeCompletedRounds(): Promise<any> {
    return this.db.createQueryBuilder()
      .delete()
      .from('round')
      .where('is_completed = :is_completed AND is_completed_locally = :is_completed_locally', {
        is_completed: 1,
        is_completed_locally: 1
      })
      .execute();
  }

}
