import {BaseEntity, Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {Play} from "./play";

@Entity('physical-questionnaire')
export class PhysicalQuestionnaire extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  feelsEnergetic: boolean;

  @Column()
  feelsGettingTired: boolean;

  @Column()
  feelsPainful: boolean;

  @Column()
  feelsTired: boolean;

  @Column()
  feelsExhausted: boolean;

  @OneToOne(() => Play, play => play.physicalQuestionnaire, {
    onDelete: "CASCADE"
  })
  @JoinColumn()
  play: Play;

}
