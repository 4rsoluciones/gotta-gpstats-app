import {Component} from '@angular/core';
import {IonicPage, ModalController, ViewController} from 'ionic-angular';
import {CoursesProvider, RoundProvider, SelectOptionsProvider, SettingsProvider} from "../../providers/providers";
import {Field} from "../../models/field";

@IonicPage()
@Component({
  selector: 'page-new-course',
  templateUrl: 'new-course.html',
})
export class NewCoursePage {
  countries: any;
  movements: any[];
  types: any[];
  coursesOptions: any[];
  levels: any[];
  parOptions: any[];
  holes: any[];
  newCourse: any;
  formStatus: string;
  selectOpts = {
    cssClass: 'secondary-select'
  };

  constructor(private viewCtrl: ViewController,
              private modalCtrl: ModalController,
              private coursesProvider: CoursesProvider,
              private roundProvider: RoundProvider,
              private settings: SettingsProvider,
              private selectOptions: SelectOptionsProvider) {
    let promises = [];
    promises.push(this.selectOptions.getCountries().then(response => {
      this.countries = response;
    }));
    promises.push(this.selectOptions.getMovements().then(response => {
      this.movements = response;
    }));
    promises.push(this.selectOptions.getFieldTypes().then(response => {
      this.types = response;
    }));
    promises.push(this.selectOptions.getParOptions().then(response => {
      this.parOptions = response;
    }));
    this.coursesOptions = [
      {
        value: 'Único',
        title: 'Único'
      }, {
        value: 'Otro',
        title: 'Otro'
      }
    ];
    this.levels = [
      {
        value: 'Dorado',
        title: 'Dorado'
      }, {
        value: 'Negro',
        title: 'Negro'
      }, {
        value: 'Azul',
        title: 'Azul'
      }, {
        value: 'Blanco',
        title: 'Blanco'
      }, {
        value: 'Amarillo',
        title: 'Amarillo'
      }, {
        value: 'Rojo',
        title: 'Rojo'
      }, {
        value: 'Caballeros Única',
        title: 'Caballeros Única'
      }, {
        value: 'Damas Única',
        title: 'Damas Única'
      }, {
        value: 'Otro',
        title: 'Otro'
      }
    ];
    this.holes = [
      {
        value: 9,
        title: '9'
      }, {
        value: 18,
        title: '18'
      }
    ];
    this.newCourse = {};
    promises.push(this.settings.get().then(response => {
      this.newCourse.country = response.country;
    }));
    Promise.all(promises).then(() => {
      // this.newCourse.country = this.countries[0].code;
    });
    this.formStatus = this.getFormStatus();
  }

  inputChanged() {
    this.formStatus = this.getFormStatus();
  }

  getFormStatus(): string {
    if (!this.newCourse.country) {
      return 'invalid-country';
    }
    if (!this.newCourse.club) {
      return 'invalid-club';
    }
    if (this.newCourse.topography == undefined) {
      return 'invalid-topography';
    }
    if (this.newCourse.type == undefined) {
      return 'invalid-type';
    }
    if ((this.newCourse.course == undefined)) {
      return 'invalid-course';
    }
    if ((this.coursesOptions[this.newCourse.course].value == 'Otro') && (!this.newCourse.courseName)) {
      return 'invalid-courseName';
    }
    if (this.newCourse.level == undefined) {
      return 'invalid-level';
    }
    if ((this.levels[this.newCourse.level].value == 'Otro') && (!this.newCourse.levelName)) {
      return 'invalid-levelName';
    }
    if (this.newCourse.par == undefined) {
      return 'invalid-par';
    }
    if (this.newCourse.holes == undefined) {
      return 'invalid-holes';
    }
    return 'valid';
  }

  createClicked() {
    if (this.getFormStatus() == 'valid') {
      let newField = new Field();
      newField.country = this.newCourse.country;
      newField.hole_count = this.holes[this.newCourse.holes].value;
      newField.par = this.parOptions[this.newCourse.par].value;
      newField.route = (this.coursesOptions[this.newCourse.course].value == 'Otro') ? this.newCourse.courseName : this.coursesOptions[this.newCourse.course].value;
      newField.sport_club_name = this.newCourse.club;
      newField.start_point = (this.levels[this.newCourse.level].value == 'Otro') ? this.newCourse.levelName : this.levels[this.newCourse.level].value;
      newField.topographic_movements = this.movements[this.newCourse.topography].value;
      newField.type = this.types[this.newCourse.type].value;
      let alertModal = this.modalCtrl.create('AlertPage', {
        image: 'assets/imgs/iconos/cancha-creada.png',
        title: '¡Cancha cargada!',
        subtitle: 'Gracias por colaborar con GPStats.\n¡Ahora podés seguir cargando tu vuelta!',
        okButton: 'CONTINUAR AHORA',
        cancelButton: 'Descartar vuelta.'
      });
      alertModal.onDidDismiss(data => {
        if (data) {
          this.roundProvider.setCourse(newField);
          this.viewCtrl.dismiss({next: 'continue-round'});
        } else {
          this.coursesProvider.saveDefault(newField);
          this.viewCtrl.dismiss({next: 'goroot'});
        }
      });
      alertModal.present();
    }
  }

}
