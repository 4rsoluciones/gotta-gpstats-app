import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-stats',
  templateUrl: 'stats.html',
})
export class StatsPage {
  tab1Root: string = 'ComparativesPage';
  tab2Root: string;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }

}
