import {BaseEntity, Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {Round} from "./round";

@Entity('final-state')
export class FinalState extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  feeling_status: string;

  @OneToOne(() => Round, round => round.post_competitive_questionnaire, {
    onDelete: "CASCADE"
  })
  @JoinColumn()
  round: Round;

  /**
   * 0: no hay modificaciones
   * 1: hay modificaciones y se debe enviar al backend
   */
  @Column()
  modified: number;

}
