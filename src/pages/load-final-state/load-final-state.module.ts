import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {LoadFinalStatePage} from './load-final-state';

@NgModule({
  declarations: [
    LoadFinalStatePage,
  ],
  imports: [
    IonicPageModule.forChild(LoadFinalStatePage),
  ],
})
export class LoadInitialStateModule {
}
