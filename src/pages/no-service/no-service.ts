import {Component} from '@angular/core';
import {Events, IonicPage, Platform} from 'ionic-angular';
import {ApiProvider, UserProvider} from "../../providers/providers";
import {InAppBrowser} from "@ionic-native/in-app-browser";

@IonicPage()
@Component({
  selector: 'page-no-service',
  templateUrl: 'no-service.html',
})
export class NoServicePage {
  subscription: any;

  constructor(private platform: Platform,
              private events: Events,
              private api: ApiProvider,
              private iab: InAppBrowser,
              private user: UserProvider) {

  }

  ionViewDidEnter() {
    this.subscription = this.platform.resume.subscribe(() => {
      this.user.checkLoginStatus();
    })
  }

  ionViewWillLeave() {
    this.subscription.unsubscribe();
  }

  getGpstatsClicked() {
    this.iab.create(this.api.getCentralizadorConfig().url + 'platform/1', '_system');
  }

  logoutClicked() {
    this.events.publish('logout');
  }

  retryClicked() {
    this.user.checkLoginStatus();
  }

}
