import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {PlayPage} from './play';
import {HorizontalSpinnerModule} from "@4r/horizontal-spinner/horizontal-spinner.module";
import {DirectivesModule} from "../../directives/directives.module";

@NgModule({
  declarations: [
    PlayPage,
  ],
  imports: [
    IonicPageModule.forChild(PlayPage),
    HorizontalSpinnerModule,
    DirectivesModule
  ]
})
export class PlayPageModule {
}
